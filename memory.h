/*
 *  Copyright (C) 2011 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISUALMEMORY_MEMORY_H
#define VISUALMEMORY_MEMORY_H

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "structs.h"
#include "geometry.h"
#include "controller.h"
#include "depuracion.h"
#include "ColorFilter.h"

using namespace visionLibrary;

namespace visualMemory {
  class Memory {
		public:
			Memory (Controller* controller);
		  virtual ~Memory ();

			void iteration ();

		private:
			Controller* controller;
			ColorFilter* colorFilter;
			colorspaces::Image *image1, *image2;
			double maintenanceInstant, actualInstant, parallelogramsInstant, cacheUpdateInstant;

			CvPoint3D32f lastPosition;

			void instantBordersProcedure ();
			void fastBordersProcedure ();
			void solisSegmentsProcedure ();

			void calcTypeLines (IplImage &src, vector<Segment2D> &lines);
			int getType (string color1, string color2);
			int getType (int color1, int color2);
			void pixel2optical(TPinHoleCamera * camera, HPoint2D * p);
			void optical2pixel(TPinHoleCamera * camera, HPoint2D * p);
			void getIntersectionZ (TPinHoleCamera *camera, HPoint3D &res, HPoint2D in);
			void myIntersection (TPinHoleCamera *camera, HPoint3D &res, HPoint2D in);
			void point3Dto2D (TPinHoleCamera *camera, HPoint2D &res, HPoint3D in);
			void linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D &intersectionPoint);
			void deleteHorizont ();

			void cargarPtosExtremos ();
			void cargarCruzCentral ();
			void cargarPtosClicados ();
			void updateSegments (CvPoint3D32f robotPosition);
			bool contieneOContenido (Parallelogram3D par1, Parallelogram3D par2);
			void hypothesizeParallelograms ();
			void segmentsMaintenance ();
			void parallelogramsMaintenance (CvPoint3D32f robotPosition);
			int distancePointLine (HPoint3D Point, Segment3D segment, HPoint3D *Intersection, float *Distance);
			bool equalsSegment2D (Segment2D seg1, Segment2D seg2);
			bool isInPrediction (Segment2D segment, TPinHoleCamera cam, int idCam);
			void createSegment2D (HPoint2D startPoint, HPoint2D endPoint, Segment2D &segment);
			void createSegment3D (HPoint3D startPoint, HPoint3D endPoint, Segment3D &segment);
			void segmentProjection (Segment2D segment, TPinHoleCamera cam, int idCam);
			bool storeSegment3D (Segment3D segment);
			bool deleteSegment3D (Segment3D segment);
			bool deleteParallelogram3D (Parallelogram3D parallelogram);
			void setImages ();
			void resetBuffers ();
			void checkSegments ();
			void searchAndEraseSegmentFromMemory (Segment2D segment);
			void searchAndEraseParallelogramFromMemory (Parallelogram2D parallelogram);
			void checkPrediction ();
			void checkSegmentPrediction ();
			void checkParallelogramPrediction ();
			void doPredictions ();
			void segmentsPredictions ();
			void parallelogramsPredictions ();

			void solis2Dto3D ();
			void predicted2Dto3D ();
			void new2Dto3D ();
			void refuted2Dto3D ();
			void fitted2Dto3D ();

			void preprocessImage(TPinHoleCamera * camera, IplImage &src);
			bool run_down(IplImage &src, int row, int col, int last, int min_diff, int &new_row, int &new_last);
			void getHorizonLine(TPinHoleCamera * camera, double &A, double &B, double &C);
			int getHorizonPos(double A, double B, double C, int col);

			static const int MAX_LIFE_INCREMENTS;
			static const int MAX_LINES_IN_MEMORY;
			static const int CACHE_SPACE;
			static const int SEGMENT_LIFE_TIME;
			static const int PARALLELOGRAM_LIFE_TIME;
			static const int MIN_TAM_SEG;
			static const int MAX_TAM_SEG;
			static const float SOLIS_THRESHOLD;
			static const float COMP_PARAL_THRES;
			static const double TIME_UPDATE_CACHE;
			static const double TIME_SEGMENTS_MAINTENANCE;
			static const double TIME_PARALLELOGRAMS_MAINTENANCE;
			static const float SALIENCY_INCREMENT;
			static const float MAX_SALIENCY;
			static const float MIN_SALIENCY;
			static const float LIFE_DECREMENT;
			static const float MIN_LIFE;
			static const float MAX_LIFE;
	};
}

#endif
