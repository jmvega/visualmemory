/*
 *  Copyright (C) 1997-2011 JDERobot Developers Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio Vega <julio.vega@urjc.es>,
 *
 */

#ifndef VISUALMEMORY_VIEW_H
#define VISUALMEMORY_VIEW_H

#include <pthread.h>
#include <string>
#include <iostream>
#include <gtkmm.h>
#include <libglademm.h>
#include <IceUtil/Thread.h>
#include <IceUtil/Time.h>
#include <jderobot/camera.h>
#include <colorspaces/colorspacesmm.h>
#include "controller.h"
#include "drawarea.h"
#include "structs.h"
#include <stdio.h>

namespace visualMemory {
	class DrawArea;
	class Controller;

  class View {
		public:
		  View(Controller* controller);
		  virtual ~View();

			/*Return true if the windows is visible*/
		  bool isVisible();

		  /*Display window*/
		  void display();
			void iteration ();
			Controller* controller;

		private:
			DrawArea* world;

			bool isFollowing;

			bool showOpenGL;

			void updateStructures ();
			pthread_mutex_t mutex_image;

			void loadViewer ();
			void attachViewerEvents ();

			void prepare2draw (IplImage &image);
			void color2bn (colorspaces::Image& image);
			void color2black (colorspaces::Image& image);
			void tratarImagen (colorspaces::Image& image);

			// SETS: lo que "ponemos" a disposición del drawarea, cogiéndolo del controller
			void setEncoders ();
			void setLaser ();
			void setCamara1 (colorspaces::Image& image);
			void setCamara2 (colorspaces::Image& image);

		  Glib::RefPtr<Gnome::Glade::Xml> refXml;
		  Gtk::Main gtkmain;
		  Gtk::Window *mainwindow;
		  Gtk::Window *openglwindow;
			pthread_mutex_t mutex_image;
			//char * imgSrc1;
			//char * imgSrc2;
			colorspaces::Image *image1;
			colorspaces::Image *image2;

			// Widgets ventana principal
			Gtk::ToggleButton *openglButton;
			Gtk::Button *exitButton;
			Gtk::ToggleButton *iterationButton;
			Gtk::Button *iterationButton1;
			Gtk::Image *gtk_image1;
			Gtk::Image *gtk_image2;
			Gtk::EventBox *eventbox_left;
			Gtk::EventBox *eventbox_right;
			Gtk::ComboBox *imagenCombo;
			Gtk::ToggleButton *solisButton;
			Gtk::ToggleButton *predictionsButton;
			Gtk::ToggleButton *refutedButton;
			Gtk::ToggleButton *fittedButton;
			Gtk::ToggleButton *newsButton;
			Gtk::ToggleButton *solisButton2;
			Gtk::ToggleButton *predictionsButton2;
			Gtk::ToggleButton *refutedButton2;
			Gtk::ToggleButton *fittedButton2;
			Gtk::ToggleButton *newsButton2;
			Gtk::ToggleButton *vergencyButton;
			virtual bool on_right_clicked(GdkEventButton * event);
			virtual bool on_left_clicked(GdkEventButton * event);
			void exitButton_clicked();
			void iterationButton_toggled();
			void iterationButton1_clicked();
			void openglButton_toggled();
			void imagenCombo_changed();
			void solisButton_toggled();
			void predictionsButton_toggled();
			void refutedButton_toggled();
			void fittedButton_toggled();
			void newsButton_toggled();
			void vergencyButton_toggled ();

			// openGl buttons
			void solisButton2_toggled();
			void predictionsButton2_toggled();
			void refutedButton2_toggled();
			void fittedButton2_toggled();
			void newsButton2_toggled();

			// Widgets ventana OpenGL
			Gtk::Button *camera1Button;
			Gtk::Button *camera2Button;
			Gtk::Button *camera3Button;
			Gtk::Button *camera4Button;
			Gtk::Button *pioneerCameraButton;
			Gtk::ComboBox *memoryCombo;
			Gtk::ToggleButton *instantButton;
			Gtk::ToggleButton *saliencyButton;
			void camera1Button_clicked();
			void camera2Button_clicked();
			void camera3Button_clicked();
			void camera4Button_clicked();
			void pioneerCameraButton_clicked();
			void memoryCombo_changed();
			void instantButton_toggled();
			void saliencyButton_toggled();
  };
} // namespace

#endif /*VISUALMEMORY_VIEW_H*/
