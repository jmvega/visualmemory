/*
 *  Copyright (C) 2011 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "memory.h"

namespace visualMemory {
	const int Memory::MAX_LIFE_INCREMENTS = 30000;
	const int Memory::MAX_LINES_IN_MEMORY = 1000;
	const int Memory::CACHE_SPACE = 4000;
	const int Memory::SEGMENT_LIFE_TIME = 30;
	const int Memory::PARALLELOGRAM_LIFE_TIME = 30;
	const int Memory::MIN_TAM_SEG = 30;
	const int Memory::MAX_TAM_SEG = 250;
	const float Memory::SOLIS_THRESHOLD = 0.3;
	const float Memory::COMP_PARAL_THRES = 50.;
	const double Memory::TIME_UPDATE_CACHE = 3.;
	const double Memory::TIME_SEGMENTS_MAINTENANCE = 15.;
	const double Memory::TIME_PARALLELOGRAMS_MAINTENANCE = 5.;
	const float Memory::SALIENCY_INCREMENT = 1.;
	const float Memory::MAX_SALIENCY = 100.;
	const float Memory::MIN_SALIENCY = 0.;
	const float Memory::LIFE_DECREMENT = 1.;
	const float Memory::MIN_LIFE = 0.;
	const float Memory::MAX_LIFE = 5000.;

	Memory::Memory (Controller* controller) {
		colorFilter = ColorFilter::getInstance();
		this->controller = controller;
		this->cargarPtosExtremos ();
		this->cargarCruzCentral ();
		//this->cargarPtosClicados ();
		this->resetBuffers ();

		CvPoint3D32f robotPosition;
		this->controller->getPosition (&robotPosition);
		lastPosition.x = robotPosition.x; // al finalizar actualizamos la última posición registrada del robot a la actual
		lastPosition.y = robotPosition.y;

		actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

		maintenanceInstant = actualInstant;
		parallelogramsInstant = actualInstant;
		cacheUpdateInstant = actualInstant;
	}

	void Memory::solisSegmentsProcedure () {
		IplImage im1, im2;

		im1 = *this->controller->image1;

		//this->preprocessImage(&this->controller->myCamA, im1);
		//cornerDetection::solisAlgorithm3 (im1, &this->controller->solisSegments1);

		if (this->controller->getNumCamerasUsed () == 2) {
			im2 = *this->controller->image2;
			//cornerDetection::solisAlgorithm3 (im2, &this->controller->solisSegments2);
		}

    //this->calcTypeLines(im1, this->controller->solisSegments1); // calculate type
	}

	void Memory::deleteHorizont () {
		std::vector<Segment2D>::iterator it;
		HPoint3D ptoHorizonte3D;
		HPoint2D ptoHorizonte2D;
		CvPoint3D32f myPoint;
		this->controller->getPosition (&myPoint);
		gsl_matrix *matrizRobotYPan, *horizonteRel, *horizonteAbs;
		horizonteRel = gsl_matrix_calloc(4,1);
		horizonteAbs = gsl_matrix_calloc(4,1);
		matrizRobotYPan = gsl_matrix_calloc(4,4);

		gsl_matrix_set(matrizRobotYPan,0,0,cos((myPoint.z+this->controller->getPan1())*DEGTORAD));
		gsl_matrix_set(matrizRobotYPan,0,1,-sin((myPoint.z+this->controller->getPan1())*DEGTORAD));
		gsl_matrix_set(matrizRobotYPan,0,2,0.);
		gsl_matrix_set(matrizRobotYPan,0,3,myPoint.x);
		gsl_matrix_set(matrizRobotYPan,1,0,sin((myPoint.z+this->controller->getPan1())*DEGTORAD));
		gsl_matrix_set(matrizRobotYPan,1,1,cos((myPoint.z+this->controller->getPan1())*DEGTORAD));
		gsl_matrix_set(matrizRobotYPan,1,2,0.);
		gsl_matrix_set(matrizRobotYPan,1,3,myPoint.y);
		gsl_matrix_set(matrizRobotYPan,2,0,0.);
		gsl_matrix_set(matrizRobotYPan,2,1,0.);
		gsl_matrix_set(matrizRobotYPan,2,2,1.);
		gsl_matrix_set(matrizRobotYPan,2,3,0.); // Z de la base robot la considero 0
		gsl_matrix_set(matrizRobotYPan,3,0,0.);
		gsl_matrix_set(matrizRobotYPan,3,1,0.);
		gsl_matrix_set(matrizRobotYPan,3,2,0.);
		gsl_matrix_set(matrizRobotYPan,3,3,1.0);

		gsl_matrix_set(horizonteRel,0,0,10000.0); // A 10000 en la dirección de Pan en Z = 0
		gsl_matrix_set(horizonteRel,1,0,0.0);
		gsl_matrix_set(horizonteRel,2,0,0.0);
		gsl_matrix_set(horizonteRel,3,0,1.0);

		gsl_linalg_matmult(matrizRobotYPan,horizonteRel,horizonteAbs);

		// Sacamos el punto horizonte en absoluto
		ptoHorizonte3D.X = (float) gsl_matrix_get (horizonteAbs, 0, 0);
		ptoHorizonte3D.Y = (float) gsl_matrix_get (horizonteAbs, 1, 0);
		ptoHorizonte3D.Z = (float) gsl_matrix_get (horizonteAbs, 2, 0);
		ptoHorizonte3D.H = 1.;

		project(ptoHorizonte3D, &ptoHorizonte2D, this->controller->myCamA);
		optical2pixel (&this->controller->myCamA, &ptoHorizonte2D);

		it = this->controller->solisSegments1.begin();

		while (it != this->controller->solisSegments1.end()) {
			if ((((*it).start.y) < ptoHorizonte2D.y) || (((*it).end.y) < ptoHorizonte2D.y)) {
				it = this->controller->solisSegments1.erase (it);
			} else it++;
		}

		gsl_matrix_free(matrizRobotYPan);
		gsl_matrix_free(horizonteRel);
		gsl_matrix_free(horizonteAbs);
	}

	void Memory::updateSegments (CvPoint3D32f robotPosition) {
		std::vector<Segment3D>::iterator it;

		it = this->controller->segmentMemory.begin();

		while (it != this->controller->segmentMemory.end()) {
			(*it).traveled += abs(sqrt(pow(robotPosition.x-lastPosition.x,2) + pow(robotPosition.y-lastPosition.y,2)));
			if (((*it).traveled > MAX_LIFE_INCREMENTS) || ((*it).timestamp - actualInstant > SEGMENT_LIFE_TIME))
				it = this->controller->segmentMemory.erase (it);
			else
				it++;
		}
	}

	bool Memory::contieneOContenido (Parallelogram3D par1, Parallelogram3D par2) {
		if (geometry::haveACommonVertex (par1, par2)) {
			Segment3D seg1, seg2, seg3, seg4;
			this->createSegment3D (par1.p2, par1.p1, seg1);
			this->createSegment3D (par1.p1, par1.p3, seg2);
			this->createSegment3D (par1.p3, par1.p4, seg3);
			this->createSegment3D (par1.p4, par1.p2, seg4);
			Segment3D myArray1[4] = {seg1, seg2, seg3, seg4};

			this->createSegment3D (par2.p2, par2.p1, seg1);
			this->createSegment3D (par2.p1, par2.p3, seg2);
			this->createSegment3D (par2.p3, par2.p4, seg3);
			this->createSegment3D (par2.p4, par2.p2, seg4);
			Segment3D myArray2[4] = {seg1, seg2, seg3, seg4};

			HPoint3D proy1, proy2;
			int ubi1, ubi2, i = 0, j;
			float dist1, dist2;
			bool found = false;

			while ((i < 4) && (!found)) {
				j = 0;
				while ((j < 4) && (!found)) {
					if (geometry::areTheSameSegment3D (myArray1[i], myArray2[j])) {
						found = true;
					} else {
						dist1 = geometry::distancePointLine (myArray1[i].start, myArray2[j], proy1, ubi1);
						dist2 = geometry::distancePointLine (myArray1[i].end, myArray2[j], proy2, ubi2);

						found = geometry::overlappedSegments (myArray2[j], proy1, proy2, ubi1, ubi2, dist1, dist2, false);
					}
					j++;
				}
				i++;
			}
			return found;

		} else return false;
	}

	void Memory::hypothesizeParallelograms () {
		bool squareFound, coincidencia, igualQue, contieneOContenido;
		float dist1, dist2, ang;
		Parallelogram3D square;
		std::vector<Segment3D>::iterator it1;
		std::vector<Segment3D>::iterator it2;
		std::vector<Parallelogram3D>::iterator it3;

		for(it1 = this->controller->segmentMemory.begin(); it1 != this->controller->segmentMemory.end(); it1++) {
			squareFound = false;
			it2 = it1;
			it2++;

			while ((it2 != this->controller->segmentMemory.end()) && (!squareFound)) {
				if (geometry::haveACommonVertex((*it1),(*it2),&square)) {
					dist1 = geometry::distanceBetweenPoints3D ((*it1).start, (*it1).end);
					dist2 = geometry::distanceBetweenPoints3D ((*it2).start, (*it2).end);
					ang = geometry::angleSquare (square);

					if(((dist1<MAX_TAM_SEG)&&(dist2<MAX_TAM_SEG))&&((dist1>MIN_TAM_SEG)&&(dist2>MIN_TAM_SEG))&&((ang > (DEGTORAD*80))&&(ang < (DEGTORAD*100)))){ // lo consideramos paralelogramo
						geometry::GetLastPointSquare (&square);
						squareFound = true;

						it3 = this->controller->parallelograms.begin ();
						coincidencia = false;

						while ((it3 != this->controller->parallelograms.end ()) && (!coincidencia)) {
							igualQue = geometry::areTheSameParallelogram (square, (*it3), COMP_PARAL_THRES); // threshold to compare
							contieneOContenido = this->contieneOContenido (square, (*it3));
							coincidencia = (igualQue || contieneOContenido);
							if (!coincidencia) it3++;
						}

						square.life = MAX_LIFE;
						square.saliency = MIN_SALIENCY;
						square.timestamp = actualInstant;
						if (coincidencia) {
							int whichIsTheMax = geometry::getMaximizedParallelogram (square, (*it3));
							if (whichIsTheMax == 1) {
								this->controller->parallelograms.erase (it3);
								this->controller->parallelograms.push_back (square);
							}
							this->controller->fittedParallelograms1_3D.push_back (square);
						} else {
							this->controller->parallelograms.push_back (square); // añadimos paralelogramo a memoria
							this->controller->newParallelograms1_3D.push_back (square);
						}
					}
				}
				it2++;
			}
		}
	}

	void Memory::segmentsMaintenance () {
		bool isMerge = false;

		std::vector<Segment3D>::iterator it1, it2;
		Segment2D seg1, seg2;

		it1 = this->controller->segmentMemory.begin();

		while ((it1 != this->controller->segmentMemory.end()) && (!isMerge)) {
			it2 = it1;
			it2++;

			if (it2 != this->controller->segmentMemory.end()) {
				// TODO: hacer merge correctamente...
				seg1.start.x = (*it1).start.X;
				seg1.start.y = (*it1).start.Y;
				seg1.end.x = (*it1).end.X;
				seg1.end.y = (*it1).end.Y;

				seg2.start.x = (*it2).start.X;
				seg2.start.y = (*it2).start.Y;
				seg2.end.x = (*it2).end.X;
				seg2.end.y = (*it2).end.Y;

				isMerge = geometry::mergeSegments (seg1, seg2);

				if (isMerge) { // si hemos logrado fusionarlo lo anulamos
					it1 = this->controller->segmentMemory.erase (it1);
				} else it1++;
			}
		}
		maintenanceInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	}

	void Memory::parallelogramsMaintenance (CvPoint3D32f robotPosition) {
		std::vector<Parallelogram3D>::iterator it;

		it = this->controller->parallelograms.begin();

		while (it != this->controller->parallelograms.end()) {
			(*it).life -= LIFE_DECREMENT;
			if ((*it).life < MIN_LIFE)
				(*it).life = MIN_LIFE;

			(*it).saliency += SALIENCY_INCREMENT;
			if ((*it).saliency > MAX_SALIENCY)
				(*it).saliency = MAX_SALIENCY;

			(*it).traveled += abs(sqrt(pow(robotPosition.x-lastPosition.x,2) + pow(robotPosition.y-lastPosition.y,2)));
			if (((*it).traveled > MAX_LIFE_INCREMENTS) || ((*it).timestamp - actualInstant > PARALLELOGRAM_LIFE_TIME))
				it = this->controller->parallelograms.erase (it);
			else
				it++;
		}

		parallelogramsInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	}

	bool Memory::equalsSegment2D (Segment2D seg1, Segment2D seg2) {
		Segment3D seg1b, seg2b;
		bool found = false;
		HPoint3D proy1, proy2;
		int ubi1, ubi2;
		float dist1, dist2;

		geometry::segment2Dto3D (seg1, &seg1b);
		geometry::segment2Dto3D (seg2, &seg2b);

		if (geometry::areTheSameSegment2D (seg1, seg2)) {
			found = true;
		} else {
			// Check every segment in memory, with segment. Calculate intersection point between segment i and segment.start perpendicular
			dist1 = geometry::distancePointLine (seg1b.start, seg2b, proy1, ubi1);
			dist2 = geometry::distancePointLine (seg1b.end, seg2b, proy2, ubi2);

			found = geometry::overlappedSegments (seg2b, proy1, proy2, ubi1, ubi2, dist1, dist2, 1); // If they're parallel lines -> we finish checking
		}

		return found;
	}

	bool Memory::isInPrediction (Segment2D segment, TPinHoleCamera cam, int idCam) {
		bool found = false;
		HPoint2D p1, p2;
		Segment2D actualSegment;
		std::vector<Segment2D>::iterator it;

		for(it = this->controller->predictedSegments1.begin(); (it != this->controller->predictedSegments1.end()) && (!found); it++) {
			found = this->equalsSegment2D ((*it), segment);
/*
			if (found) {
				//printf ("isInPrediction!\n");
				(*it).timestamp = actualInstant;
				(*it).traveled = 0.;
				(*it).isWellPredicted = true;
			}*/
		}

		return found;
	}

	void Memory::createSegment2D (HPoint2D startPoint, HPoint2D endPoint, Segment2D &segment) {
		segment.start.x = startPoint.x;
		segment.start.y = startPoint.y;
		segment.start.h = 1.;
		segment.end.x = endPoint.x;
		segment.end.y = endPoint.y;
		segment.end.h = 1.;
	}

	void Memory::createSegment3D (HPoint3D startPoint, HPoint3D endPoint, Segment3D &segment) {
		segment.start.X = startPoint.X;
		segment.start.Y = startPoint.Y;
		segment.start.Z = startPoint.Z;
		segment.start.H = 1.;
		segment.end.X = endPoint.X;
		segment.end.Y = endPoint.Y;
		segment.end.Z = endPoint.Z;
		segment.end.H = 1.;

		segment.timestamp = actualInstant;
		segment.traveled = 0.; // desde que lo vemos tiene un recorrido el robot de 0 sobre él
	}

	bool Memory::storeSegment3D (Segment3D segment) {
		bool isMerge = false;
		std::vector<Segment3D>::iterator it;
		Segment2D seg1, seg2;

		// TODO: segmentos para Edu
		//printf ("%f, %f, %f, %i\n", segment.start.X, segment.start.Y, segment.start.Z, segment.type);
		//printf ("%f, %f, %f, %i\n", segment.end.X, segment.end.Y, segment.end.Z, segment.type);

		geometry::segment3Dto2D (segment, &seg1);

		if (geometry::segmentLength(segment) > MIN_TAM_SEG) { // TODO: puede haber un problema de memoria, al insertar muchos...
			for(it = this->controller->segmentMemory.begin(); (it != this->controller->segmentMemory.end()) && (!isMerge); it++) {
				geometry::segment3Dto2D ((*it), &seg2);
				isMerge = geometry::mergeSegments (seg1, seg2, MAX_PAR, MAX_DIST, MAX_DIST);
			}

			// TODO
			if (!isMerge) { // check insertion position
				this->controller->segmentMemory.push_back (segment);
			}
		}
		return isMerge;
	}

	bool Memory::deleteSegment3D (Segment3D segment) {
		bool wasDeleted = false;
		std::vector<Segment3D>::iterator it;
		Segment2D seg1, seg2;

		geometry::segment3Dto2D (segment, &seg1);

		for(it = this->controller->segmentMemory.begin(); (it != this->controller->segmentMemory.end()) && (!wasDeleted); it++) {
			geometry::segment3Dto2D ((*it), &seg2);
			wasDeleted = geometry::mergeSegments (seg1, seg2, MAX_PAR, MAX_DIST, MAX_DIST);

			if (wasDeleted)
				it = this->controller->segmentMemory.erase (it);
		}

		return wasDeleted;
	}

	bool Memory::deleteParallelogram3D (Parallelogram3D parallelogram) {
		bool wasDeleted = false;
		std::vector<Parallelogram3D>::iterator it;

		for(it = this->controller->parallelograms.begin(); (it != this->controller->parallelograms.end()) && (!wasDeleted); it++) {
			wasDeleted = geometry::areTheSameParallelogram (parallelogram, (*it), COMP_PARAL_THRES);

			if (wasDeleted)
				it = this->controller->parallelograms.erase (it);
		}

		return wasDeleted;
	}

	void Memory::calcTypeLines(IplImage &src, vector<Segment2D> &lines) {
		HPoint2D center, p_up, p_down;
		int color_up, color_down;
		double angle;
		bool found_type;
		int n_pixels = 5;

		vector<Segment2D>::iterator line1;
		for (line1 = lines.begin(); line1 != lines.end(); line1++) {
			found_type = 0;

			/*Calc line center*/
			center.x = ((*line1).start.x + (*line1).end.x)/2.0;
			center.y = ((*line1).start.y + (*line1).end.y)/2.0;	

			/*Get angle*/
			angle = geometry::calcVectorAngle((*line1).start.x, (*line1).start.y, (*line1).end.x, (*line1).end.y);

			for(int i=1; i<n_pixels && !found_type; i=i+2) {
				/*Get check points and colors*/
				p_up.x = center.x + n_pixels*sin(angle);
				p_up.y = center.y + n_pixels*cos(angle);
				p_down.x = center.x - n_pixels*sin(angle);
				p_down.y = center.y - n_pixels*cos(angle);

				color_up = this->colorFilter->getColor(1, (char *)src.imageData, (int) p_up.x, (int) p_up.y);
				color_down = this->colorFilter->getColor(1, (char *)src.imageData, (int) p_down.x, (int) p_down.y);

				(*line1).type =	this->getType(color_up, color_down);
				found_type = (*line1).type != ImageInput::CUNKNOWN;
			}
		}
	}

	int Memory::getType(string color1, string color2) {
		int c1, c2;

		if(!color1.compare("ORANGE"))
			c1 = ImageInput::CORANGE;
		else if(!color1.compare("BLUE"))
			c1 = ImageInput::CBLUE;
		else if(!color1.compare("YELLOW"))
			c1 = ImageInput::CYELLOW;
		else if(!color1.compare("WHITE"))
			c1 = ImageInput::CWHITE;
		else
			c1 = ImageInput::CUNKNOWN;

		if(!color2.compare("ORANGE"))
			c2 = ImageInput::CORANGE;
		else if(!color2.compare("BLUE"))
			c2 = ImageInput::CBLUE;
		else if(!color2.compare("YELLOW"))
			c2 = ImageInput::CYELLOW;
		else if(!color2.compare("WHITE"))
			c2 = ImageInput::CWHITE;
		else
			c1 = ImageInput::CUNKNOWN;

		return this->getType(c1, c2);
	}

	int Memory::getType(int color1, int color2) {
		if(color1 == ImageInput::CORANGE && color2 == ImageInput::CWHITE)
			return 1;
		if(color2 == ImageInput::CORANGE && color1 == ImageInput::CWHITE)
			return 1;

		if(color1 == ImageInput::CORANGE && color2 == ImageInput::CBLUE)
			return 2;
		if(color2 == ImageInput::CORANGE && color1 == ImageInput::CBLUE)
			return 2;

		if(color1 == ImageInput::CORANGE && color2 == ImageInput::CYELLOW)
			return 3;		
		if(color2 == ImageInput::CORANGE && color1 == ImageInput::CYELLOW)
			return 3;	

		if(color1 == ImageInput::CWHITE && color2 == ImageInput::CYELLOW)
			return 4;
		if(color2 == ImageInput::CWHITE && color1 == ImageInput::CYELLOW)
			return 4;

		if(color1 == ImageInput::CWHITE && color2 == ImageInput::CBLUE)
			return 5;
		if(color2 == ImageInput::CWHITE && color1 == ImageInput::CBLUE)
			return 5;

		return 0;
	}

	void Memory::pixel2optical (TPinHoleCamera * camera, HPoint2D * p)	{
		float aux;
		int height;

		height = camera->rows;

		aux = p->x;
		p->x = height-1.-(p->y);
		p->y = aux;
		p->h = 1.;
	}

	void Memory::optical2pixel (TPinHoleCamera * camera, HPoint2D * p)	{
		float aux;
		int height;

		height = camera->rows;

		aux = p->y;
		p->y = height-1.-p->x;
		p->x = aux;	
		p->h = 1.;
	}

	void Memory::point3Dto2D (TPinHoleCamera *camera, HPoint2D &res, HPoint3D in) {
		HPoint2D p2d;

		project(in, &p2d, *camera);
		this->optical2pixel(camera, &p2d);

		res.x=p2d.x;
		res.y=p2d.y;
		res.h=p2d.h;
	}

	void Memory::cargarPtosExtremos () {
		HPoint2D p1,p2,p3,p4;

		p1.x = 0.;
		p1.y = 0.;//35.;
		p1.h = 1.;

		p2.x = 320.;
		p2.y = 0.;//35.;
		p2.h = 1.;

		p3.x = 320.;
		p3.y = 240.;
		p3.h = 1.;

		p4.x = 0.;
		p4.y = 240.;
		p4.h = 1.;

		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoExtremo1, p1);
		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoExtremo2, p2);
		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoExtremo3, p3);
		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoExtremo4, p4);

		if (this->controller->getNumCamerasUsed () == 2) {
			this->getIntersectionZ (&this->controller->myCamB, this->controller->puntoExtremo5, p1);
			this->getIntersectionZ (&this->controller->myCamB, this->controller->puntoExtremo6, p2);
			this->getIntersectionZ (&this->controller->myCamB, this->controller->puntoExtremo7, p3);
			this->getIntersectionZ (&this->controller->myCamB, this->controller->puntoExtremo8, p4);
		}
	}

	void Memory::cargarCruzCentral () {
		HPoint2D p1,p2,p3,p4;

		// horizontal
		p1.x = 150.;
		p1.y = 120.;
		p1.h = 1.;

		p2.x = 170.;
		p2.y = 120.;
		p2.h = 1.;

		// vertical
		p3.x = 160.;
		p3.y = 110.;
		p3.h = 1.;

		p4.x = 160.;
		p4.y = 130.;
		p4.h = 1.;

		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoCruz1, p1);
		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoCruz2, p2);
		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoCruz3, p3);
		this->getIntersectionZ (&this->controller->myCamA, this->controller->puntoCruz4, p4);
	}

	void Memory::cargarPtosClicados () {
		this->getIntersectionZ (&this->controller->myCamA, this->controller->clickedPixelA_3D, this->controller->clickedPixelA);
		this->getIntersectionZ (&this->controller->myCamB, this->controller->clickedPixelB_3D, this->controller->clickedPixelB);
	}

	void Memory::getIntersectionZ (TPinHoleCamera *camera, HPoint3D &res, HPoint2D in) {
		HPoint2D p2d;
		HPoint3D p3d;
		float x, y, z;
		float xfinal, yfinal, zfinal;

		x = camera->position.X;
		y = camera->position.Y;
		z = camera->position.Z;

		p2d.x = in.x;
		p2d.y = in.y;
		p2d.h = in.h;

		this->pixel2optical(camera, &p2d);
		backproject(&p3d, p2d, *camera);

		/*Check division by zero*/
		if((p3d.Z-z) == 0.0) {
			res.H = 0.0;
			return;
		}

		zfinal = 0.; // Quiero que intersecte con el Plano Z = 0

		/*Linear equation (X-x)/(p3d.X-x) = (Y-y)/(p3d.Y-y) = (Z-z)/(p3d.Z-z)*/
		xfinal = x + (p3d.X - x)*(zfinal - z)/(p3d.Z-z);
		yfinal = y + (p3d.Y - y)*(zfinal - z)/(p3d.Z-z);	

		res.X = xfinal;
		res.Y = yfinal;
		res.Z = zfinal;
		res.H = 1.0;
	}

	void Memory::segmentProjection (Segment2D segment, TPinHoleCamera cam, int idCam) {
		bool wasMerged;

		HPoint3D camPosition, p1, p2, p1Intersection, p2Intersection;
		HPoint2D startPoint, endPoint;
		Segment3D segment3D;

		this->getIntersectionZ (&cam, p1Intersection, segment.start);
		this->getIntersectionZ (&cam, p2Intersection, segment.end);

		this->createSegment3D (p1Intersection, p2Intersection, segment3D);
		segment3D.type = segment.type;
		wasMerged = this->storeSegment3D (segment3D);

		if (wasMerged) {
			if (idCam == 0)
				this->controller->fittedSegments1.push_back (segment);
			else if (idCam == 1)
				this->controller->fittedSegments2.push_back (segment);
		} else {
			if (idCam == 0)
				this->controller->newSegments1.push_back (segment);
			else if (idCam == 1)
				this->controller->newSegments2.push_back (segment);

		}
	}

	void Memory::setImages () {
		pthread_mutex_lock(&this->controller->cameraMutex); // lock
		//this->image1 = this->controller->image1;
		cvCopyImage (this->controller->image1, this->image1);
		pthread_mutex_unlock(&this->controller->cameraMutex); // unlock
	}

	void Memory::resetBuffers () {
		this->controller->solisSegments1.clear ();
		this->controller->solisSegments2.clear ();
		this->controller->newSegments1.clear ();
		this->controller->newSegments2.clear ();
		this->controller->refutedSegments1.clear ();
		this->controller->refutedSegments2.clear ();
		this->controller->fittedSegments1.clear ();
		this->controller->fittedSegments2.clear ();
		this->controller->predictedSegments1.clear ();
		this->controller->predictedSegments2.clear ();

		this->controller->solisSegments1_3D.clear(); // cam A
		this->controller->solisSegments2_3D.clear(); // cam B
		this->controller->predictedSegments1_3D.clear(); // cam A
		this->controller->predictedSegments2_3D.clear(); // cam B
		this->controller->newSegments1_3D.clear(); // cam A
		this->controller->newSegments2_3D.clear(); // cam B
		this->controller->refutedSegments1_3D.clear(); // cam A
		this->controller->refutedSegments2_3D.clear(); // cam B
		this->controller->fittedSegments1_3D.clear(); // cam A
		this->controller->fittedSegments2_3D.clear(); // cam B

		this->controller->newParallelograms1.clear ();
		this->controller->refutedParallelograms1.clear ();
		this->controller->fittedParallelograms1.clear ();
		this->controller->predictedParallelograms1.clear ();
		this->controller->predictedParallelograms1_3D.clear();
		this->controller->newParallelograms1_3D.clear();
		this->controller->refutedParallelograms1_3D.clear();
		this->controller->fittedParallelograms1_3D.clear();
	}

	void Memory::checkSegments () {
		std::vector<Segment2D>::iterator itSolis;

		for(itSolis = this->controller->solisSegments1.begin(); itSolis != this->controller->solisSegments1.end(); itSolis++) {
			if (!this->isInPrediction (*itSolis, this->controller->myCamA, 0)) {
				this->segmentProjection (*itSolis, this->controller->myCamA, 0);
			}
		}

		if (this->controller->getNumCamerasUsed () == 2) {
			for(itSolis = this->controller->solisSegments2.begin(); itSolis != this->controller->solisSegments2.end(); itSolis++) {
				if (!this->isInPrediction (*itSolis, this->controller->myCamB, 1)) {
					this->segmentProjection (*itSolis, this->controller->myCamB, 1);
				}
			}
		}
	}

	void Memory::searchAndEraseSegmentFromMemory (Segment2D segment) {
		bool wasDeleted;

		HPoint3D p1Intersection, p2Intersection;
		Segment3D segment3D;

		this->getIntersectionZ (&this->controller->myCamA, p1Intersection, segment.start);
		this->getIntersectionZ (&this->controller->myCamA, p2Intersection, segment.end);

		this->createSegment3D (p1Intersection, p2Intersection, segment3D);
		segment3D.type = segment.type;
		wasDeleted = this->deleteSegment3D (segment3D);

		if (wasDeleted) {
			this->controller->refutedSegments1_3D.push_back (segment3D);
		}
	}

	void Memory::searchAndEraseParallelogramFromMemory (Parallelogram2D parallelogram) {
		bool wasDeleted;

		HPoint3D p1Intersection, p2Intersection, p3Intersection, p4Intersection;
		Parallelogram3D parallelogram3D;

		this->getIntersectionZ (&this->controller->myCamA, p1Intersection, parallelogram.p1);
		this->getIntersectionZ (&this->controller->myCamA, p2Intersection, parallelogram.p2);
		this->getIntersectionZ (&this->controller->myCamA, p3Intersection, parallelogram.p3);
		this->getIntersectionZ (&this->controller->myCamA, p4Intersection, parallelogram.p4);

		parallelogram3D.p1 = p1Intersection;
		parallelogram3D.p2 = p2Intersection;
		parallelogram3D.p3 = p3Intersection;
		parallelogram3D.p4 = p4Intersection;

		wasDeleted = this->deleteParallelogram3D (parallelogram3D);

		if (wasDeleted) {
			this->controller->refutedParallelograms1_3D.push_back (parallelogram3D);
		}
	}

	void Memory::checkPrediction () {
		this->checkSegmentPrediction ();
		this->checkParallelogramPrediction ();
	}

	void Memory::checkSegmentPrediction () {
		std::vector<Segment2D>::iterator it, it2;
		bool found = false;

		it = this->controller->predictedSegments1.begin();

		while (it != this->controller->predictedSegments1.end()) {
			for(it2 = this->controller->solisSegments1.begin(); (it2 != this->controller->solisSegments1.end()) && (!found); it2++) {
				found = this->equalsSegment2D (*it, *it2);
			}
			if (!found) {
				this->controller->refutedSegments1.push_back (*it);
				it = this->controller->predictedSegments1.erase (it);
				this->searchAndEraseSegmentFromMemory (*it);
			} else {
				it++;
				found = false; // preparamos found para la siguiente vuelta
			}
		}
	}

	void Memory::checkParallelogramPrediction () {
		std::vector<Parallelogram2D>::iterator it;
		std::vector<Segment2D>::iterator it2;
		std::vector<Segment2D> paral; // almacenamos aquí los 4 segmentos implícitos del paralelogramo
		Segment2D seg2D;
		bool found = false;

		it = this->controller->predictedParallelograms1.begin();

		while (it != this->controller->predictedParallelograms1.end()) {
			for(it2 = this->controller->solisSegments1.begin(); (it2 != this->controller->solisSegments1.end()) && (!found); it2++) {
				this->createSegment2D ((*it).p2, (*it).p1, seg2D);
				paral.push_back (seg2D);
				this->createSegment2D ((*it).p1, (*it).p3, seg2D);
				paral.push_back (seg2D);
				this->createSegment2D ((*it).p3, (*it).p4, seg2D);
				paral.push_back (seg2D);
				this->createSegment2D ((*it).p4, (*it).p2, seg2D);
				paral.push_back (seg2D);

				for (int i = 0; (i < 4) && (!found); i ++) {
					found = this->equalsSegment2D (paral[i], *it2);
				}
			}
			if (!found) {
				this->controller->refutedParallelograms1.push_back (*it);
				it = this->controller->predictedParallelograms1.erase (it);
				this->searchAndEraseParallelogramFromMemory (*it);
			} else {
				it++;
				found = false; // preparamos found para la siguiente vuelta
			}
		}
	}

	void Memory::doPredictions () {
		this->segmentsPredictions ();
		this->parallelogramsPredictions ();
	}

	void Memory::segmentsPredictions () {
		HPoint2D p1, p2, gooda, goodb;
		Segment2D actualSegment;
		std::vector<Segment3D>::iterator it;

		for(it = this->controller->segmentMemory.begin(); it != this->controller->segmentMemory.end(); it++) {
			// por si acaso: 
			(*it).start.H = 1.;
			(*it).end.H = 1.;

			project((*it).start, &p1, this->controller->myCamA);
			project((*it).end, &p2, this->controller->myCamA);

			if(displayline(p1,p2,&gooda,&goodb,this->controller->myCamA)==1) { // es proyectable, está en el campo de visión
				optical2pixel (&this->controller->myCamA, &gooda);
				optical2pixel (&this->controller->myCamA, &goodb);

				actualSegment.start.x = gooda.x;
				actualSegment.start.y = gooda.y;
				actualSegment.start.h = 1.;
				actualSegment.end.x = goodb.x;
				actualSegment.end.y = goodb.y;
				actualSegment.end.h = 1.;
				this->controller->predictedSegments1.push_back (actualSegment); // lista de predichos
			}
		}
	}

	void Memory::parallelogramsPredictions () {
		HPoint2D p1, p2, p3, p4, gooda, goodb;
		Parallelogram2D actualParallelogram;
		std::vector<Parallelogram3D>::iterator it;

		for(it = this->controller->parallelograms.begin(); it != this->controller->parallelograms.end(); it++) {
			// por si acaso: 
			(*it).p1.H = 1.;
			(*it).p2.H = 1.;
			(*it).p3.H = 1.;
			(*it).p4.H = 1.;

			project((*it).p1, &p1, this->controller->myCamA);
			project((*it).p2, &p2, this->controller->myCamA);
			project((*it).p3, &p3, this->controller->myCamA);
			project((*it).p4, &p4, this->controller->myCamA);

			if(displayline(p2,p1,&gooda,&goodb,this->controller->myCamA)==1) { // es proyectable, está en el campo de visión
				optical2pixel (&this->controller->myCamA, &gooda);
				optical2pixel (&this->controller->myCamA, &goodb);

				actualParallelogram.p2.x = gooda.x;
				actualParallelogram.p2.y = gooda.y;
				actualParallelogram.p2.h = 1.;
				actualParallelogram.p1.x = goodb.x;
				actualParallelogram.p1.y = goodb.y;
				actualParallelogram.p1.h = 1.;
			}

			if(displayline(p1,p3,&gooda,&goodb,this->controller->myCamA)==1) { // es proyectable, está en el campo de visión
				optical2pixel (&this->controller->myCamA, &gooda);
				optical2pixel (&this->controller->myCamA, &goodb);

				actualParallelogram.p1.x = gooda.x;
				actualParallelogram.p1.y = gooda.y;
				actualParallelogram.p1.h = 1.;
				actualParallelogram.p3.x = goodb.x;
				actualParallelogram.p3.y = goodb.y;
				actualParallelogram.p3.h = 1.;
			}

			if(displayline(p3,p4,&gooda,&goodb,this->controller->myCamA)==1) { // es proyectable, está en el campo de visión
				optical2pixel (&this->controller->myCamA, &gooda);
				optical2pixel (&this->controller->myCamA, &goodb);

				actualParallelogram.p3.x = gooda.x;
				actualParallelogram.p3.y = gooda.y;
				actualParallelogram.p3.h = 1.;
				actualParallelogram.p4.x = goodb.x;
				actualParallelogram.p4.y = goodb.y;
				actualParallelogram.p4.h = 1.;
			}

			if(displayline(p4,p2,&gooda,&goodb,this->controller->myCamA)==1) { // es proyectable, está en el campo de visión
				optical2pixel (&this->controller->myCamA, &gooda);
				optical2pixel (&this->controller->myCamA, &goodb);

				actualParallelogram.p4.x = gooda.x;
				actualParallelogram.p4.y = gooda.y;
				actualParallelogram.p4.h = 1.;
				actualParallelogram.p2.x = goodb.x;
				actualParallelogram.p2.y = goodb.y;
				actualParallelogram.p2.h = 1.;
			}

			this->controller->predictedParallelograms1.push_back (actualParallelogram); // lista de predichos
		}
	}

	void Memory::solis2Dto3D () {
		std::vector<Segment2D>::iterator it;
		HPoint3D p1Intersection, p2Intersection;
		Segment3D segment;

		for(it = this->controller->solisSegments1.begin(); it != this->controller->solisSegments1.end(); it++) {
			this->getIntersectionZ (&this->controller->myCamA, segment.start, (*it).start);
			this->getIntersectionZ (&this->controller->myCamA, segment.end, (*it).end);

			this->controller->solisSegments1_3D.push_back (segment);
		}

		if (this->controller->getNumCamerasUsed () == 2) {
			for(it = this->controller->solisSegments2.begin(); it != this->controller->solisSegments2.end(); it++) {
				this->getIntersectionZ (&this->controller->myCamB, segment.start, (*it).start);
				this->getIntersectionZ (&this->controller->myCamB, segment.end, (*it).end);

				this->controller->solisSegments2_3D.push_back (segment);
			}
		}
	}

	void Memory::predicted2Dto3D () {
		std::vector<Segment2D>::iterator it;
		HPoint3D p1Intersection, p2Intersection;
		Segment3D segment;

		for(it = this->controller->predictedSegments1.begin(); it != this->controller->predictedSegments1.end(); it++) {
			this->getIntersectionZ (&this->controller->myCamA, segment.start, (*it).start);
			this->getIntersectionZ (&this->controller->myCamA, segment.end, (*it).end);

			this->controller->predictedSegments1_3D.push_back (segment);
		}

		if (this->controller->getNumCamerasUsed () == 2) {
			for(it = this->controller->predictedSegments2.begin(); it != this->controller->predictedSegments2.end(); it++) {
				this->getIntersectionZ (&this->controller->myCamB, segment.start, (*it).start);
				this->getIntersectionZ (&this->controller->myCamB, segment.end, (*it).end);

				this->controller->predictedSegments2_3D.push_back (segment);
			}
		}
	}

	void Memory::new2Dto3D () {
		std::vector<Segment2D>::iterator it;
		HPoint3D p1Intersection, p2Intersection;
		Segment3D segment;

		for(it = this->controller->newSegments1.begin(); it != this->controller->newSegments1.end(); it++) {
			this->getIntersectionZ (&this->controller->myCamA, segment.start, (*it).start);
			this->getIntersectionZ (&this->controller->myCamA, segment.end, (*it).end);

			this->controller->newSegments1_3D.push_back (segment);
		}

		if (this->controller->getNumCamerasUsed () == 2) {
			for(it = this->controller->newSegments2.begin(); it != this->controller->newSegments2.end(); it++) {
				this->getIntersectionZ (&this->controller->myCamB, segment.start, (*it).start);
				this->getIntersectionZ (&this->controller->myCamB, segment.end, (*it).end);

				this->controller->newSegments2_3D.push_back (segment);
			}
		}
	}

	void Memory::refuted2Dto3D () {
		std::vector<Segment2D>::iterator it;
		HPoint3D p1Intersection, p2Intersection;
		Segment3D segment;

		for(it = this->controller->refutedSegments1.begin(); it != this->controller->refutedSegments1.end(); it++) {
			this->getIntersectionZ (&this->controller->myCamA, segment.start, (*it).start);
			this->getIntersectionZ (&this->controller->myCamA, segment.end, (*it).end);

			this->controller->refutedSegments1_3D.push_back (segment);
		}

		if (this->controller->getNumCamerasUsed () == 2) {
			for(it = this->controller->refutedSegments2.begin(); it != this->controller->refutedSegments2.end(); it++) {
				this->getIntersectionZ (&this->controller->myCamB, segment.start, (*it).start);
				this->getIntersectionZ (&this->controller->myCamB, segment.end, (*it).end);

				this->controller->refutedSegments2_3D.push_back (segment);
			}
		}
	}

	void Memory::fitted2Dto3D () {
		std::vector<Segment2D>::iterator it;
		HPoint3D p1Intersection, p2Intersection;
		Segment3D segment;

		for(it = this->controller->fittedSegments1.begin(); it != this->controller->fittedSegments1.end(); it++) {
			this->getIntersectionZ (&this->controller->myCamA, segment.start, (*it).start);
			this->getIntersectionZ (&this->controller->myCamA, segment.end, (*it).end);

			this->controller->fittedSegments1_3D.push_back (segment);
		}

		if (this->controller->getNumCamerasUsed () == 2) {
			for(it = this->controller->fittedSegments2.begin(); it != this->controller->fittedSegments2.end(); it++) {
				this->getIntersectionZ (&this->controller->myCamB, segment.start, (*it).start);
				this->getIntersectionZ (&this->controller->myCamB, segment.end, (*it).end);

				this->controller->fittedSegments2_3D.push_back (segment);
			}
		}
	}

	void Memory::iteration () {
		actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		CvPoint3D32f robotPosition;
		this->controller->getPosition (&robotPosition);

		this->cargarPtosExtremos ();
		this->cargarCruzCentral ();
		this->cargarPtosClicados ();

		CvPoint3D32f robotPos;
		this->controller->getPosition (&robotPos);

		this->resetBuffers (); // reseteamos las estructuras temporales

		if (!this->controller->giraffeIsMoving()) { // PTU unit must be stopped to get images
			this->solisSegmentsProcedure (); // obtenemos segmentos 2D en solisSegments1 y solisSegments2
/*			this->updateSegments (robotPosition);

			this->doPredictions ();

			this->solisSegmentsProcedure (); // obtenemos segmentos 2D en solisSegments1 y solisSegments2

			this->deleteHorizont (); // quitamos los segmentos "molestos" del horizonte

			this->checkSegments (); // comprobamos cómo meter estos nuevos segmentos

			this->checkPrediction (); // comprobamos cómo es la predicción sobre los actuales

			//if ((actualInstant - maintenanceInstant) > TIME_SEGMENTS_MAINTENANCE) this->segmentsMaintenance ();

			this->hypothesizeParallelograms (); // obtenemos supuestos paralelogramos

			if ((actualInstant - parallelogramsInstant) > TIME_PARALLELOGRAMS_MAINTENANCE) this->parallelogramsMaintenance (robotPosition);

			lastPosition.x = robotPosition.x;
			lastPosition.y = robotPosition.y;

			this->solis2Dto3D ();
			this->predicted2Dto3D ();
			this->new2Dto3D ();
			this->refuted2Dto3D ();
			this->fitted2Dto3D ();*/
		}
	}

	void Memory::preprocessImage(TPinHoleCamera * camera, IplImage &src) {
		  int col, row, color;
		  char h, s, v;
		  double A, B, C;
		  CvPoint pos;
		  int last = 0;
		  int diff;
		  bool first;
		  int new_row, new_last;

		  //cvCopy(&src, this->src_processed, NULL);

		  this->getHorizonLine(camera, A, B, C);

		  for(col = 5;col < ImageInput::IMG_WIDTH; col+=1) {
		      first = true;
		      row = this->getHorizonPos(A, B, C, col);
		      pos.x = col;
		      pos.y = row;
		      //cvCircle(&src, pos, 1, CV_RGB(255, 0, 0), 1);

		      while(row < ImageInput::IMG_HEIGHT-15) {       
		          color = this->colorFilter->getColor(0, (char *)src.imageData, col, row);

		          if(color==ImageInput::CWHITE) {
		              /*Get hsv*/
		              this->colorFilter->getColorHSV(0, (char *)src.imageData, col, row, h, s, v);

		              /*Calculate difference*/
		              diff = (int) abs(last - (int) v);
		              last = (int) v;

		              /*Skip first iteration*/
		              if(first) {
		                  first = false;
		                  row+=2;
		                  continue;
		              }

		              /*If difference is big enough*/
		              if(diff > 4) {
		                  /*Check if there are more diffs*/
		                  if(this->run_down(src, row, col, last, 4, new_row, new_last)) {
		                      last = new_last;
		                      row = new_row;

		                      pos.x = col;
		                      pos.y = row;

		                      //cvCircle(&src, pos, 1, CV_RGB(0, 255, 0), 4);
		                      cvCircle(&src, pos, 1, CV_RGB(0, 0, 0), 1, 8, 0);           
		                  }

		              }
		          }

		          row+=2;
		      }       
		  }
	}

	bool Memory::run_down(IplImage &src, int row, int col, int last, int min_diff, int &new_row, int &new_last)	{
		  char h, s, v;
		  int num_false = 0, max_false = 30; //15;
		  int cont = 0;
		  int color;
		  int diff;
		  int min_detected = 3;

		  new_row = row;

		  for(int i=row+1;i<=ImageInput::IMG_HEIGHT-15;i++) {
		      color = this->colorFilter->getColor(0, (char *)src.imageData, col, i);

		      if(color==ImageInput::CWHITE) {
		          /*Get hsv*/
		          this->colorFilter->getColorHSV(0, (char *)src.imageData, col, i, h, s, v);

		          /*Calculate difference*/
		          diff = (int) abs(last - (int) v);
		          last = (int) v;

		          /*Save if difference is big enough*/
		          if(diff > 4) {
		              num_false = 0;
		              new_row = i;
		              new_last = last;
		              cont++;
		          } else {
		              num_false++;
		              if(num_false > max_false)
		                  break;
		          }
		      }       
		  }

		  return cont >= min_detected;
	}

	void Memory::getHorizonLine(TPinHoleCamera * camera, double &A, double &B, double &C) {
		  HPoint3D p3d1, p3d2;
		  HPoint2D p2d1, p2d2;   

		  //Obtain kinematics horizon
		  p3d1.X = 5000.0;
		  p3d1.Y = 500.0;
		  p3d1.Z = 0.0;
		  p3d1.H = 1.0;
		  p3d2.X = 5000.0;
		  p3d2.Y = -500.0;
		  p3d2.Z = 0.0;
		  p3d2.H = 1.0;

			point3Dto2D (camera, p2d1, p3d1);
			point3Dto2D (camera, p2d2, p3d2);

		  //Calculate line equation Ax+By+X=0, cross product
		  A = p2d1.y - p2d2.y; //y1*z2 - z1*y2
		  B = p2d2.x - p2d1.x; //z1*x2 - x1*z2
		  C = p2d1.x*p2d2.y - p2d1.y*p2d2.x; //x1*y2 - y1*x2
	}


	int Memory::getHorizonPos(double A, double B, double C, int col)	{
		  double res;
		  int row;

		  if(B==0)
		      return 0;

		  //Solve Ax+By+C=0
		  res = -C-A*(double)col;
		  row = (int) (res/B);
		  if(row < 0)
		      row = 0;
		  return row;
	}

	Memory::~Memory () {}
}
