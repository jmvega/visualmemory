/*
 *  Copyright (C) 2011 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef PioneerPlatform_MEMORY_H
#define PioneerPlatform_MEMORY_H

#include "AbstractRobot.h"

namespace visualMemory {
  class PioneerPlatform : public AbstractRobot {
		public:
			PioneerPlatform ();
			virtual void getCameraMatrix (CvPoint3D32f robotPos, CvPoint3D32f cameraPos, float roll, float pan, float tilt, int idCam, gsl_matrix *camMatrix);
		  virtual ~PioneerPlatform ();

		//private:

	};
}

#endif
