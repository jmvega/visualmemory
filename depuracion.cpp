#include "depuracion.h"

namespace visualMemory {
	const int depuracion::DEBUG = 0;

	void depuracion::printPoint (HPoint3D point) {
		if (DEBUG) printf ("(%0.2f, %0.2f, %0.2f)", point.X, point.Y, point.Z);
	}

	void depuracion::printParallelogram (Parallelogram3D par1) {
		HPoint3D* myArray1[4] = {&(par1.p1), &(par1.p2), &(par1.p3), &(par1.p4)};
		int i;

		if (DEBUG) printf ("Parallelogram %p\n", &par1);
		if (DEBUG) printf ("=====================\n");
		if (DEBUG) printf ("[");
		for (i = 0; i < 4; i ++) {
			printPoint (*myArray1[i]);
			if (DEBUG) printf (", ");
		}
		if (DEBUG) printf ("]\n\n");
	}

	void depuracion::print_time (double initTime, double endTime, char* message) {
		long diff = (endTime-initTime);

		std::cout << message << " " << diff << " ms" << std::endl;
	} 
}
