/*
 *  Copyright (C) 2011 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "NaoRobot.h"

namespace visualMemory {
	NaoRobot::NaoRobot () {}
	void NaoRobot::getCameraMatrix (CvPoint3D32f robotPos, CvPoint3D32f cameraPos, float roll, float pan, float tilt, int idCam, gsl_matrix *camMatrix) {
		gsl_matrix *robotRT, *centroRT, *panRT, *tiltRT, *rollRT, *centroCam, *temp1, *temp2;

		robotRT = gsl_matrix_calloc(4,4);
		centroRT = gsl_matrix_calloc(4,4);
		panRT = gsl_matrix_calloc(4,4);
		tiltRT = gsl_matrix_calloc(4,4);
		rollRT = gsl_matrix_calloc(4,4);
		centroCam = gsl_matrix_calloc(4,4);
		temp1 = gsl_matrix_calloc(4,4);
		temp2 = gsl_matrix_calloc(4,4);

		gsl_matrix_set(robotRT,0,0,cos(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,0,1,-sin(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,0,2,0.);
		gsl_matrix_set(robotRT,0,3,robotPos.x);
		gsl_matrix_set(robotRT,1,0,sin(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,1,1,cos(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,1,2,0.);
		gsl_matrix_set(robotRT,1,3,robotPos.y);
		gsl_matrix_set(robotRT,2,0,0.);
		gsl_matrix_set(robotRT,2,1,0.);
		gsl_matrix_set(robotRT,2,2,1.);
		gsl_matrix_set(robotRT,2,3,0.); // Z de la base robot la considero 0
		gsl_matrix_set(robotRT,3,0,0.);
		gsl_matrix_set(robotRT,3,1,0.);
		gsl_matrix_set(robotRT,3,2,0.);
		gsl_matrix_set(robotRT,3,3,1.0);

    //Camera position
    gsl_matrix_set_identity(centroRT);
    gsl_matrix_set(centroRT, 0, 3, cameraPos.x);
    gsl_matrix_set(centroRT, 1, 3, cameraPos.y);
    gsl_matrix_set(centroRT, 2, 3, cameraPos.z);

		gsl_linalg_matmult (robotRT, centroRT, temp1);
    gsl_matrix_memcpy(centroRT, temp1);

    //Pan
    gsl_matrix_set_identity(panRT);
    gsl_matrix_set(panRT, 0, 0, cos(pan*DEGTORAD));
    gsl_matrix_set(panRT, 0, 1, -sin(pan*DEGTORAD));
    gsl_matrix_set(panRT, 1, 0, sin(pan*DEGTORAD));
    gsl_matrix_set(panRT, 1, 1, cos(pan*DEGTORAD));

    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, centroRT, panRT, 0, temp2);
    gsl_matrix_memcpy(centroRT, temp2);

    //Tilt
    gsl_matrix_set_identity(tiltRT);
    gsl_matrix_set(tiltRT, 0, 0, cos(-tilt*DEGTORAD));
    gsl_matrix_set(tiltRT, 0, 2, sin(-tilt*DEGTORAD));
    gsl_matrix_set(tiltRT, 2, 0, -sin(-tilt*DEGTORAD));
    gsl_matrix_set(tiltRT, 2, 2, cos(-tilt*DEGTORAD));

    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, centroRT, tiltRT, 0, temp2);
    gsl_matrix_memcpy(centroRT, temp2);

    //Roll
    gsl_matrix_set_identity(rollRT);
    gsl_matrix_set(rollRT, 1, 1, cos(roll*DEGTORAD));
    gsl_matrix_set(rollRT, 1, 2, -sin(roll*DEGTORAD));
    gsl_matrix_set(rollRT, 2, 1, sin(roll*DEGTORAD));
    gsl_matrix_set(rollRT, 2, 2, cos(roll*DEGTORAD));

    gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1, centroRT, rollRT, 0, temp2);
    gsl_matrix_memcpy(camMatrix, temp2);
	}

	NaoRobot::~NaoRobot () {}
}
