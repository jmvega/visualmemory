/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "attention.h"

namespace visualMemory {
	const int Attention::LIFE_INCREMENT = 20;
	const int Attention::MAX_LIFE = 5000;

	const int Attention::TIME_MAINTENANCE = 5;
	const double Attention::ATTENTION_TIME = 7.;
	const double Attention::FORCED_ATTENTION_TIME = 10.;

	Attention::Attention (Controller* controller) {
		this->controller = controller;

		actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
		attentionInstant = actualInstant;
		forcedAttentionInstant = actualInstant;
		forcedAttentionTime = 4.;

		srand (time (NULL));// initializa random seed
		latValues = 10; // valores para generar los randoms
		minLatValue = -15;
		longValues = 80;
		minLongValue = -40;
	}
/*
	bool Attention::elementAttainable (attentionElement *element, HPoint3D cameraPosition) {
		HPoint3D p;
		float pan, tilt;
		bool isAttainable = false;

		if (element->type == 1)
			p = element->parallelogram.centroid;
		else if (element->type == 2)
			p = element->face.center;
		else if (element->type == 3)
			p = element->arrow.end;

		p.X = p.X - cameraPosition.X;
		p.Y = p.Y - cameraPosition.Y;
		p.Z = 0. - cameraPosition.Z;
		p.H = 1.;

		if (p.X != 0.) {
			pan = atan(p.Y/p.X);
			tilt = atan (p.Z/p.X); // La Z siempre saldrá negativa, porque estará más alta que el punto

			pan = pan * 180.0/M_PI; // pasamos a grados
			tilt = tilt * 180.0/M_PI;

			if ((pan >= -90) && (pan <= 90) && (tilt < -20) && (tilt > -50))
				isAttainable = true;
		}

		return (isAttainable);
	}
*/
/*
	void Attention::updateElements () {
		std::vector<attentionElement>::iterator it;
		float maxSaliency = -9999.;

		for(it = this->elements.begin(); it != this->elements.end(); it++) {
			(*it).liveliness -= LIFE_DECREMENT;
			(*it).saliency += SALIENCY_INCREMENT;

			if (((*it).saliency > maxSaliency)) { // && (elementAttainable (p))) { // elemento atendible
				maxSaliency = (*it).saliency;
				maxSaliencyElement = &(*it); // me lo guardo // TODO: es correcto esto?
			}

			// TODO: implementar acción para eliminar objeto "cumplío"
			if ((*it).liveliness < LIFE_TO_DEAD) {
				this->elements.erase (it);
				if (depuracion::DEBUG) printf ("updateElements: Deleting old element of memory...\n");
			} else if ((*it).saliency > MAX_SALIENCY) {
				(*it).saliency = MAX_SALIENCY; // evitar saturación
			}
		}

		if (maxSaliencyElement != NULL) { // nos aseguramos que q tenga algo, para que no casque
			maxSaliencyElement->saliency = MIN_SALIENCY;
			maxSaliencyElement->liveliness = MAX_LIFE;
			maxSaliencyElement->lastInstant = actualInstant;
		}
	}
*/
	void Attention::point2angle (HPoint3D p, HPoint3D cameraPosition, double *longitude, double *latitude) {
		float pan, tilt;

		p.X = p.X - cameraPosition.X;
		p.Y = p.Y - cameraPosition.Y;
		p.Z = 0. - cameraPosition.Z;
		p.H = 1.;

		if (p.X == 0.) pan = 0.;
		else pan = atan(p.Y/p.X);

		if (p.X == 0.) tilt = 0.;
		else tilt = atan (p.Z/p.X); // La Z siempre saldrá negativa, porque estará más alta que el punto

		*longitude = pan * 180.0/M_PI;
		*latitude = tilt * 180.0/M_PI;
		if (depuracion::DEBUG) printf ("p = [%0.2f, %0.2f, %0.2f], pan = %0.2f, tilt = %0.2f, New coordinates: [%0.2f, %0.2f]\n", p.X, p.Y, p.Z, pan, tilt, *latitude, *longitude);
	}

/*
	void Attention::chooseState () {
		if ((myActualState == think) && (((elements.empty()) || (maxSaliencyElement == NULL)) || ((maxSaliencyElement != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)))) {
			if (depuracion::DEBUG) printf ("chooseState: 1ª opcion\n");
			if ((maxSaliencyElement != NULL) && ((actualInstant - timeForced) > timeToForcedSearch)) {
				isForcedSearch = true; // para que a la siguiente vuelta tengamos el flag activado y hagamos búsqueda
				timeForced = actualInstant;
				if (depuracion::DEBUG) printf ("Activamos flag timeForced\n");
			}
			if (depuracion::DEBUG) printf ("FORCED SEARCH\n");

			// TODO: insertVirtualElement (); // generaremos un destino si no hay destino o si, aun habiendo hay una búsqueda forzada
			updateElements ();
		}

		else if (((myActualState == think) && ((!elements.empty()) && (maxSaliencyElement != NULL))) || (isForcedSearch == true)) {
			if (depuracion::DEBUG) printf ("chooseState: 2ª opcion\n");
			myActualState = look;
			isForcedSearch = false;
		}

		else if ((myActualState == look) && (completedSearch)) {
			if (depuracion::DEBUG) printf ("chooseState: 3ª opcion\n");
			myActualState = analizeSearch;
			completedSearch = false; // TODO: comentar para poner cuello fijo
		}

		else if (myActualState == analizeSearch) {
			if (depuracion::DEBUG) printf ("chooseState: 4ª opcion\n");
			myActualState = think; // una vez que hayamos analizado correctamente la imagen en curso
		}
	}
*/
	void Attention::atenderParalelogramo (Parallelogram3D &parallelogram) {
		double longitude, latitude;
		geometry::getPolygonCentroid (parallelogram); // obtenemos el centroide del paralelogramo

		this->point2angle (parallelogram.centroid, this->controller->myCamA.position, &longitude, &latitude);

		printf ("Elemento (X/%i) en [%f,%f] - Ordenes en lat,lon = [%f, %f]\n", this->controller->parallelograms.size(), parallelogram.centroid.X, parallelogram.centroid.Y, latitude, longitude);

		this->controller->setPT1 ((float)latitude, (float)longitude);

		parallelogram.life += LIFE_INCREMENT;
		if (parallelogram.life > MAX_LIFE)
			parallelogram.life = MAX_LIFE;

		parallelogram.timestamp = actualInstant;

		parallelogram.saliency = 0.;
	}

	void Attention::atenderParalelogramos () {
		std::vector<Parallelogram3D>::iterator it, itMax;
		float maxSaliency = -9999., saliency;
		int j = 0, jMax = 0;

		it = this->controller->parallelograms.begin();
		if (it != this->controller->parallelograms.end()) { // comprobamos que no es NULL
			for (it = this->controller->parallelograms.begin(); it != this->controller->parallelograms.end(); it++, j++) {
				saliency = (*it).saliency;
				//printf ("-------: paral %i, sal = %f, lif = %f\n", j, (*it).saliency, (*it).life);
				if (saliency > maxSaliency) {
					itMax = it;
					maxSaliency = saliency;
					jMax = j;
				}
			} // itMax contendrá el paralelogramo de mayor saliencia, al cual mirar

			//printf ("BEFORE: paral %i, sal = %f, lif = %f\n", jMax, (*itMax).saliency, (*itMax).life);
			this->atenderParalelogramo (*itMax);

			//printf ("AFTER: paral %i, sal = %f, lif = %f\n", jMax, (*itMax).saliency, (*itMax).life);
		}

		attentionInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;
	}

	void Attention::generarPuntoAleatorio () {
		// longValues = número de valores posibles en el random, contados a partir de
    // minLongValue = número base a partir del que comienza el random
		latitudeRand = rand () % latValues + minLatValue;
		longitudeRand = rand () % longValues + minLongValue;
		//printf ("lat = %f, long = %f\n", latitudeRand, longitudeRand);

		this->controller->setPT1 ((float)latitudeRand, (float)longitudeRand);

		forcedAttentionInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

		// si vamos teniendo elementos, aumentamos el tiempo de búsqueda forzada:
		if (this->controller->parallelograms.size () > 0) {
			if ((this->controller->parallelograms.size() > 0) && (this->controller->parallelograms.size() < 5))
				forcedAttentionTime = FORCED_ATTENTION_TIME + this->controller->parallelograms.size ();
			else if ((this->controller->parallelograms.size() > 4) && (this->controller->parallelograms.size() < 10))
				forcedAttentionTime = (FORCED_ATTENTION_TIME*1.4) + this->controller->parallelograms.size ();
			else
				forcedAttentionTime = (FORCED_ATTENTION_TIME*1.8) + this->controller->parallelograms.size ();
		}
	}

	void Attention::iteration ()  {
		actualInstant = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000000;

		if ((actualInstant - forcedAttentionInstant) > forcedAttentionTime) this->generarPuntoAleatorio ();
		else if ((actualInstant - attentionInstant) > ATTENTION_TIME) this->atenderParalelogramos ();
	}

	Attention::~Attention () {}
}

