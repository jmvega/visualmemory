/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISUALMEMORY_VERGENCY_H
#define VISUALMEMORY_VERGENCY_H

#include <unistd.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdio.h>
#include <colorspaces/colorspacesmm.h>
#include "depuracion.h"
#include "structs.h"
#include "controller.h"

namespace visualMemory {
  class Vergency {
		public:
			Vergency (Controller* controller);
		  virtual ~Vergency ();

			std::vector<HPoint2D> epipolarPoints;
			std::vector<HPoint2D> epipolarCandidatePoints;

			void iteration ();

		private:
			HPoint2D getMiddleOfSegment (Segment2D segment);
			void calculateEpipolar (HPoint2D saliencyPoint2D_A, TPinHoleCamera camA, TPinHoleCamera camB);
			//void calculatePatchs (colorspaces::Image image, HPoint2D saliencyPoint2D, colorspaces::Image& patchedImg);
			void calculatePatchDavison (colorspaces::Image image, HPoint2D saliencyPoint2D, IplImage *patch_out, CvScalar &patch_avg, CvScalar &patch_sdv);
			double comparePatchsDavison (IplImage *patchA, IplImage *patchB, CvScalar avgA, CvScalar sdvA, CvScalar avgB, CvScalar sdvB);
			void updateStructures ();
			void matchPoint (HPoint2D point2D_A, bool analizeNeighboring);
			HPoint3D triangular (HPoint2D point2D_camA, HPoint2D point2D_camB, TPinHoleCamera camA, TPinHoleCamera camB);
			//float comparePatchs (colorspaces::Image patchedImgA, colorspaces::Image patchedImgB);
			int isCandidatePoint (HPoint2D p2D, colorspaces::Image image, bool oneCandidate = false);
			HPoint3D getPointInLineNearOfFocus (TPinHoleCamera cam, HPoint3D pointLine, float reduccion);
			HPoint2D getLine2D (HPoint2D point1, HPoint2D point2);
			float getCoordinateY (HPoint2D line1, float coordinateX);
			void resetBuffers ();
			void setImages ();
			void pixel2optical(TPinHoleCamera * camera, HPoint2D * p);
			void optical2pixel(TPinHoleCamera * camera, HPoint2D * p);

			colorspaces::Image *image1;
			colorspaces::Image *image2;

			IplImage * grayImage;
			IplImage * patch;
			IplImage * patch_tmp;
			IplImage * product;

			IplImage *patchedImgA;
			CvScalar patch_avgA;
			CvScalar patch_sdvA;

			IplImage *patchedImgB;
			CvScalar patch_avgB;
			CvScalar patch_sdvB;

			Controller* controller;

			static const int PATCH_SIZE;
			static const int PATCH_ROWS;
			static const int PATCH_COLUMNS;
			static const int DIST_EPIPOLAR;
	};
}

#endif
