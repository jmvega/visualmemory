/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISUALMEMORY_ATTENTION_H
#define VISUALMEMORY_ATTENTION_H

#include <unistd.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdio.h>
#include "depuracion.h"
#include "structs.h"
#include "controller.h"

namespace visualMemory {
  class Attention {
		public:
			Attention (Controller* controller);
		  virtual ~Attention ();

			void iteration ();

		private:
			Controller* controller;
			jderobot::PTMotorsData* myData1;
			jderobot::PTMotorsData* myData2;

			int myActualState, minLatValue, latValues, minLongValue, longValues;
			float latitudeRand, longitudeRand;
			double actualInstant, timeForced, timeToForcedSearch, attentionInstant, forcedAttentionInstant, forcedAttentionTime;
			bool isForcedSearch, completedSearch;
			//std::vector<attentionElement> elements;
			//attentionElement *maxSaliencyElement;

			//void chooseState ();
			//bool elementAttainable (attentionElement *element, HPoint3D cameraPosition);
			//void updateElements ();

			void point2angle (HPoint3D p, HPoint3D cameraPosition, double *longitude, double *latitude);
			void atenderParalelogramos ();
			void atenderParalelogramo (Parallelogram3D &parallelogram);
			void generarPuntoAleatorio ();

			static const int LIFE_INCREMENT;
			static const int MAX_LIFE;
			static const int TIME_MAINTENANCE;
			static const double ATTENTION_TIME;
			static const double FORCED_ATTENTION_TIME;
	};
}

#endif
