/*
 *  Copyright (C) 1997-2011 JDERobot Developers Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio Vega <julio.vega@urjc.es>
 *
 */

#include "controller.h"

namespace visualMemory {
	const float Controller::V_MOTOR = 250.; // mm/s
	const float Controller::W_MOTOR = 20.; // deg/s

	int Controller::getNumCamerasUsed () {
		return this->numCamerasUsed;
	}

	void Controller::putNumCamerasUsed (int numCameras) {
		this->numCamerasUsed = numCameras;
	}

	std::string Controller::getTypeOfRobot () {
		return this->typeOfRobot;
	}

	void Controller::putTypeOfRobot (std::string typeOfRobot) {
		this->typeOfRobot = typeOfRobot;
	}

	void Controller::initMutex () {
		// Inicialización de mutexes:
		pthread_mutex_init(&this->cameraMutex, NULL);
		pthread_mutex_init(&this->encodersMutex, NULL);
		pthread_mutex_init(&this->ptEncodersMutex, NULL);
		pthread_mutex_init(&this->solisSegmentsMutex1, NULL);
		pthread_mutex_init(&this->predictionsMutex1, NULL);
		pthread_mutex_init(&this->fittedMutex1, NULL);
		pthread_mutex_init(&this->refutedMutex1, NULL);
		pthread_mutex_init(&this->newsMutex1, NULL);
	}

	void Controller::initSensors (RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1) {
		this->jprx = jprx;
		this->cprx1 = cprx1;
	}

	void Controller::initSensors (RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2) {
		this->jprx = jprx;
		this->cprx1 = cprx1;
		this->cprx2 = cprx2;
	}

	void Controller::initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1) {
		this->mprx = mprx;
		this->eprx = eprx;
		this->jprx = jprx;
		this->cprx1 = cprx1;
	}

	void Controller::initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2) {
		this->mprx = mprx;
		this->eprx = eprx;
		this->jprx = jprx;
		this->cprx1 = cprx1;
		this->cprx2 = cprx2;
	}

	void Controller::initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1) {
		this->mprx = mprx;
		this->eprx = eprx;
		this->cprx1 = cprx1;
		this->ptmprx1 = ptmprx1;
		this->pteprx1 = pteprx1;
	}

	void Controller::initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1, jderobot::CameraPrx cprx2, jderobot::Pose3DMotorsPrx ptmprx2, jderobot::Pose3DEncodersPrx pteprx2) {
		this->mprx = mprx;
		this->eprx = eprx;
		this->cprx1 = cprx1;
		this->cprx2 = cprx2;
		this->ptmprx1 = ptmprx1;
		this->pteprx1 = pteprx1;
		this->ptmprx2 = ptmprx2;
		this->pteprx2 = pteprx2;
	}

	void Controller::initVariables () {
		this->showPredictions=false;
		this->showRefuted=false;
		this->showFitted=false;
		this->showNews=false;
		this->showSolis=false;
		this->showPredictions2=false;
		this->showRefuted2=false;
		this->showFitted2=false;
		this->showNews2=false;
		this->showSolis2=false;
		this->imagenComboNumber = 0;
		this->memoryComboNumber = 0;
		this->showInstant = false;
		this->showIteration = false;
		this->showSaliency = false;
		this->typeOfPioneerLoaded = -1;

		this->clickedPixelA.x=0.;
		this->clickedPixelA.y=0.;
		this->clickedPixelA.h=1.;
		this->clickedPixelB.x=0.;
		this->clickedPixelB.y=0.;
		this->clickedPixelB.h=1.;

		this->vergency3Dpoint.X=0.;
		this->vergency3Dpoint.Y=0.;
		this->vergency3Dpoint.Z=0.;
	}

	void Controller::initProgeoCameras () {
		camera *mycameraA, *mycameraB;

		mycameraA = new camera("cameras/calibA-Girafa");
		mycameraB = new camera("cameras/calibB-Girafa");

//		mycameraA = new camera("cameras/calibA");
//		mycameraB = new camera("cameras/calibB");

		myCamA = mycameraA->readConfig();
		myCamB = mycameraB->readConfig();
	}

	void Controller::initialization () {
		this->gladepath = std::string("./visualMemory.glade");
		this->initMutex ();
		this->initVariables ();
		this->initProgeoCameras();
	}

	Controller::Controller(RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1) {
		this->putNumCamerasUsed(1);
		this->initialization ();
		this->initSensors (jprx, cprx1);

		this->typeOfPioneerLoaded = 0;
	}

	Controller::Controller(RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2) {
		this->putNumCamerasUsed(2);
		this->initialization ();
		this->initSensors (jprx, cprx1, cprx2);

		this->typeOfPioneerLoaded = 1;
	}

	Controller::Controller(jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1) {
		this->putNumCamerasUsed(1);
		this->initialization ();
		this->initSensors (mprx, eprx, jprx, cprx1);

		this->typeOfPioneerLoaded = 2;
	}

	Controller::Controller(jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2) {
		this->putNumCamerasUsed(2);
		this->initialization ();
		this->initSensors (mprx, eprx, jprx, cprx1, cprx2);

		this->typeOfPioneerLoaded = 3;
	}

	Controller::Controller(jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1) {
		this->putNumCamerasUsed(1);
		this->initialization ();

		this->initSensors (mprx, eprx, cprx1, ptmprx1, pteprx1);
		this->updateSensors1(); // actualizo mis sensores/actuadores ICE<->Controller
	}

	Controller::Controller(jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1, jderobot::CameraPrx cprx2, jderobot::Pose3DMotorsPrx ptmprx2, jderobot::Pose3DEncodersPrx pteprx2) {
		this->putNumCamerasUsed(2);
		this->initialization ();
		this->initSensors (mprx, eprx, cprx1, ptmprx1, pteprx1, cprx2, ptmprx2, pteprx2);
		this->updateSensors1(); // actualizo mis sensores/actuadores ICE<->Controller
		this->updateSensors2(); // actualizo mis sensores/actuadores ICE<->Controller
	}

	void Controller::setCameraData1() {
		// Get image1
    this->data1 = this->cprx1->getImageData();
    colorspaces::Image::FormatPtr fmt1 = colorspaces::Image::Format::searchFormat(this->data1->description->format);
    if (!fmt1)
			throw "Format not supported";

    this->image1 = new colorspaces::Image (this->data1->description->width,
		       this->data1->description->height,
		       fmt1,
		       &(this->data1->pixelData[0]));
	}

	void Controller::getCameraData1(unsigned char **imagen1) {
	  //pthread_mutex_lock(&this->cameraMutex1); // lock

		// Set image1
		memset (*imagen1, 0, this->data1->description->width*this->data1->description->height*3);
		memcpy (*imagen1, &this->data1->pixelData[0], this->data1->description->width*this->data1->description->height*3);

		//pthread_mutex_unlock(&this->cameraMutex1); // unlock
	}

	void Controller::setCameraData2() {
		// Get image2
    this->data2 = this->cprx2->getImageData();
    colorspaces::Image::FormatPtr fmt2 = colorspaces::Image::Format::searchFormat(this->data2->description->format);
    if (!fmt2)
			throw "Format not supported";

    this->image2 = new colorspaces::Image (this->data2->description->width,
		       this->data2->description->height,
		       fmt2,
		       &(this->data2->pixelData[0]));
	}

	void Controller::getCameraData2(unsigned char **imagen2) {
		//pthread_mutex_lock(&this->cameraMutex2); // lock

		// Set image2
		memset (*imagen2,0,this->data2->description->width*this->data2->description->height*3);
		memcpy (*imagen2, &this->data2->pixelData[0], this->data2->description->width*this->data2->description->height*3);

		//pthread_mutex_unlock(&this->cameraMutex2); // unlock
	}

	void Controller::stopMotors () {
		this->mprx->setV (0.0);
		this->mprx->setW (0.0);
	}

	void Controller::goLeft () {
		this->mprx->setW (W_MOTOR);
	}

	void Controller::goRight () {
		this->mprx->setW (-W_MOTOR);
	}

	void Controller::goUp () {
		this->mprx->setV (V_MOTOR);
	}

	void Controller::goDown () {
		this->mprx->setV (-V_MOTOR);
	}

	void Controller::setV (float v) {
		this->mprx->setV (v);
	}

	void Controller::setW (float w) {
		this->mprx->setW (w);
	}

	int	Controller::checkAndSetGiraffe (float pan, float tilt) {
  	RoboCompJointMotor::MotorGoalPositionList list;
  	RoboCompJointMotor::MotorGoalPosition g;

		/*Set motors to change*/
		g.maxSpeed = 0;
		g.name = "neck";
		if(pan > MAX_PAN)
			g.position = MAX_PAN;
		else if(pan < MIN_PAN)
			g.position = MIN_PAN;
		else
			g.position = pan;
		list.push_back(g);

		g.maxSpeed = 0;
		g.name = "tilt";
		if(tilt > MAX_TILT)
			g.position = MAX_TILT;
		else if(tilt < MIN_TILT)
			g.position = MIN_TILT;
		else
			g.position = tilt;

		list.push_back(g);

		try {
			this->setGiraffePosition (list);
		} catch(...) {
			std::cout << "Error: Couldn't set giraffe looking with angles " << pan << ", " << tilt << std::endl;
		}

		//std::cout << "Giraffe looking with angles " << pan << ", " << tilt << std::endl;

		return 0;
	}

	void Controller::setPT1 (float latitude, float longitude) {
		if (this->getTypeOfRobot() == "Pioneer") {
			this->checkAndSetGiraffe (longitude*DEGTORAD, latitude*DEGTORAD);
		} else {
			jderobot::Pose3DMotorsData* myData;
			myData = new jderobot::Pose3DMotorsData ();
			myData->tilt = latitude;
			myData->pan = longitude;

			this->ptmprx1->setPose3DMotorsData (myData);
		}
	}

	void Controller::setPT2 (float latitude, float longitude) {
		if (this->getTypeOfRobot () == "Pioneer") {
			this->checkAndSetGiraffe (longitude*DEGTORAD, latitude*DEGTORAD);
		} else {
			jderobot::Pose3DMotorsData* myData;
			myData = new jderobot::Pose3DMotorsData ();
			myData->tilt = latitude;
			myData->pan = longitude;

			this->ptmprx2->setPose3DMotorsData (myData);
		}
	}

	void Controller::setGiraffePosition (RoboCompJointMotor::MotorGoalPositionList list) {
		this->jprx->setSyncPosition(list);
	}

	void Controller::getPosition (CvPoint3D32f* myPoint) {
		if ((this->typeOfPioneerLoaded == 0) || (this->typeOfPioneerLoaded == 1)) { // no hay base motora
			myPoint->x = 0.;
			myPoint->y = 0.;
			myPoint->z = 0.;
		} else {
			myPoint->x = this->ed->robotx;
			myPoint->y = this->ed->roboty;
			myPoint->z = this->ed->robottheta;
		}
	}

	void Controller::getPoseCam1 (CvPoint3D32f* myPoint) {
		if ((this->typeOfPioneerLoaded == 0) || (this->typeOfPioneerLoaded == 1)) { // no hay base motora
			myPoint->x = 0.;
			myPoint->y = 0.;
			myPoint->z = 0.;
		} else {
			myPoint->x = this->pted1->x;
			myPoint->y = this->pted1->y;
			myPoint->z = this->pted1->z;
		}
	}

	void Controller::getPoseCam2 (CvPoint3D32f* myPoint) {
		if ((this->typeOfPioneerLoaded == 0) || (this->typeOfPioneerLoaded == 1)) { // no hay base motora
			myPoint->x = 0.;
			myPoint->y = 0.;
			myPoint->z = 0.;
		} else {
			myPoint->x = this->pted2->x;
			myPoint->y = this->pted2->y;
			myPoint->z = this->pted2->z;
		}
	}

	float Controller::getPan1 () {
		if (this->getTypeOfRobot () == "Pioneer") {
			jprx->getAllMotorState(motorsstate);
			return motorsstate["neck"].pos*RADTODEG;
		} else return this->pted1->pan;
	}

	float Controller::getTilt1 () {
		if (this->getTypeOfRobot () == "Pioneer") {
			jprx->getAllMotorState(motorsstate);
			return motorsstate["tilt"].pos*RADTODEG;
		} else return this->pted1->tilt;
	}

	float Controller::getPan2 () {
		if (this->getTypeOfRobot () == "Pioneer") {
			jprx->getAllMotorState(motorsstate);
			return motorsstate["neck"].pos*RADTODEG;
		} else return this->pted2->pan;
	}

	float Controller::getTilt2 () {
		if (this->getTypeOfRobot () == "Pioneer") {
			jprx->getAllMotorState(motorsstate);
			return motorsstate["tilt"].pos*RADTODEG;
		} else return this->pted2->tilt;
	}

	void Controller::setEncoders () {
		pthread_mutex_lock(&this->encodersMutex); // lock
		this->ed = this->eprx->getEncodersData(); // cogemos informacion de los encoders
		pthread_mutex_unlock(&this->encodersMutex); // unlock
	}

	void Controller::setPTEncoders1() {
		pthread_mutex_lock(&this->ptEncodersMutex); // lock
		this->pted1 = this->pteprx1->getPose3DEncodersData();
		pthread_mutex_unlock(&this->ptEncodersMutex); // unlock
	}

	void Controller::setPTEncoders2() {
		pthread_mutex_lock(&this->ptEncodersMutex); // lock
		this->pted2 = this->pteprx2->getPose3DEncodersData();
		pthread_mutex_unlock(&this->ptEncodersMutex); // unlock
	}

	void Controller::updateSensors1 () {
		this->setEncoders(); // cogemos información de los encoders
		this->setPTEncoders1 (); // cogemos información de los ptencoders
		pthread_mutex_lock(&this->cameraMutex); // lock
		this->setCameraData1(); // cogemos información de las cámaras
		pthread_mutex_unlock(&this->cameraMutex); // unlock
	}

	void Controller::updateSensors2 () {
		this->setPTEncoders2 ();

		pthread_mutex_lock(&this->cameraMutex); // lock
		this->setCameraData2(); // cogemos información de las cámaras
		pthread_mutex_unlock(&this->cameraMutex); // unlock
	}

	void Controller::updatePioneerSensors () {
		if (this->typeOfPioneerLoaded == 0) {
			pthread_mutex_lock(&this->cameraMutex); // lock
			this->setCameraData1(); // cogemos información de las cámaras
			pthread_mutex_unlock(&this->cameraMutex); // unlock
		} else if (this->typeOfPioneerLoaded == 1) {
			pthread_mutex_lock(&this->cameraMutex); // lock
			this->setCameraData1(); // cogemos información de las cámaras
			//pthread_mutex_unlock(&this->cameraMutex); // unlock
			//pthread_mutex_lock(&this->cameraMutex); // lock
			this->setCameraData2(); // cogemos información de las cámaras
			pthread_mutex_unlock(&this->cameraMutex); // unlock
		} else if (this->typeOfPioneerLoaded == 2) {
			this->setEncoders(); // cogemos información de los encoders
			this->setPTEncoders1 (); // cogemos información de los ptencoders
			pthread_mutex_lock(&this->cameraMutex); // lock
			this->setCameraData1(); // cogemos información de las cámaras
			pthread_mutex_unlock(&this->cameraMutex); // unlock
		} else if (this->typeOfPioneerLoaded == 3) {
			this->setEncoders(); // cogemos información de los encoders
			this->setPTEncoders1 (); // cogemos información de los ptencoders
			this->setPTEncoders2 (); // cogemos información de los ptencoders
			pthread_mutex_lock(&this->cameraMutex); // lock
			this->setCameraData1(); // cogemos información de las cámaras
			pthread_mutex_unlock(&this->cameraMutex); // unlock
			pthread_mutex_lock(&this->cameraMutex); // lock
			this->setCameraData2(); // cogemos información de las cámaras
			pthread_mutex_unlock(&this->cameraMutex); // unlock
		}
	}

	void Controller::setSolis (bool value) {
		this->showSolis = value;
	}

	void Controller::setPredictions (bool value) {
		this->showPredictions = value;
	}

	void Controller::setRefuted (bool value) {
		this->showRefuted = value;
	}

	void Controller::setFitted (bool value) {
		this->showFitted = value;
	}

	void Controller::setNews (bool value) {
		this->showNews = value;
	}

	bool Controller::getSolis () {
		return (this->showSolis);
	}

	bool Controller::getPredictions () {
		return (this->showPredictions);
	}

	bool Controller::getRefuted () {
		return (this->showRefuted);
	}

	bool Controller::getFitted () {
		return (this->showFitted);
	}

	bool Controller::getNews () {
		return (this->showNews);
	}

	void Controller::setSolis2 (bool value) {
		this->showSolis2 = value;
	}

	void Controller::setPredictions2 (bool value) {
		this->showPredictions2 = value;
	}

	void Controller::setRefuted2 (bool value) {
		this->showRefuted2 = value;
	}

	void Controller::setFitted2 (bool value) {
		this->showFitted2 = value;
	}

	void Controller::setNews2 (bool value) {
		this->showNews2 = value;
	}

	void Controller::setManualVergency (bool value) {
		this->manualVergency = value;
	}

	bool Controller::getManualVergency () {
		return (this->manualVergency);
	}

	bool Controller::getSolis2 () {
		return (this->showSolis2);
	}

	bool Controller::getPredictions2 () {
		return (this->showPredictions2);
	}

	bool Controller::getRefuted2 () {
		return (this->showRefuted2);
	}

	bool Controller::getFitted2 () {
		return (this->showFitted2);
	}

	bool Controller::getNews2 () {
		return (this->showNews2);
	}

	void Controller::setImagenCombo (int value) {
		this->imagenComboNumber = value;
	}

	int Controller::getImagenCombo () {
		return (this->imagenComboNumber);
	}

	void Controller::setMemoryCombo (int value) {
		this->memoryComboNumber = value;
	}

	int Controller::getMemoryCombo () {
		return (this->memoryComboNumber);
	}

	void Controller::setSaliency (bool value) {
		this->showSaliency = value;
	}

	bool Controller::getSaliency () {
		return (this->showSaliency);
	}

	void Controller::setIteration (bool value) {
		this->showIteration = value;
	}

	void Controller::setPlusIteration (bool value) {
		this->plusIteration = value;
	}

	bool Controller::getPlusIteration () {
		return (this->plusIteration);
	}

	bool Controller::getIteration () {
		return (this->showIteration);
	}

	void Controller::updateCamerasPos() {
		gsl_matrix *foaRel, *camMatrix, *foaAbs;
		CvPoint3D32f robotPos, cameraPos;

		foaRel = gsl_matrix_calloc(4,1);
		foaAbs = gsl_matrix_calloc(4,1);

		gsl_matrix_set(foaRel,0,0,1000.0);
		gsl_matrix_set(foaRel,1,0,0.0);
		gsl_matrix_set(foaRel,2,0,0.0);
		gsl_matrix_set(foaRel,3,0,1.0);
		camMatrix = gsl_matrix_calloc(4,4);
		this->getPosition (&robotPos);
		cameraPos.x = 0.; // inicialización
		cameraPos.y = 0.;
		cameraPos.z = 0.;
		this->getPoseCam1 (&cameraPos);

		if ((this->typeOfPioneerLoaded == 0) || (this->typeOfPioneerLoaded == 1)) {
			this->robotPlatform->getCameraMatrix (robotPos, cameraPos, NULL, this->getPan1 (), this->getTilt1 (), 1, camMatrix);
		} else {
			this->robotPlatform->getCameraMatrix (robotPos, cameraPos, this->pted1->roll, this->getPan1 (), this->getTilt1 (), 1, camMatrix);
		}

		gsl_linalg_matmult(camMatrix, foaRel, foaAbs);

		myCamA.position.X = gsl_matrix_get (camMatrix, 0, 3);
		myCamA.position.Y = gsl_matrix_get (camMatrix, 1, 3);
		myCamA.position.Z = gsl_matrix_get (camMatrix, 2, 3);
		myCamA.foa.X=(float)gsl_matrix_get (foaAbs,0, 0);
		myCamA.foa.Y=(float)gsl_matrix_get (foaAbs,1, 0);
		myCamA.foa.Z=(float)gsl_matrix_get (foaAbs,2, 0);

		update_camera_matrix (&myCamA);
		//printf ("camA-----"); printCameraInformation (&myCamA);

		if (this->getNumCamerasUsed () == 2) {
			this->getPoseCam2 (&cameraPos);
			if ((this->typeOfPioneerLoaded == 0) || (this->typeOfPioneerLoaded == 1)) {
				this->robotPlatform->getCameraMatrix (robotPos, cameraPos, NULL, this->getPan2 (), this->getTilt2 (), 2, camMatrix);
			} else {
				this->robotPlatform->getCameraMatrix (robotPos, cameraPos, this->pted2->roll, this->getPan2 (), this->getTilt2 (), 2, camMatrix);
			}

			gsl_linalg_matmult(camMatrix, foaRel, foaAbs);

			myCamB.position.X = gsl_matrix_get (camMatrix, 0, 3);
			myCamB.position.Y = gsl_matrix_get (camMatrix, 1, 3);
			myCamB.position.Z = gsl_matrix_get (camMatrix, 2, 3);
			myCamB.foa.X=(float)gsl_matrix_get (foaAbs,0, 0);
			myCamB.foa.Y=(float)gsl_matrix_get (foaAbs,1, 0);
			myCamB.foa.Z=(float)gsl_matrix_get (foaAbs,2, 0);

			update_camera_matrix (&myCamB);
			//printf ("camB-----"); printCameraInformation (&myCamB);
		}

		gsl_matrix_free(foaRel);
		gsl_matrix_free(foaAbs);
	}

  Controller::~Controller() {}

	void Controller::iteration () {
		if (this->getTypeOfRobot() == "Pioneer") {
			this->updatePioneerSensors ();
		} else {
			this->updateSensors1 (); // cogemos información de nuestros sensores (camaras, encoders...)
			if (this->getNumCamerasUsed () == 2)
				this->updateSensors2 (); // cogemos información de nuestros sensores (camaras, encoders...)
		}

		this->updateCamerasPos ();
	}

  std::string	Controller::getGladePath() {
		return this->gladepath;
  }

	void Controller::printCameraInformation (TPinHoleCamera* actualCamera) {
		printf ("CAMERA INFORMATION\n");
		printf ("==================\n");
		printf ("Position = [%0.2f, %0.2f, %0.2f]\n", actualCamera->position.X, actualCamera->position.Y, actualCamera->position.Z);
		printf ("FOA = [%0.2f, %0.2f, %0.2f]\n", actualCamera->foa.X, actualCamera->foa.Y, actualCamera->foa.Z);
		printf ("fdist = [%0.2f, %0.2f], u0 = %0.2f, v0 = %0.2f, roll = %0.2f\n", actualCamera->fdistx, actualCamera->fdisty, actualCamera->u0, actualCamera->v0, actualCamera->roll);
		printf ("K matrix:\n");
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->k11, actualCamera->k12, actualCamera->k13, actualCamera->k14);
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->k21, actualCamera->k22, actualCamera->k23, actualCamera->k24);
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->k31, actualCamera->k32, actualCamera->k33, actualCamera->k34);
		printf ("RT matrix:\n");
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt11, actualCamera->rt12, actualCamera->rt13, actualCamera->rt14);
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt21, actualCamera->rt22, actualCamera->rt23, actualCamera->rt24);
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt31, actualCamera->rt32, actualCamera->rt33, actualCamera->rt34);
		printf ("%0.2f %0.2f %0.2f %0.2f\n", actualCamera->rt41, actualCamera->rt42, actualCamera->rt43, actualCamera->rt44);
	}

	void Controller::printJointMotorParams () {
		motorsparams = jprx->getAllMotorParams();

		cout << "Motors params:" << endl;
		for(vector<RoboCompJointMotor::MotorParams>::iterator it = motorsparams.begin(); it != motorsparams.end(); it++) {
			cout << endl;
			cout << "Name: " << (*it).name << endl;
			cout << "Id: " << (int) (*it).busId << endl;
			cout << "minPos: " << (*it).minPos << endl;
			cout << "maxPos: " << (*it).maxPos << endl;
			cout << "maxVel: " << (*it).maxVelocity << endl;
			cout << "zeroPos: " << (*it).zeroPos << endl;
			cout << "inverted: " << (*it).invertedSign << endl;
		}
	}

	bool Controller::giraffeIsMoving () {
		this->jprx->getAllMotorState(this->motorsstate); // Get giraffe motors state
		return ((motorsstate["neck"].isMoving) || (motorsstate["tilt"].isMoving));
	}
} // namespace

