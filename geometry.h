/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *           Eduardo Perdices (eperdices [at] gsyc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jderobot.org/index.php/robotvision
 *
 */

#ifndef VISIONLIBRARY_GEOMETRY_H
#define VISIONLIBRARY_GEOMETRY_H

#include <iostream>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "structs.h"

#ifndef G_SQUARE
#define G_SQUARE(a) ( (a) * (a) )
#endif

#define MAX_PAR 1.22
#define MAX_DIST 40
#define MAX_PAR_2D 15
#define MAX_DIST_2D 15

namespace visionLibrary {
  class geometry {

		public:
			geometry ();
		  virtual ~geometry ();

			// TODO: estas funcionas han sido añadidas por mí
			static void segment2Dto3D (Segment2D segment2D, Segment3D *segment3D);
			static void segment3Dto2D (Segment3D segment3D, Segment2D *segment2D);
			static bool overlappedSegments (Segment3D segment, HPoint3D proy1, HPoint3D proy2, int ubi1, int ubi2, float dist1, float dist2, bool isIn2D);

			/*Distance between two points in 2D*/
			static double distanceBetweenPoints2D(int x1, int y1, int x2, int y2);
			static double distanceBetweenPoints2D(double x1, double y1, double x2, double y2);
			static double distanceBetweenPoints2D(HPoint2D p1, HPoint2D p2);

			/*Distance between two points in 3D*/
			static double distanceBetweenPoints3D(HPoint3D p1, HPoint3D p2);

			/*Distance between two points in 2D in a concrete axis*/
			static double calcDistanceAxis(double x1, double y1, double x2, double y2, double alpha);
			static double calcDistanceAxis(HPoint3D p1, HPoint3D p2, double alpha);
			static double calcDistanceAxis(HPoint3D p1, HPoint3D p2, double cosa, double sina);

			/*Calc the positive angle of a vector*/
			static double calcVectorAngle(double x1, double y1, double x2, double y2);

			/*Calc a 2D vector from 2 2D points*/
			static void calcVector2D(HPoint2D p1, HPoint2D p2, HPoint3D &v);
			static void calcVector2D(HPoint3D p1, HPoint3D p2, HPoint3D &v);

			/*Calc a 2D normal vector from 3 2D points (normal vector of p1-p2 in p3)*/
			static void calcNormalVector2D(HPoint2D p1, HPoint2D p2, HPoint2D p3, HPoint3D &v);
			static void calcNormalVector2D(HPoint3D p1, HPoint3D p2, HPoint3D p3, HPoint3D &v);

			/*Calc intersection between 2 2D vectors in a 2D point*/
			static void calcIntersection2D(HPoint3D v1, HPoint3D v2, HPoint2D &p);
			static void calcIntersection2D(HPoint3D v1, HPoint3D v2, HPoint3D &p);

			/*Return true if the angles of two vectors can be considerated parallel*/
			static bool areVectorsParallel(double alpha1, double alpha2, double threshold);

			/*Return true if the projection of the point P is "inside" the segment A-B*/
			static bool isPointInsideLine(double px, double py, double ax, double ay, double bx, double by);

			/*Return a point belonging to the vector A-B*/
			static void getPointFromVector(int &px, int &py, int ax, int ay, int bx, int by, double u);
			static void getPointFromVector(double &px, double &py, double ax, double ay, double bx, double by, double u);

			/*Return the intersection between a circle (radius and P_central) and a vector*/
			static void calIntersectionCircleVector(HPoint3D v, HPoint3D p_c, double r, HPoint3D &int1, HPoint3D &int2);

			/*Return the intersection between two points (A and B) on 3D, and the ground*/
			static void lineGroundIntersection (HPoint3D A, HPoint3D B, HPoint3D &intersectionPoint);	

			/*Try to merge two segments, return true if merged and update segment1.
			Params: max parallel angle (rads), max horizontal distance (pixs), max perpendicular distance (pixs)*/
			static bool mergeSegments(Segment2D &segment1, Segment2D segment2, double max_parallel = 0.3, double max_distance = 10.0, double max_normal_distance = 10.0);

			/*Merge two segments to obtain the longest one*/
			static void getMaximizedSegment(HPoint3D seg1Start, HPoint3D seg1End, HPoint3D seg2Start, HPoint3D seg2End, HPoint3D &startPoint, HPoint3D &endPoint);

			static int getMaximizedParallelogram (Parallelogram3D par1, Parallelogram3D par2);

			/*Length of a segment*/
			static double segmentLength (Segment3D segment);
			static double segmentLength (Segment2D segment);

			/*Distance between a point and a line. Check if the points is inside the line in "isInside" parameter*/
			static double distancePointLine (HPoint2D point, Segment2D segment, HPoint2D &intersection, int &isInside);
			static double distancePointLine (HPoint3D point, Segment3D segment, HPoint3D &intersection, int &isInside);

			/*Check if two segments are equal*/
			static bool areTheSameSegment2D (Segment2D s1, Segment2D s2);
			static bool areTheSameSegment3D (Segment3D s1, Segment3D s2);
			static void createSegment3D (HPoint3D startPoint, HPoint3D endPoint, Segment3D &segment);

			/*Check if two parallelograms are equal*/
			static bool areTheSameParallelogram (Parallelogram3D par1, Parallelogram3D par2, float threshold);

			static int haveACommonVertex (Segment3D s1, Segment3D s2, Parallelogram3D *square);
			static bool haveACommonVertex (Parallelogram3D par1, Parallelogram3D par2);
			static float angleSquare (Parallelogram3D square);
			static void GetLastPointSquare (Parallelogram3D* square);

			/*Calculate centroid of a polygon*/
			static void getPolygonCentroid (Parallelogram3D &parallelogram);
			static bool LineIntersect (float Ax, float Ay, float Bx, float By, float Cx, float Cy, float Dx, float Dy, 	float *X, float *Y);

		private:
			static const double GEOMETRY_PI;
			static const double GEOMETRY_PI_2;
			static const double GEOMETRY_SQRT_2;
			static const float COMMON_VERTEX_THRESHOLD;
	};
}

#endif
