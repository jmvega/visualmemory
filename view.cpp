/*
 *  Copyright (C) 1997-2011 JDERobot Developers Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio Vega <julio.vega@urjc.es>
 *
 */

#include "view.h"

namespace visualMemory {
	void View::loadViewer () {
		/*Set image*/
		//this->imgSrc1 = new char[IMAGE_WIDTH * IMAGE_HEIGHT * IMAGE_CHANNELS];
		//this->imgSrc2 = new char[IMAGE_WIDTH * IMAGE_HEIGHT * IMAGE_CHANNELS];
		pthread_mutex_init(&mutex_image, NULL);

		/*Init OpenGL*/
		if(!Gtk::GL::init_check(NULL, NULL))	{
			std::cerr << "Couldn't initialize GL\n";
			std::exit(1);
		}

    std::cout << "Loading glade\n";
    refXml = Gnome::Glade::Xml::create(this->controller->getGladePath());

		// Ventanas
    refXml->get_widget("mainwindow", mainwindow);
    refXml->get_widget("openglwindow", openglwindow);

		// Widgets ventana principal
    refXml->get_widget("image1",gtk_image1);
    refXml->get_widget("image2",gtk_image2);
    refXml->get_widget("eventbox_left",eventbox_left);
    refXml->get_widget("eventbox_right",eventbox_right);
		refXml->get_widget("openglButton", openglButton);
    refXml->get_widget("exitButton", exitButton);
    refXml->get_widget("iterationButton", iterationButton);
    refXml->get_widget("iterationButton1", iterationButton1);
    refXml->get_widget("imagenCombo", imagenCombo);
		refXml->get_widget("solisButton", solisButton);
    refXml->get_widget("predictionsButton", predictionsButton);
    refXml->get_widget("refutedButton", refutedButton);
		refXml->get_widget("fittedButton", fittedButton);
		refXml->get_widget("newsButton", newsButton);
		refXml->get_widget("solisButton2", solisButton2);
    refXml->get_widget("predictionsButton2", predictionsButton2);
    refXml->get_widget("refutedButton2", refutedButton2);
		refXml->get_widget("fittedButton2", fittedButton2);
		refXml->get_widget("newsButton2", newsButton2);
		refXml->get_widget("vergencyButton", vergencyButton);

		// Widgets ventana OpenGL
    refXml->get_widget_derived("world",this->world); // mundo OpenGL
    refXml->get_widget("camera1Button", camera1Button);
    refXml->get_widget("camera2Button", camera2Button);
    refXml->get_widget("camera3Button", camera3Button);
    refXml->get_widget("camera4Button", camera4Button);
    refXml->get_widget("pioneerCameraButton", pioneerCameraButton);
    refXml->get_widget("memoryCombo", memoryCombo);
    refXml->get_widget("saliencyButton", saliencyButton);

		pthread_mutex_init(&mutex_image, NULL);
	}

	void View::attachViewerEvents () {
		// Widgets ventana principal
		eventbox_left->add_events(Gdk::BUTTON_PRESS_MASK);
		eventbox_left->signal_button_press_event().connect(sigc::mem_fun(this, &View::on_left_clicked));
		eventbox_right->add_events(Gdk::BUTTON_PRESS_MASK);
		eventbox_right->signal_button_press_event().connect(sigc::mem_fun(this, &View::on_right_clicked));
		imagenCombo->signal_changed().connect(sigc::mem_fun(this,&View::imagenCombo_changed));
		openglButton->signal_clicked().connect(sigc::mem_fun(this,&View::openglButton_toggled));
		exitButton->signal_clicked().connect(sigc::mem_fun(this,&View::exitButton_clicked));
		iterationButton->signal_clicked().connect(sigc::mem_fun(this,&View::iterationButton_toggled));
		iterationButton1->signal_clicked().connect(sigc::mem_fun(this,&View::iterationButton1_clicked));
		solisButton->signal_clicked().connect(sigc::mem_fun(this,&View::solisButton_toggled));
		predictionsButton->signal_clicked().connect(sigc::mem_fun(this,&View::predictionsButton_toggled));
		refutedButton->signal_clicked().connect(sigc::mem_fun(this,&View::refutedButton_toggled));
		fittedButton->signal_clicked().connect(sigc::mem_fun(this,&View::fittedButton_toggled));
		newsButton->signal_clicked().connect(sigc::mem_fun(this,&View::newsButton_toggled));
		solisButton2->signal_clicked().connect(sigc::mem_fun(this,&View::solisButton2_toggled));
		predictionsButton2->signal_clicked().connect(sigc::mem_fun(this,&View::predictionsButton2_toggled));
		refutedButton2->signal_clicked().connect(sigc::mem_fun(this,&View::refutedButton2_toggled));
		fittedButton2->signal_clicked().connect(sigc::mem_fun(this,&View::fittedButton2_toggled));
		newsButton2->signal_clicked().connect(sigc::mem_fun(this,&View::newsButton2_toggled));
		vergencyButton->signal_clicked().connect(sigc::mem_fun(this,&View::vergencyButton_toggled));

		// Widgets ventana OpenGL
		camera1Button->signal_clicked().connect(sigc::mem_fun(this,&View::camera1Button_clicked));
		camera2Button->signal_clicked().connect(sigc::mem_fun(this,&View::camera2Button_clicked));
		camera3Button->signal_clicked().connect(sigc::mem_fun(this,&View::camera3Button_clicked));
		camera4Button->signal_clicked().connect(sigc::mem_fun(this,&View::camera4Button_clicked));
		pioneerCameraButton->signal_clicked().connect(sigc::mem_fun(this,&View::pioneerCameraButton_clicked));
		memoryCombo->signal_changed().connect(sigc::mem_fun(this,&View::memoryCombo_changed));
		saliencyButton->signal_clicked().connect(sigc::mem_fun(this,&View::saliencyButton_toggled));
	}

	View::View (Controller* controller): gtkmain(0,0) {
		this->controller = controller;
		this->isFollowing = false;
		//this->showOpenGL = true;

		this->loadViewer ();
		this->attachViewerEvents ();

		this->world->setView (this);

		this->solisButton->set_active (true);
		this->openglButton->set_active (true);
		this->imagenCombo->set_active (false);
		this->memoryCombo->set_active (1); // opción de mostrar todo "Everything"
		this->iterationButton->set_active (true);
		this->vergencyButton->set_active (true);

		/*Show window. Note: Set window visibility to false in Glade, otherwise opengl won't work*/
		mainwindow->show();
	}

	View::~View() {
		delete this->controller;
		delete this->world;
	}

	/** prepare2draw ************************************************************************
	* Prepare an image to draw it with a GtkImage in BGR format with 3 pixels per byte.	*
	*	src: source image								*
	*	dest: destiny image. It has to have the same size as the source image and	*
	*	      3 bytes per pixel.
	*****************************************************************************************/
	void View::prepare2draw (IplImage &image) {
		int i;
		int pos;
		char temp;

		for (i=0; i<image.width*image.height; i++) {
			pos = i*3;
			temp = image.imageData[pos+2];
			image.imageData[pos+2]=image.imageData[pos];
			image.imageData[pos]=temp;
		}
	}

	void View::color2bn (colorspaces::Image& image) {
		int i;
		int pos;
		unsigned char temp;

		for (i=0; i<image.width*image.height; i++) {
			pos = i*3;
			temp = image.data[pos]*0.3 + image.data[pos+1]*0.59 + image.data[pos+2]*0.11;
			image.data[pos]=temp;
			image.data[pos+1]=temp;
			image.data[pos+2]=temp;
		}
	}

	void View::color2black (colorspaces::Image& image) {
		int i;
		int pos;

		for (i=0; i<image.width*image.height; i++) {
			pos = i*3;
			image.data[pos]=0;
			image.data[pos+1]=0;
			image.data[pos+2]=0;
		}
	}

	void View::iteration () {
		pthread_mutex_lock(&mutex_image);
		this->updateStructures (); // nos guardamos las estructuras (laser, camaras, encoders...) para pintado
		pthread_mutex_unlock(&mutex_image);
	}

  void View::display() {
		if (this->isFollowing) // cámara desde la que estamos visualizando
			this->world->setToPioneerCamera();

		if (this->showOpenGL) { // visualizamos OpenGL
			openglwindow->show();
		} else {
			openglwindow->hide();
		}

		pthread_mutex_lock(&mutex_image);
    mainwindow->resize(1,1);
    while (gtkmain.events_pending())
      gtkmain.iteration();
		pthread_mutex_unlock(&mutex_image);
  }

	void View::tratarImagen (colorspaces::Image& image) {
		if (this->imagenCombo->get_active_row_number() == 0) { // imagen a color
			// no hacemos nada
		} else if (this->imagenCombo->get_active_row_number() == 1) { // imgen B/N
			this->color2bn (image);
		} else { // imagen en negro
			this->color2black (image);
		}
	}

	void View::setCamara1 (colorspaces::Image& image) {
		this->tratarImagen (image);

		CvPoint p1, p2, p3, p4;
		IplImage src = image;
		std::vector<Segment2D>::iterator it;
		std::vector<Parallelogram2D>::iterator it2;

		if (this->controller->getSolis ()) {
		  for(it = this->controller->solisSegments1.begin(); it != this->controller->solisSegments1.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(255,255,0), 1, 8);
			}
		}

		if (this->controller->getPredictions ()) {
		  for(it = this->controller->predictedSegments1.begin(); it != this->controller->predictedSegments1.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(255,0,0), 1, 8);
			}

		  for(it2 = this->controller->predictedParallelograms1.begin(); it2 != this->controller->predictedParallelograms1.end(); it2++) {
				p1.x = (*it2).p1.x;
				p1.y = (*it2).p1.y;
				p2.x = (*it2).p2.x;
				p2.y = (*it2).p2.y;
				p3.x = (*it2).p3.x;
				p3.y = (*it2).p3.y;
				p4.x = (*it2).p4.x;
				p4.y = (*it2).p4.y;

				cvLine (&src, p2, p1, CV_RGB(255,0,0), 2, 8);
				cvLine (&src, p1, p3, CV_RGB(255,0,0), 2, 8);
				cvLine (&src, p3, p4, CV_RGB(255,0,0), 2, 8);
				cvLine (&src, p4, p2, CV_RGB(255,0,0), 2, 8);
			}
		}

		if (this->controller->getNews ()) {
		  for(it = this->controller->newSegments1.begin(); it != this->controller->newSegments1.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(0,255,255), 1, 8);
			}

		  for(it2 = this->controller->newParallelograms1.begin(); it2 != this->controller->newParallelograms1.end(); it2++) {
				p1.x = (*it2).p1.x;
				p1.y = (*it2).p1.y;
				p2.x = (*it2).p2.x;
				p2.y = (*it2).p2.y;
				p3.x = (*it2).p3.x;
				p3.y = (*it2).p3.y;
				p4.x = (*it2).p4.x;
				p4.y = (*it2).p4.y;

				cvLine (&src, p2, p1, CV_RGB(0,255,255), 2, 8);
				cvLine (&src, p1, p3, CV_RGB(0,255,255), 2, 8);
				cvLine (&src, p3, p4, CV_RGB(0,255,255), 2, 8);
				cvLine (&src, p4, p2, CV_RGB(0,255,255), 2, 8);
			}
		}

		if (this->controller->getRefuted ()) {
		  for(it = this->controller->refutedSegments1.begin(); it != this->controller->refutedSegments1.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(0,0,255), 1, 8);
			}

		  for(it2 = this->controller->refutedParallelograms1.begin(); it2 != this->controller->refutedParallelograms1.end(); it2++) {
				p1.x = (*it2).p1.x;
				p1.y = (*it2).p1.y;
				p2.x = (*it2).p2.x;
				p2.y = (*it2).p2.y;
				p3.x = (*it2).p3.x;
				p3.y = (*it2).p3.y;
				p4.x = (*it2).p4.x;
				p4.y = (*it2).p4.y;

				cvLine (&src, p2, p1, CV_RGB(0,0,255), 2, 8);
				cvLine (&src, p1, p3, CV_RGB(0,0,255), 2, 8);
				cvLine (&src, p3, p4, CV_RGB(0,0,255), 2, 8);
				cvLine (&src, p4, p2, CV_RGB(0,0,255), 2, 8);
			}
		}

		if (this->controller->getFitted ()) {
		  for(it = this->controller->fittedSegments1.begin(); it != this->controller->fittedSegments1.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(0,255,0), 1, 8);
			}

		  for(it2 = this->controller->fittedParallelograms1.begin(); it2 != this->controller->fittedParallelograms1.end(); it2++) {
				p1.x = (*it2).p1.x;
				p1.y = (*it2).p1.y;
				p2.x = (*it2).p2.x;
				p2.y = (*it2).p2.y;
				p3.x = (*it2).p3.x;
				p3.y = (*it2).p3.y;
				p4.x = (*it2).p4.x;
				p4.y = (*it2).p4.y;

				cvLine (&src, p2, p1, CV_RGB(0,255,0), 2, 8);
				cvLine (&src, p1, p3, CV_RGB(0,255,0), 2, 8);
				cvLine (&src, p3, p4, CV_RGB(0,255,0), 2, 8);
				cvLine (&src, p4, p2, CV_RGB(0,255,0), 2, 8);
			}
		}

		colorspaces::ImageRGB8 img_rgb888(image); // conversion will happen if needed

		Glib::RefPtr<Gdk::Pixbuf> imgBuff = Gdk::Pixbuf::create_from_data((const guint8*)img_rgb888.data,
				    Gdk::COLORSPACE_RGB,
				    false,
				    8,
				    img_rgb888.width,
				    img_rgb888.height,
				    img_rgb888.step);

	  gtk_image1->clear();
	  gtk_image1->set(imgBuff);
	}

	void View::setCamara2 (colorspaces::Image& image) {
		this->tratarImagen (image);

		CvPoint p1, p2;
		IplImage src = image;
		std::vector<Segment2D>::iterator it;

		if (this->controller->getSolis ()) {
		  for(it = this->controller->solisSegments2.begin(); it != this->controller->solisSegments2.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(255,255,0), 1, 8);
			}
		}

		if (this->controller->getPredictions ()) {
		  for(it = this->controller->predictedSegments2.begin(); it != this->controller->predictedSegments2.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(255,0,0), 2, 8);
			}
		}

		if (this->controller->getNews ()) {
		  for(it = this->controller->newSegments2.begin(); it != this->controller->newSegments2.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(0,255,255), 2, 8);
			}
		}

		if (this->controller->getRefuted ()) {
		  for(it = this->controller->refutedSegments2.begin(); it != this->controller->refutedSegments2.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(0,0,255), 2, 8);
			}
		}

		if (this->controller->getFitted ()) {
		  for(it = this->controller->fittedSegments2.begin(); it != this->controller->fittedSegments2.end(); it++) {
				p1.x = (*it).start.x;
				p1.y = (*it).start.y;
				p2.x = (*it).end.x;
				p2.y = (*it).end.y;

				cvLine (&src, p1, p2, CV_RGB(0,255,0), 2, 8);
			}
		}

		colorspaces::ImageRGB8 img_rgb888(image); // conversion will happen if needed

		Glib::RefPtr<Gdk::Pixbuf> imgBuff = Gdk::Pixbuf::create_from_data((const guint8*)img_rgb888.data,
				    Gdk::COLORSPACE_RGB,
				    false,
				    8,
				    img_rgb888.width,
				    img_rgb888.height,
				    img_rgb888.step);

	  gtk_image2->clear();
	  gtk_image2->set(imgBuff);
	}

	void View::updateStructures () {
		// Tratamiento de imágenes
		pthread_mutex_lock(&mutex_image);
		pthread_mutex_lock(&this->controller->cameraMutex); // lock
    colorspaces::Image::FormatPtr fmt1 = colorspaces::Image::Format::searchFormat(this->controller->data1->description->format);
    if (!fmt1)
			throw "Format not supported";

    this->image1 = new colorspaces::Image (this->controller->data1->description->width,
		       this->controller->data1->description->height,
		       fmt1,
		       &(this->controller->data1->pixelData[0]));

		setCamara1(*this->image1);

		if (this->controller->getNumCamerasUsed () == 2) {
		  colorspaces::Image::FormatPtr fmt2 = colorspaces::Image::Format::searchFormat(this->controller->data2->description->format);
		  if (!fmt2)
				throw "Format not supported";

		  this->image2 = new colorspaces::Image (this->controller->data2->description->width,
				     this->controller->data2->description->height,
				     fmt2,
				     &(this->controller->data2->pixelData[0]));

			setCamara2(*this->image2);
		}
		pthread_mutex_unlock(&this->controller->cameraMutex); // unlock
		pthread_mutex_unlock(&mutex_image);
	}

  bool View::isVisible() {
    return mainwindow->is_visible();
  }

	void View::camera1Button_clicked() {
		this->isFollowing = false;
		this->world->setToCamera1();
	}

	void View::camera2Button_clicked() {
		this->isFollowing = false;
		this->world->setToCamera2();
	}

	void View::camera3Button_clicked() {
		this->isFollowing = false;
		this->world->setToCamera3();
	}

	void View::camera4Button_clicked() {
		this->isFollowing = false;
		this->world->setToCamera4();
	}

	void View::pioneerCameraButton_clicked() {
		this->isFollowing = true;
		this->world->setToPioneerCamera();
	}

	void View::saliencyButton_toggled () {
		this->controller->setSaliency(this->saliencyButton->get_active ());
	}

	void View::memoryCombo_changed () {
		this->controller->setMemoryCombo(this->memoryCombo->get_active_row_number());
	}

	void View::exitButton_clicked() {
		mainwindow->hide();
		exit (0);
	}

	void View::iterationButton_toggled() {
		this->controller->setIteration(this->iterationButton->get_active ());
	}

	void View::iterationButton1_clicked() {
		if (this->iterationButton->get_active()) // sólo funcionará si está activo el "modo iteration"
			this->controller->setPlusIteration(true);
	}

	void View::openglButton_toggled () {
		if (this->openglButton->get_active ())
			showOpenGL=true;
		else
			showOpenGL=false;
	}

	void View::solisButton_toggled () {
		this->controller->setSolis(this->solisButton->get_active ());
	}

	void View::predictionsButton_toggled () {
		this->controller->setPredictions(this->predictionsButton->get_active ());
	}

	void View::refutedButton_toggled () {
		this->controller->setRefuted(this->refutedButton->get_active ());
	}

	void View::fittedButton_toggled () {
		this->controller->setFitted(this->fittedButton->get_active ());
	}

	void View::newsButton_toggled () {
		this->controller->setNews(this->newsButton->get_active ());
	}

	void View::solisButton2_toggled () {
		this->controller->setSolis2(this->solisButton2->get_active ());
	}

	void View::predictionsButton2_toggled () {
		this->controller->setPredictions2(this->predictionsButton2->get_active ());
	}

	void View::refutedButton2_toggled () {
		this->controller->setRefuted2(this->refutedButton2->get_active ());
	}

	void View::fittedButton2_toggled () {
		this->controller->setFitted2(this->fittedButton2->get_active ());
	}

	void View::newsButton2_toggled () {
		this->controller->setNews2(this->newsButton2->get_active ());
	}

	void View::vergencyButton_toggled () {
		this->controller->setManualVergency(this->vergencyButton->get_active ());
	}

	void View::imagenCombo_changed () {
		this->controller->setImagenCombo(this->imagenCombo->get_active_row_number());
	}

	bool View::on_right_clicked(GdkEventButton * event){
		gint x,y;
		gdk_window_at_pointer(&x,&y);

		this->controller->clickedPixelB.x=x;
		this->controller->clickedPixelB.y=y;
		this->controller->clickedPixelB.h=1.0;
		printf ("Clicado pixel [%f, %f] en imagen derecha\n", this->controller->clickedPixelB.x, this->controller->clickedPixelB.y);
		return true;
	}

	bool View::on_left_clicked(GdkEventButton * event){
		gint x,y;
		gdk_window_at_pointer(&x,&y);

		this->controller->clickedPixelA.x=x;
		this->controller->clickedPixelA.y=y;
		this->controller->clickedPixelA.h=1.0;
		printf ("Clicado pixel [%f, %f] en imagen izquierda\n", this->controller->clickedPixelA.x, this->controller->clickedPixelA.y);
		return true;
	}
} // namespace
