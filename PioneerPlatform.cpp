/*
 *  Copyright (C) 2011 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "PioneerPlatform.h"

namespace visualMemory {
	PioneerPlatform::PioneerPlatform () {}
	void PioneerPlatform::getCameraMatrix (CvPoint3D32f robotPos, CvPoint3D32f cameraPos, float roll, float pan, float tilt, int idCam, gsl_matrix *camMatrix) {
		gsl_matrix *robotRT, *centroRT, *panRT, *tiltRT, *centroCam, *temp1, *temp2;

		robotRT = gsl_matrix_calloc(4,4);
		centroRT = gsl_matrix_calloc(4,4);
		panRT = gsl_matrix_calloc(4,4);
		tiltRT = gsl_matrix_calloc(4,4);
		centroCam = gsl_matrix_calloc(4,4);
		temp1 = gsl_matrix_calloc(4,4);
		temp2 = gsl_matrix_calloc(4,4);

		gsl_matrix_set(robotRT,0,0,cos(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,0,1,-sin(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,0,2,0.);
		gsl_matrix_set(robotRT,0,3,robotPos.x);
		gsl_matrix_set(robotRT,1,0,sin(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,1,1,cos(robotPos.z*DEGTORAD));
		gsl_matrix_set(robotRT,1,2,0.);
		gsl_matrix_set(robotRT,1,3,robotPos.y);
		gsl_matrix_set(robotRT,2,0,0.);
		gsl_matrix_set(robotRT,2,1,0.);
		gsl_matrix_set(robotRT,2,2,1.);
// TODO: OJO cuando ancle el cuello, tengo que considerar la altura de la base del robot!!
		gsl_matrix_set(robotRT,2,3,0.); // Z de la base robot la considero 0
		gsl_matrix_set(robotRT,3,0,0.);
		gsl_matrix_set(robotRT,3,1,0.);
		gsl_matrix_set(robotRT,3,2,0.);
		gsl_matrix_set(robotRT,3,3,1.0);

		gsl_matrix_set(centroRT,0,0,1.);
		gsl_matrix_set(centroRT,0,1,0.);
		gsl_matrix_set(centroRT,0,2,0.);
		gsl_matrix_set(centroRT,0,3,0.);
		gsl_matrix_set(centroRT,1,0,0.);
		gsl_matrix_set(centroRT,1,1,1.);
		gsl_matrix_set(centroRT,1,2,0.);
		gsl_matrix_set(centroRT,1,3,0.);
		gsl_matrix_set(centroRT,2,0,0.);
		gsl_matrix_set(centroRT,2,1,0.);
		gsl_matrix_set(centroRT,2,2,1.);
		gsl_matrix_set(centroRT,2,3,175.); // altura hasta el eje de rotación de la jirafa
		gsl_matrix_set(centroRT,3,0,0.);
		gsl_matrix_set(centroRT,3,1,0.);
		gsl_matrix_set(centroRT,3,2,0.);
		gsl_matrix_set(centroRT,3,3,1.);

		//pan
		gsl_matrix_set(panRT,0,0,cos(-pan*DEGTORAD));
		gsl_matrix_set(panRT,0,1,sin(-pan*DEGTORAD));
		gsl_matrix_set(panRT,0,2,0.);
		gsl_matrix_set(panRT,0,3,0.); // translacion en x
		gsl_matrix_set(panRT,1,0,-sin(-pan*DEGTORAD));
		gsl_matrix_set(panRT,1,1,cos(-pan*DEGTORAD));
		gsl_matrix_set(panRT,1,2,0.);
		gsl_matrix_set(panRT,1,3,0.);// translacion en y
		gsl_matrix_set(panRT,2,0,0.);
		gsl_matrix_set(panRT,2,1,0.);
		gsl_matrix_set(panRT,2,2,1.);
		gsl_matrix_set(panRT,2,3,0.); // Altura del centro optico de la camara respecto del suelo. 
		gsl_matrix_set(panRT,3,0,0.);
		gsl_matrix_set(panRT,3,1,0.);
		gsl_matrix_set(panRT,3,2,0.);
		gsl_matrix_set(panRT,3,3,1.0);

		//tilt
		gsl_matrix_set(tiltRT,0,0,cos(tilt*DEGTORAD));
		gsl_matrix_set(tiltRT,0,1,0.);
		gsl_matrix_set(tiltRT,0,2,-sin(tilt*DEGTORAD));
		gsl_matrix_set(tiltRT,0,3,0.); 
		gsl_matrix_set(tiltRT,1,0,0.);
		gsl_matrix_set(tiltRT,1,1,1.);
		gsl_matrix_set(tiltRT,1,2,0.);
		gsl_matrix_set(tiltRT,1,3,0.);
		gsl_matrix_set(tiltRT,2,0,sin(tilt*DEGTORAD));
		gsl_matrix_set(tiltRT,2,1,0.);
		gsl_matrix_set(tiltRT,2,2,cos(tilt*DEGTORAD));
		gsl_matrix_set(tiltRT,2,3,0.); 
		gsl_matrix_set(tiltRT,3,0,0.);
		gsl_matrix_set(tiltRT,3,1,0.);
		gsl_matrix_set(tiltRT,3,2,0.);
		gsl_matrix_set(tiltRT,3,3,1.0);

		if (idCam == 1) { // posición de la cámara 1 respecto al eje de rotación tilt
			gsl_matrix_set(centroCam,0,0,1.);
			gsl_matrix_set(centroCam,0,1,0.);
			gsl_matrix_set(centroCam,0,2,0.);
			gsl_matrix_set(centroCam,0,3,50.);
			gsl_matrix_set(centroCam,1,0,0.);
			gsl_matrix_set(centroCam,1,1,1.);
			gsl_matrix_set(centroCam,1,2,0.);
			gsl_matrix_set(centroCam,1,3,100.);
			gsl_matrix_set(centroCam,2,0,0.);
			gsl_matrix_set(centroCam,2,1,0.);
			gsl_matrix_set(centroCam,2,2,1.);
			gsl_matrix_set(centroCam,2,3,60.);
			gsl_matrix_set(centroCam,3,0,0.);
			gsl_matrix_set(centroCam,3,1,0.);
			gsl_matrix_set(centroCam,3,2,0.);
			gsl_matrix_set(centroCam,3,3,1.);
		} else if (idCam == 2) { // posición de la cámara 2 respecto al eje de rotación tilt
			gsl_matrix_set(centroCam,0,0,1.);
			gsl_matrix_set(centroCam,0,1,0.);
			gsl_matrix_set(centroCam,0,2,0.);
			gsl_matrix_set(centroCam,0,3,50.);
			gsl_matrix_set(centroCam,1,0,0.);
			gsl_matrix_set(centroCam,1,1,1.);
			gsl_matrix_set(centroCam,1,2,0.);
			gsl_matrix_set(centroCam,1,3,-100.);
			gsl_matrix_set(centroCam,2,0,0.);
			gsl_matrix_set(centroCam,2,1,0.);
			gsl_matrix_set(centroCam,2,2,1.);
			gsl_matrix_set(centroCam,2,3,60.);
			gsl_matrix_set(centroCam,3,0,0.);
			gsl_matrix_set(centroCam,3,1,0.);
			gsl_matrix_set(centroCam,3,2,0.);
			gsl_matrix_set(centroCam,3,3,1.);
		}

		gsl_linalg_matmult (robotRT, centroRT, temp1);
		gsl_linalg_matmult (temp1, panRT, temp2);
		gsl_linalg_matmult (temp2, tiltRT, temp1);
		gsl_linalg_matmult (temp1, centroCam, camMatrix);

		gsl_matrix_free(robotRT);
		gsl_matrix_free(centroRT);
		gsl_matrix_free(panRT);
		gsl_matrix_free(tiltRT);
		gsl_matrix_free(temp1);
		gsl_matrix_free(temp2);
	}
	PioneerPlatform::~PioneerPlatform () {}
}
