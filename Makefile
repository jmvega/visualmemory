JDEROBOTDIR = /usr/local
CXXFLAGS = -I. -I$(JDEROBOTDIR)/include/jderobot -I$(JDEROBOTDIR)/include/jderobot/jderobot -I/usr/include/gearbox `pkg-config --cflags opencv` `pkg-config --cflags gtkmm-2.4 libglademm-2.4 gthread-2.0` `pkg-config --cflags gtkglextmm-1.2`
LDFLAGS = `pkg-config --libs gtkmm-2.4 libglademm-2.4 gthread-2.0` `pkg-config --libs opencv` -lgsl -lgslcblas `pkg-config --libs gtkglextmm-1.2` -lGL -lGLU -lglut
LDADD = $(JDEROBOTDIR)/lib/jderobot/libJderobotIce.la \
	$(JDEROBOTDIR)/lib/jderobot/libJderobotUtil.la \
	$(JDEROBOTDIR)/lib/jderobot/libcolorspacesmm.la \
	$(JDEROBOTDIR)/lib/jderobot/libJderobotInterfaces.la \
	$(JDEROBOTDIR)/lib/jderobot/libprogeo.la \
	$(JDEROBOTDIR)/lib/jderobot/libpioneer.la

visualMemory: visualMemory.o view.o controller.o drawarea.o pioneeropengl.o camera.o cornerDetection.o geometry.o cvfast.o image.o memory.o attention.o vergency.o depuracion.o memory3Dinterfaz.o bufferMemory.o ColorFilter.o ImageInput.o pose3dmotors.o pose3dencoders.o NaoRobot.o PioneerPlatform.o AbstractRobot.o SimulatorPioneer.o FastMatchTemplate.o
	libtool --mode=link g++ -g -O -o visualMemory visualMemory.o view.o controller.o drawarea.o pioneeropengl.o camera.o cornerDetection.o geometry.o cvfast.o image.o memory.o attention.o vergency.o depuracion.o memory3Dinterfaz.o bufferMemory.o ColorFilter.o ImageInput.o pose3dmotors.o pose3dencoders.o NaoRobot.o PioneerPlatform.o AbstractRobot.o SimulatorPioneer.o FastMatchTemplate.o $(LDADD) $(LDFLAGS)

clean:
	rm -f *.o visualMemory


