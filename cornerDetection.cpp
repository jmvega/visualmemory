/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "cornerDetection.h"

namespace visualMemory {
	const int cornerDetection::CASES_OFFSET = 5;
	const int cornerDetection::solis_cases[][11] = {
		{6,6,6,7,7,7,7,7,8,8,8},
		{6,6,6,7,7,7,7,7,8,8,8},
		{6,6,6,6,7,7,7,8,8,8,8},
		{5,5,6,6,7,7,7,8,8,1,1},
		{5,5,5,5,0,0,0,1,1,1,1},
		{5,5,5,5,0,0,0,1,1,1,1},
		{5,5,5,5,0,0,0,1,1,1,1},
		{5,5,4,4,3,3,3,2,2,1,1},
		{4,4,4,4,3,3,3,2,2,2,2},
		{4,4,4,3,3,3,3,3,2,2,2},
		{4,4,4,3,3,3,3,3,2,2,2}
	};

	void cornerDetection::cannyFilter (const colorspaces::Image &image, float cannyScale, float houghThres, float houghLong, float houghGap, bool houghActived, std::vector<float> *mylines, int idCam) {

		struct timeval a, b;
		int cycle = 100;/*33*/;
		long totalb,totala;
		long diff;

		gettimeofday(&a,NULL);
		totala=a.tv_sec*1000000+a.tv_usec;

		IplImage src=image;
		IplImage *color_dst;
		IplImage *dst;
		IplImage *gray;

		color_dst = cvCreateImage(cvSize(image.width,image.height), IPL_DEPTH_8U, 3);
		dst = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		gray = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);      
		CvMemStorage* storage = cvCreateMemStorage(0);
		CvSeq* lines = 0;
		int i;

		cvCvtColor (&src, gray, CV_RGB2GRAY);	
		cvCanny (gray, dst, cannyScale, cannyScale*3, 3);       
		cvCvtColor (dst, color_dst, CV_GRAY2BGR);

		if (houghActived) {
			lines = cvHoughLines2( dst,
				storage,
				CV_HOUGH_PROBABILISTIC,
				1,
				CV_PI/180,
				houghThres,
				houghLong,
				houghGap);

			for (i = 0; i < lines->total; i++) {
				CvPoint* line = (CvPoint*)cvGetSeqElem(lines,i);
				cvLine (color_dst, line[0], line[1], CV_RGB(255,0,0), 1, 8);

				mylines->push_back (line[0].x);
				mylines->push_back (line[0].y);
				mylines->push_back (0.);
				mylines->push_back (0);
				mylines->push_back (line[1].x);
				mylines->push_back (line[1].y);
				mylines->push_back (0.);
				mylines->push_back (0);
				mylines->push_back (idCam); // nos servirá para luego dibujar de distinto modo
			}
		}

		cvCopy(color_dst,&src,0);

		cvReleaseImage(&color_dst);
		cvReleaseImage(&dst);
		cvReleaseImage(&gray);
		cvReleaseMemStorage(&storage);

		gettimeofday(&b,NULL);
		totalb=b.tv_sec*1000000+b.tv_usec;
		cout << "visualMemory: cannyFilter takes " << (totalb-totala)/1000 << " ms" << endl;
	}

	void cornerDetection::fastCorners (const colorspaces::Image &image, std::vector<Segment2D> *segments, double fastThres, double sobelThres, bool showBlackBorder, bool showSobelBorders, bool showFastCorners, bool showFastBorders, bool showLaplaceBorders, bool showLaplaceContours) {

		struct timeval a, b;
		int cycle = 100;/*33*/;
		long totalb,totala;
		long diff;

		gettimeofday(&a,NULL);
		totala=a.tv_sec*1000000+a.tv_usec;

		IplImage src = image;
		IplImage* lap, *lapRGB, *lap8U;
		IplImage *gray, *gray2, *rgb;
		CvMemStorage *storage;
		CvPoint p1, p2;
		CvScalar color = CV_RGB(255,255,0); 

		gray = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		gray2 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		lap8U = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		lapRGB = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 3);
		storage = cvCreateMemStorage(0);
		rgb = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 3);
		lap = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_32F, 1);
		std::vector<HPoint2D> *corners;

		cvCvtColor (&src, gray, CV_RGB2GRAY);
		cvLaplace (gray, lap, 3);

		if (showBlackBorder) {
			p1.x = 0.; p1.y = 0.;
		  p2.x = 320.; p2.y = 0.;
	  	cvLine(&src, p1, p2, cvScalar(0,0,0), 2, CV_AA, 0);
			p1.x = 0.; p1.y = 0.;
		  p2.x = 0.; p2.y = 240.;
	  	cvLine(&src, p1, p2, cvScalar(0,0,0), 2, CV_AA, 0);
			p1.x = 320.; p1.y = 0.;
		  p2.x = 320.; p2.y = 240.;
	  	cvLine(&src, p1, p2, cvScalar(0,0,0), 2, CV_AA, 0);
			p1.x = 0.; p1.y = 240.;
		  p2.x = 320.; p2.y = 240.;
	  	cvLine(&src, p1, p2, cvScalar(0,0,0), 2, CV_AA, 0);
		}

		cvCvtColor (&src, gray2, CV_RGB2GRAY);

    /*Get corners fast*/
    cvResetImageROI(gray);
    corners = image::getSegments(gray2, gray, segments, fastThres, sobelThres);

    /*Check lines*/
    for(std::vector<Segment2D>::iterator it1 = segments->begin(); it1 != segments->end(); it1++) {
		  if(!(*it1).isValid)
		      continue;

		  vector<Segment2D>::iterator it2 = it1;
		  for(it2++; it2 != segments->end(); it2++) {
		    if(!(*it2).isValid)
		        continue;

		    if(!geometry::mergeSegments((*it1), (*it2), PARALLEL_FACTOR, MAX_DISTANCE_LINES, PARALLEL_DISTANCE))
		        continue;


		    (*it2).isValid = false;
		  }

		  //draw
		  p1.x = (*it1).start.x; p1.y = (*it1).start.y;
		  p2.x = (*it1).end.x; p2.y = (*it1).end.y;

			if (showFastBorders)
			  cvLine(&src, p1, p2, color, 2, CV_AA, 0);
    }

		// draw corners
		vector<HPoint2D>::iterator itC = itC;
		for(itC=corners->begin(); itC != corners->end(); itC++) {
			if (showFastCorners) // draw a circle at (100,100) with a radius of 10. Use green lines of width 3
				cvCircle(&src, cvPoint(itC->x, itC->y), 5, cvScalar(0,255,0), 3);
		}

		if (showSobelBorders) {
			cvCvtColor(gray, rgb, CV_GRAY2BGR);
			image::getSegmentsMiniDebug(rgb);
			cvCopy (rgb, &src, NULL);
		}	

		if (showLaplaceBorders) {
			cvConvertScale(lap,lap8U);
			cvCvtColor(lap8U, lapRGB, CV_GRAY2RGB);
			cvCopy (lapRGB, &src, NULL);
		}

		if (showLaplaceContours) {
			cvConvertScale(lap,lap8U);
			CvSeq* contour = 0;
			CvPoint p1, p2;
			CvPoint *p3, *p4;
			int pos, pos2, maxDist;
			cvThreshold (lap8U, lap8U, 100, 255, CV_THRESH_BINARY); // convert to black and white
			cvFindContours (lap8U, storage, &contour, sizeof(CvContour), CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
			cvZero (&src);
			for( ; contour != 0; contour = contour->h_next ) {
				cvDrawContours (&src, contour, CV_RGB(0,255,0), CV_RGB(255,255,0), -1, 1, 8 );
			}
		}

		gettimeofday(&b,NULL);
		totalb=b.tv_sec*1000000+b.tv_usec;
		//cout << "visualMemory: fastCorners takes " << (totalb-totala)/1000 << " ms" << endl;
	}

	/* queHacer:
	0: Normalizar
	1: Suavizar
	2: Contornos
	3: Umbralización
	4: Dirección
	*/
	void cornerDetection::solisAlgorithm (const colorspaces::Image &image, std::vector<Segment2D> *segments, double solisSegmentsThres) {
		struct timeval a, b;
		int cycle = 100;/*33*/;
		long totalb,totala;
		long diff;
		int queHacer = 5;

		gettimeofday(&a,NULL);
		totala=a.tv_sec*1000000+a.tv_usec;

		IplImage *IplTmp1,*IplTmp2,*IplTmp3,*IplBlack,*gray;
		IplTmp1 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		IplTmp2 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		IplTmp3 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		gray = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		IplBlack = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 3);

		IplImage m_Ipl = image;
		int ThressValue = 90;
		float alpha = 3;
		float beta = 5;
		float gamma = 2;
		CvSize win;
		CvTermCriteria criteria;
		int jumpPoint;
		CvPoint *WholePointArray;
		CvPoint *PointArray[1];
		const int NUMBER_OF_PIXELS = 30;
		const int MAX_NUMBER_OF_PIXELS = 4000;
		CvMemStorage *storage;
		CvSeq* contour = NULL, *contourPointer;
		int i, j, k, n = NUMBER_OF_PIXELS;
		double angulo = 0., anguloAnterior = angulo, tempAngle;
		bool isStraight, primero;
		CvScalar color;
		Segment2D mySegment;

		cvCvtColor (&m_Ipl, gray, CV_RGB2GRAY);

		PointArray[0] = (CvPoint *)malloc(MAX_NUMBER_OF_PIXELS * sizeof(CvPoint));
		//cvCopyImage (gray, IplTmp3); // IplTmp3 is the image to draw temp results to
		cvCopyImage (gray, IplTmp1); // (src,dst) copy to Working ipl image (IplTmp1 and IplTmp2 is working containers)

		// Make a average filtering
		//cvCopyImage (IplTmp1, IplTmp2);
		//cvSmooth (IplTmp1,IplTmp2,CV_BLUR,31,15);
		// iplBlur( IplTmp1, IplTmp2, 31, 31, 15, 15); //Don't use IPL

		//Do a threshold
		cvThreshold(IplTmp1,IplTmp2,ThressValue,255,CV_THRESH_BINARY);
		//iplThreshold(IplTmp2,IplTmp1,ThressValue); // DistImg is thressholded image (IplTmp1)//Don't use IPL

		// expand the thressholded image of ones -smoothing the edge.
		//And move start position of snake out since there are no ballon forceç
		//cvCopyImage (IplTmp1, IplTmp2);
		//cvDilate( IplTmp1, IplTmp2, NULL, 3);

		//Find the contours
		storage = cvCreateMemStorage (0);
		cvFindContours (IplTmp2,storage, &contour,sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_NONE); 
		//Run through the found coutours

		contourPointer = contour; // me lo guardo

		while (contour != NULL) { // vamos recorriendo el conjunto de contornos
			if (contour->total >= NUMBER_OF_PIXELS) { // si es un contorno lo suficientemente largo (válido)
				WholePointArray = (CvPoint *)malloc(contour->total * sizeof(CvPoint));
				cvCvtSeqToArray(contour, WholePointArray, CV_WHOLE_SEQ); // copy the contour to a array

				i = 0;
				j = 0;

				while ((j < (contour->total - 4))) {
					if ((queHacer == 3) || (queHacer == 4)) { // me quedo con todos los fragmentos
						PointArray[0][i].x = WholePointArray [(int)j].x;
						PointArray[0][i].y = WholePointArray [(int)j].y;

						PointArray[0][(i+1)].x = WholePointArray [(int)(j+3)].x;
						PointArray[0][(i+1)].y = WholePointArray [(int)(j+3)].y;

						angulo = geometry::calcVectorAngle(PointArray[0][i].x, PointArray[0][i].y, PointArray[0][(i+1)].x, PointArray[0][(i+1)].y);
						angulo = angulo * (180/M_PI);

						if (queHacer == 4) // vamos haciendo porciones visuales
							cvCircle(IplBlack, WholePointArray [(int)(j+4)], 1, cvScalar(255,0,0), 2);

						if ((angulo >= 0) && (angulo < 45))
							color = CV_RGB(255,255,0);
						else if ((angulo >= 45) && (angulo < 90))
							color = CV_RGB(255,255,255);
						else if ((angulo >= 90) && (angulo < 135))
							color = CV_RGB(0,255,255);
						else if ((angulo >= 135) && (angulo < 180))
							color = CV_RGB(255,0,0);
						else if ((angulo >= 180) && (angulo < 225))
							color = CV_RGB(0,255,0);
						else if ((angulo >= 225) && (angulo < 270))
							color = CV_RGB(0,0,255);
						else if ((angulo >= 270) && (angulo < 315))
							color = CV_RGB(255,0,255);
						else if ((angulo >= 315) && (angulo < 360))
							color = CV_RGB(128,128,128);

						cvLine(IplBlack, PointArray[0][i], PointArray[0][(i+1)], color, 2, CV_AA, 0);

						i = i + 2;
						j = j + 5;

					} else if (queHacer == 5) { // Sólo nos quedamos con los fragmentos que conforman un segmento recto
						primero = true;
						isStraight = true;
						k = 0; // nos llevará la cuenta de píxeles que no tengan el mismo ángulo (margen de 3)

						while ((isStraight) && (j < (contour->total - 4))) {
							PointArray[0][i].x = WholePointArray [(int)j].x;
							PointArray[0][i].y = WholePointArray [(int)j].y;
							PointArray[0][(i+1)].x = WholePointArray [(int)(j+3)].x;
							PointArray[0][(i+1)].y = WholePointArray [(int)(j+3)].y;

							if (primero) {
								mySegment.start.x = WholePointArray [(int)j].x;
								mySegment.start.y = WholePointArray [(int)j].y;
								primero = false;

								angulo = geometry::calcVectorAngle(PointArray[0][i].x, PointArray[0][i].y, PointArray[0][(i+1)].x, PointArray[0][(i+1)].y);
								angulo = angulo * (180/M_PI);
								anguloAnterior = angulo;
							} else {
								tempAngle = abs (anguloAnterior - angulo);
								if (tempAngle > 180) // hacemos la ñapa
									tempAngle = abs (tempAngle - 360);

								if (tempAngle < solisSegmentsThres) { // si son más o menos iguales
									k=0; // reseteamos el conteo de espúreos
									anguloAnterior = angulo;

									angulo = geometry::calcVectorAngle(PointArray[0][i].x, PointArray[0][i].y, PointArray[0][(i+1)].x, PointArray[0][(i+1)].y);
									angulo = angulo * (180/M_PI);
								} else {
									k++;
									if (k > 20)
										isStraight = false; // y salimos a por otro segmento
								}
							}
							i = i + 2;
							j = j + 5;
						} // cuando salgamos será porque isStraight es falso o hemos llegado al final

						mySegment.end.x = WholePointArray [(int)(j-2)].x;
						mySegment.end.y = WholePointArray [(int)(j-2)].y;

						segments->push_back (mySegment);
					}
				}

				free(WholePointArray);
			}

			contour = contour->h_next;
		} // end (while contours != null)

		//Save result
		if (queHacer == 0) // normalización
			cvCvtColor (IplTmp2, &m_Ipl, CV_GRAY2RGB);
		else if (queHacer == 1) { // suavizar
			cvSmooth (IplTmp1, IplTmp2,CV_BLUR,31,15);
			cvCvtColor (IplTmp2, &m_Ipl, CV_GRAY2RGB);
		}	else if (queHacer == 2) { // contornos
			for( ; contourPointer != 0; contourPointer = contourPointer->h_next ) {
				cvDrawContours (&m_Ipl, contourPointer, CV_RGB(0,255,0), CV_RGB(255,255,0), -1, 3, 8 );
			}
		}	else if ((queHacer == 3) || (queHacer == 4)) { // umbralización / dirección (respectivamente)
			cvCopy (IplBlack, &m_Ipl, NULL);
		} else if (queHacer == 5) {
			CvPoint p1, p2;

	    for(std::vector<Segment2D>::iterator it1 = segments->begin(); it1 != segments->end(); it1++) {
				p1.x = (*it1).start.x;
				p1.y = (*it1).start.y;
				p2.x = (*it1).end.x;
				p2.y = (*it1).end.y;

				cvLine (&m_Ipl, p1, p2, CV_RGB(255,0,0), 2, 8);
			}
		}

		gettimeofday(&b,NULL);
		totalb=b.tv_sec*1000000+b.tv_usec;
		cout << "visualMemory: solisAlgorithm takes " << (totalb-totala)/1000 << " ms" << endl;

		// Clean up
/*
		free (contour);
		free (PointArray);
		cvReleaseMemStorage (&storage);
		cvReleaseImage(&IplTmp1);
		cvReleaseImage(&IplTmp2);
		cvReleaseImage(&IplTmp3);
*/
	}

	void cornerDetection::solisAlgorithm2 (IplImage &image, std::vector<Segment2D> *segments, double solisSegmentsThres) {
		IplImage *IplTmp1,*IplTmp2,*IplBlack;
		CvPoint pstart, pend;
		CvMemStorage *storage;
		CvSeq* contour = NULL;
		int i, last_i, i_jump = 5;
		int margin, max_margin = 2;
		int ThressValue = 90;
		CvPoint *WholePointArray;
		const int min_size_contour = 30;
		const int min_size_segment = 15;
		double angle = 0.0, lastAngle = 0.0, tempAngle;
		bool first;
		CvScalar color;
		Segment2D mySegment;
		bool debug = false;

		IplTmp1 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		IplTmp2 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		storage = cvCreateMemStorage(0);

		if(debug) {
			IplBlack = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 3);
			cvZero(IplBlack);
		}

		/*Convert to Gray Image*/
		cvCvtColor (&image, IplTmp1, CV_RGB2GRAY); // (src,dst) copy to Working ipl image (IplTmp1 and IplTmp2 is working containers)

		// Make a average filtering
		//cvCopyImage (IplTmp1, IplTmp2);
		//cvSmooth (IplTmp1,IplTmp2,CV_BLUR,31,15);
		// iplBlur( IplTmp1, IplTmp2, 31, 31, 15, 15); //Don't use IPL

		/*Perform a binary threshold*/
		cvThreshold(IplTmp1,IplTmp2,ThressValue,255,CV_THRESH_BINARY);
		//iplThreshold(IplTmp2,IplTmp1,ThressValue); // DistImg is thressholded image (IplTmp1)//Don't use IPL

		// expand the thressholded image of ones -smoothing the edge.
		//And move start position of snake out since there are no ballon forceç
		//cvCopyImage (IplTmp1, IplTmp2);
		//cvDilate( IplTmp1, IplTmp2, NULL, 3);

		/*Find contours*/
		cvFindContours(IplTmp2, storage, &contour,sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_NONE); 

		/*Run through found coutours*/
		while (contour != NULL) {
			/*Check length*/
			if (contour->total >= min_size_contour) {
				/*Convert to array*/
				WholePointArray = (CvPoint *)malloc(contour->total * sizeof(CvPoint));
				cvCvtSeqToArray(contour, WholePointArray, CV_WHOLE_SEQ);

				i = 0;
				first = true;
				margin = 0;

				while ((i < (contour->total - (i_jump-1)))) {

					/*Get current segment*/
					pstart.x = WholePointArray [(int)i].x;
					pstart.y = WholePointArray [(int)i].y;
					pend.x = WholePointArray [(int)(i+3)].x;
					pend.y = WholePointArray [(int)(i+3)].y;

					/*Get segment angle*/
					angle = geometry::calcVectorAngle(pstart.x, pstart.y, pend.x, pend.y);

					if (debug) {
						/*Visual portions*/
						//cvCircle(IplBlack, WholePointArray [(int)(i+4)], 1, cvScalar(255,0,0), 2);

						if ((angle >= 0) && (angle < M_PI/4))
							color = CV_RGB(255,255,0);
						else if ((angle >= M_PI/4) && (angle < M_PI/2))
							color = CV_RGB(255,255,255);
						else if ((angle >= M_PI/2) && (angle < 3*M_PI/4))
							color = CV_RGB(0,255,255);
						else if ((angle >= 3*M_PI/4) && (angle < M_PI))
							color = CV_RGB(255,0,0);
						else if ((angle >= M_PI) && (angle < 5*M_PI/4))
							color = CV_RGB(0,255,0);
						else if ((angle >= 5*M_PI/4) && (angle < 6*M_PI/4))
							color = CV_RGB(0,0,255);
						else if ((angle >= 6*M_PI/4) && (angle < 7*M_PI/4))
							color = CV_RGB(255,0,255);
						else if ((angle >= 7*M_PI/4) && (angle < 2*M_PI))
							color = CV_RGB(128,128,128);

						/*Draw line with orientation*/
						cvLine(IplBlack, pstart, pend, color, 2, CV_AA, 0);
					} else {
						if (first) {
							mySegment.start.x = pstart.x;
							mySegment.start.y = pstart.y;
							mySegment.end.x = pend.x;
							mySegment.end.y = pend.y;
							first = false;
							last_i = i;
							lastAngle = angle;

							//cout << "guardamos primero" << endl;
						} else {
							tempAngle = abs (lastAngle - angle);		/*TODO: compare with average*/
							if (tempAngle > M_PI)
								tempAngle = abs (tempAngle - 2*M_PI);

							/*Check angle threshold*/
							if (tempAngle < solisSegmentsThres) {
								//cout << "guardamos ultimo" << endl;
								margin=0;
								last_i = i;
								lastAngle = angle;

								/*Save current end*/
								mySegment.end.x = pend.x;
								mySegment.end.y = pend.y;
							} else {
								//cout << "sumamos margin" << endl;
								margin++;
								if (margin > max_margin) {
									//cout << "guardamos segmento" << endl;
									/*Save current segment if length is enough*/
									if(geometry::distanceBetweenPoints2D(mySegment.start, mySegment.end) > min_size_segment)
										segments->push_back(mySegment);
									first = true;
									margin = 0;
								}
							}
						}
					}

					if(first && !debug)
						i = last_i;
					i += i_jump;
				}

				free(WholePointArray);
			}

			contour = contour->h_next;
		}

		/*Clean up*/
		cvReleaseImage(&IplTmp1);
		cvReleaseImage(&IplTmp2);
		cvClearMemStorage(storage); //Free contour
		cvReleaseMemStorage (&storage);

		if(debug)
			cvReleaseImage(&IplBlack);
	}

	void cornerDetection::solisAlgorithm3 (IplImage &image, std::vector<Segment2D> *segments) {
		IplImage *IplTmp1,*IplTmp2,*IplBlack,*IplLaplace;
		CvPoint pstart, pend;
		CvMemStorage *storage;
		CvSeq* contour = NULL;
		int i, i_jump = 6;
		int ThressValue = 30;
		int diff_x, diff_y;
		CvPoint *WholePointArray;
		const int min_size_contour = 30;
		CvScalar color;
		Segment2D mySegment;
		int type, current_type;
		int num_frag, min_frags = 3, counter = 0;
		double max_distance;
		bool first, debug = false;

		IplTmp1 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		IplTmp2 = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 1);
		IplLaplace = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_16S, 1);
		storage = cvCreateMemStorage(0);

		if(debug) {
			IplBlack = cvCreateImage (cvSize(image.width,image.height), IPL_DEPTH_8U, 3);
			cvZero(IplBlack);
		}

		/*Convert to Gray Image*/
		cvCvtColor (&image, IplTmp1, CV_RGB2GRAY);

		/*Normalize image*/
		cvNormalize(IplTmp1, IplTmp1, 0, 255, CV_MINMAX);

		// Make a average filtering
		cvSmooth(IplTmp1,IplTmp2,CV_BLUR,3,3);

		//Laplace
		cvLaplace(IplTmp2, IplLaplace, 3);
		cvConvertScale(IplLaplace,IplTmp1);

		/*Perform a binary threshold*/
		cvThreshold(IplTmp1,IplTmp2,ThressValue,255,CV_THRESH_BINARY);

		/*Find contours*/
		cvFindContours(IplTmp2, storage, &contour,sizeof(CvContour), CV_RETR_LIST, CV_CHAIN_APPROX_NONE);

		/*Run through found coutours*/
		while (contour != NULL) {
			/*Check length*/
			if (contour->total >= min_size_contour) {
				/*Convert to array*/
				WholePointArray = (CvPoint *)malloc(contour->total * sizeof(CvPoint));
				cvCvtSeqToArray(contour, WholePointArray, CV_WHOLE_SEQ);

				i = 0;
				first = true;

				while ((i < (contour->total - (i_jump-1)))) {

					counter++;

					/*Get current segment*/
					pstart.x = WholePointArray[(int)i].x;
					pstart.y = WholePointArray[(int)i].y;
					pend.x = WholePointArray[(int)(i+i_jump-1)].x;
					pend.y = WholePointArray[(int)(i+i_jump-1)].y;

					/*Calculate type*/
					diff_x = pstart.x - pend.x;
					diff_y = pstart.y - pend.y;
					type = cornerDetection::solis_cases[diff_x+cornerDetection::CASES_OFFSET][diff_y+cornerDetection::CASES_OFFSET];

					if (debug) {
						/*Visual portions*/
						if (type==0)
							color = CV_RGB(255,255,255);
						else if (type==1)
							color = CV_RGB(255,255,0);
						else if (type==2)
							color = CV_RGB(0,255,255);
						else if (type==3)
							color = CV_RGB(255,0,0);
						else if (type==4)
							color = CV_RGB(0,255,0);
						else if (type==5)
							color = CV_RGB(0,0,255);
						else if (type==6)
							color = CV_RGB(255,0,255);
						else if (type==7)
							color = CV_RGB(0,128,128);
						else if (type==8)
							color = CV_RGB(128,128,0);

						/*Draw line with orientation*/
						cvLine(IplBlack, pstart, pend, color, 2, CV_AA, 0);
					} else {
						if (first) {
							if(type != 0) {
								mySegment.start.x = pstart.x;
								mySegment.start.y = pstart.y;
								mySegment.start.h = (float) counter;
								mySegment.end.x = pend.x;
								mySegment.end.y = pend.y;
								mySegment.end.h = (float) counter;
								mySegment.type = type;
								first = false;
								current_type = type;
								num_frag = 1;
							}
						} else {
							/*Check angle threshold*/
							if (current_type == type){// || type == 0) {
								/*Save current end*/
								mySegment.end.x = pend.x;
								mySegment.end.y = pend.y;
								mySegment.end.h = (float) counter;
								num_frag++;
							} else {
								/*Save current segment if length is enough*/
								if(num_frag >= min_frags)
									segments->push_back(mySegment);
								first = true;
							}
						}
					}

					i += i_jump;
				}

				/*Save the last one*/
				if(!first && num_frag >= min_frags)
					segments->push_back(mySegment);

				free(WholePointArray);
			}

			contour = contour->h_next;
		}

		if (debug) {
			//cvCvtColor (IplTmp1, IplBlack, CV_GRAY2RGB);
			cvCopy(IplBlack, &image, NULL);
		} else {

			/*CvPoint p1, p2;*/

			/*Draw lines*/
			/*for(std::vector<Segment2D>::iterator it1 = segments->begin(); it1 != segments->end(); it1++) {
				p1.x = (*it1).start.x;
				p1.y = (*it1).start.y;
				p2.x = (*it1).end.x;
				p2.y = (*it1).end.y;
				cvLine (&image, p1, p2, CV_RGB(0,0,255), 2, 8);	
			}*/
		}

		max_distance = (double) (i_jump*min_frags);

		/*Merge consecutive fragments*/
		std::vector<Segment2D>::iterator it1 = segments->begin();
		while(it1 != segments->end()) {
				/*Compare with next one*/
				std::vector<Segment2D>::iterator it2 = it1;
				it2++;
				if(it2 != segments->end()) {	
					if((*it1).type == (*it2).type && (*it2).start.h - (*it1).end.h <= min_frags) {
						if(geometry::distanceBetweenPoints2D((*it2).start.x, (*it2).start.y, (*it1).end.x, (*it1).end.y) <= max_distance) { 
							(*it1).end.x = (*it2).end.x;
							(*it1).end.y = (*it2).end.y;
							(*it1).end.h = (*it2).end.h;
							segments->erase(it2);
							continue;
						}
					}
				}

				it1++;
		}

		/*Reset .h values*/
		for(std::vector<Segment2D>::iterator it_s = segments->begin(); it_s != segments->end(); it_s++) {
			(*it_s).start.h = 1.0;
			(*it_s).end.h = 1.0;
		}

		/*Clean up*/
		cvReleaseImage(&IplTmp1);
		cvReleaseImage(&IplTmp2);
		cvClearMemStorage(storage); //Free contour
		cvReleaseMemStorage (&storage);

		if(debug)
			cvReleaseImage(&IplBlack);
	}

	int cornerDetection::findClosestPoint (CvPoint *p1, CvPoint *p2, CvSeq *points, int maxDist) {  
		 int ret = -1,i;
		 float dist, minDist = maxDist;
		 CvPoint* test;
		 int (*dirTest)(CvPoint *,CvPoint *);

		 if(isVertical(p1,p2)){ //vertical line
			  if(p2->y > p1->y) {//going down
			     dirTest = isBelow;
			  } else { // going up
			     dirTest = isAbove;
			  }
		 } else if (isHorizontal(p1,p2)){ //horizontal line
			  if(p2->x > p1->x) {//going right
			     dirTest = isRight;
			  } else { //going left
			     dirTest = isLeft;
			  }
		 }

		 for( i = 0; i < points->total; i++ )
		 {
			  test = (CvPoint*)cvGetSeqElem( points, i );
			  if(dirTest(p2,test)){ //only test points in the region we care about
			     dist = sqrt(pow(test->x - p2->x,2)+pow(test->y - p2->y,2));
			     if(dist<minDist){
			        minDist = dist;
			        ret = i;
			     }
			  }
		 } 
		 return ret;
	}

	int cornerDetection::isVertical(CvPoint *p1, CvPoint *p2){
		 return p1->x == p2->x;
	}

	int cornerDetection::isHorizontal(CvPoint *p1, CvPoint *p2){
		 return p1->y == p2->y;
	}

	int cornerDetection::isRight(CvPoint *pt1, CvPoint *pt2){
		 return pt2->x > pt1->x;
	}

	int cornerDetection::isLeft(CvPoint *pt1, CvPoint *pt2){
		 return pt2->x < pt1->x;
	}

	int cornerDetection::isBelow(CvPoint *pt1, CvPoint *pt2){
		 return pt2->y > pt1->y;
	}

	int cornerDetection::isAbove(CvPoint *pt1, CvPoint *pt2){
		 return pt2->y < pt1->y;
	}
}

