/*
 *  Copyright (C) 2011 Julio Vega
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Author : Julio Vega <julio.vega@urjc.es>,
 *
 */

#include "drawarea.h"

namespace visualMemory {
	const float DrawArea::MAXWORLD = 30.;
	const float DrawArea::PI = 3.141592654;

	DrawArea::DrawArea(BaseObjectType* cobject, const Glib::RefPtr<Gnome::Glade::Xml>& builder)
	: Gtk::DrawingArea(cobject), Gtk::GL::Widget<DrawArea>() {

		this->refresh_time = 10; //ms

		Glib::RefPtr<Gdk::GL::Config> glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB | Gdk::GL::MODE_DEPTH | Gdk::GL::MODE_DOUBLE);
		if (!glconfig) {
			std::cerr << "*** Cannot find the double-buffered visual.\n" << "*** Trying single-buffered visual.\n";

			// Try single-buffered visual
			glconfig = Gdk::GL::Config::create(Gdk::GL::MODE_RGB | Gdk::GL::MODE_DEPTH);
			if (!glconfig) {
				std::cerr << "*** Cannot find any OpenGL-capable visual.\n";
				std::exit(1);
			}
		}

		/*Set OpenGL-capability to the widget.*/
		this->unrealize();
		if(!this->set_gl_capability(glconfig) || !this->is_gl_capable()) {
			std::cerr << "No Gl capability\n";
			std::exit(1);
		}
		this->realize();

		/*Add events*/
		this->add_events(	Gdk::BUTTON1_MOTION_MASK    |
											Gdk::BUTTON2_MOTION_MASK    |
											Gdk::BUTTON3_MOTION_MASK    |
											Gdk::BUTTON_PRESS_MASK      |
											Gdk::BUTTON_RELEASE_MASK    |
											Gdk::VISIBILITY_NOTIFY_MASK);

		this->signal_motion_notify_event().connect(sigc::mem_fun(this,&DrawArea::on_motion_notify));
		this->signal_button_press_event().connect(sigc::mem_fun(this,&DrawArea::on_button_press));
		this->signal_scroll_event().connect(sigc::mem_fun(this,&DrawArea::on_drawarea_scroll));

		/*Call to expose_event*/
		Glib::signal_timeout().connect( sigc::mem_fun(*this, &DrawArea::on_timeout), this->refresh_time);

		/*Init Glut*/
		int val_init = 0;
		glutInit(&val_init, NULL);
 		glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);

		pthread_mutex_init(&mutex_3D, NULL);

		// GL Camera Position and FOA
		this->glcam_pos.X=0.;
		this->glcam_pos.Y=0.;
		this->glcam_pos.Z=1000.;
		this->glcam_foa.X=0.;
		this->glcam_foa.Y=0.;
		this->glcam_foa.Z=0.;

    this->radius = 20.0;
    this->lati = 0.2;
    this->longi = -1.0;
    this->old_x = 0.0;
    this->old_y = 0.0; 

		init_pioneer(); // IMPORTANTE: para cargar los vectores de sonares y lasers
	}

	void DrawArea::lock() {
		pthread_mutex_lock(&mutex_3D);
	}

	void DrawArea::unlock() {
		pthread_mutex_unlock(&mutex_3D);
	}

	void DrawArea::posCamRel2Robot () { // sacamos la posicióin de la cámara de visualización, según la pos del robot
		CvPoint3D32f myPoint;
		this->view->controller->getPosition (&myPoint);
		gsl_matrix *matrizRobot, *foaRel, *foaAbs, *posRel, *posAbs;
		foaRel = gsl_matrix_calloc(4,1);
		foaAbs = gsl_matrix_calloc(4,1);
		posRel = gsl_matrix_calloc(4,1);
		posAbs = gsl_matrix_calloc(4,1);
		matrizRobot = gsl_matrix_calloc(4,4);

		gsl_matrix_set(matrizRobot,0,0,cos((myPoint.z)*DEGTORAD));
		gsl_matrix_set(matrizRobot,0,1,-sin((myPoint.z)*DEGTORAD));
		gsl_matrix_set(matrizRobot,0,2,0.);
		gsl_matrix_set(matrizRobot,0,3,myPoint.x);
		gsl_matrix_set(matrizRobot,1,0,sin((myPoint.z)*DEGTORAD));
		gsl_matrix_set(matrizRobot,1,1,cos((myPoint.z)*DEGTORAD));
		gsl_matrix_set(matrizRobot,1,2,0.);
		gsl_matrix_set(matrizRobot,1,3,myPoint.y);
		gsl_matrix_set(matrizRobot,2,0,0.);
		gsl_matrix_set(matrizRobot,2,1,0.);
		gsl_matrix_set(matrizRobot,2,2,1.);
		gsl_matrix_set(matrizRobot,2,3,0.); // Z de la base robot la considero 0
		gsl_matrix_set(matrizRobot,3,0,0.);
		gsl_matrix_set(matrizRobot,3,1,0.);
		gsl_matrix_set(matrizRobot,3,2,0.);
		gsl_matrix_set(matrizRobot,3,3,1.0);

		gsl_matrix_set(foaRel,0,0,2000.0); // A 2000 en la dirección de theta en Z = 0
		gsl_matrix_set(foaRel,1,0,0.0);
		gsl_matrix_set(foaRel,2,0,0.0);
		gsl_matrix_set(foaRel,3,0,1.0);

		gsl_matrix_set(posRel,0,0,-1000.0); // A 1000 por detras del robot y por arriba
		gsl_matrix_set(posRel,1,0,0.0);
		gsl_matrix_set(posRel,2,0,2000.0);
		gsl_matrix_set(posRel,3,0,1.0);

		gsl_linalg_matmult(matrizRobot,foaRel,foaAbs);
		gsl_linalg_matmult(matrizRobot,posRel,posAbs);

		// Sacamos el punto horizonte en absoluto
		this->glcam_pos.X = (float) gsl_matrix_get (posAbs, 0, 0)/SCALE;
		this->glcam_pos.Y = (float) gsl_matrix_get (posAbs, 1, 0)/SCALE;
		this->glcam_pos.Z = (float) gsl_matrix_get (posAbs, 2, 0)/SCALE;
		this->glcam_foa.X = (float) gsl_matrix_get (foaAbs, 0, 0)/SCALE;
		this->glcam_foa.Y = (float) gsl_matrix_get (foaAbs, 1, 0)/SCALE;
		this->glcam_foa.Z = (float) gsl_matrix_get (foaAbs, 2, 0)/SCALE;

		//printf ("Pos = %f, %f, %f\n", this->glcam_pos.X, this->glcam_pos.Y, this->glcam_pos.Z);
		//printf ("Foa = %f, %f, %f\n", this->glcam_foa.X, this->glcam_foa.Y, this->glcam_foa.Z);
	}

	void DrawArea::setView (View* view) {
		this->view = view;
	}

	DrawArea::~DrawArea() {	}

	void DrawArea::pintaSegmento (CvPoint3D32f a, CvPoint3D32f b, CvPoint3D32f color) {
	  /* OJO mundo de coordenadas OpenGL está en decímetros, por compatibilidad con la plantilla OpenGL 
	     del robotPioneer. El factor SCALE marca la relación entre las coordenadas en milímetros de a,b
	     y los homólogos en el mundo OpenGL */
		glColor3f(color.x, color.y, color.z);
		glLineWidth(2.0f);
		glBegin(GL_LINES);
			v3f(a.x/SCALE, a.y/SCALE, a.z/SCALE);
			v3f(b.x/SCALE, b.y/SCALE, b.z/SCALE);
		glEnd();
	}

	void DrawArea::pintaPuntos3D () {
		std::vector<HPoint3D>::iterator it;

		for(it = this->view->controller->pointMemory.begin(); it != this->view->controller->pointMemory.end(); it++) {
			glColor3f (1, 0, 0);

			glPushMatrix();
			glTranslatef((*it).X/SCALE, (*it).Y/SCALE, (*it).Z/SCALE);
			glutSolidSphere(0.1, 10, 10);
			glPopMatrix();
		}
	}

	void DrawArea::pintaMemoria () {
		CvPoint3D32f a, b, color;
		std::vector<Segment3D>::iterator it;

		for(it = this->view->controller->segmentMemory.begin(); it != this->view->controller->segmentMemory.end(); it++) {
			a.x = (*it).start.X;
			a.y = (*it).start.Y;
			a.z = (*it).start.Z;

			b.x = (*it).end.X;
			b.y = (*it).end.Y;
			b.z = (*it).end.Z;

      if ((*it).type==1) {
				color.x = 1;
				color.y = 0;
				color.z = 0;
			} else if((*it).type==2) {
				color.x = 0;
				color.y = 0;
				color.z = 1;
      } else if((*it).type==3) {
				color.x = 1;
				color.y = 0;
				color.z = 1;
      } else if((*it).type==4) {
				color.x = 0.6;
				color.y = 0.6;
				color.z = 0.6;
      } else if((*it).type==5) {
				color.x = 0;
				color.y = 1;
				color.z = 1;
      } else {
				color.x = 0;
				color.y = 0;
				color.z = 0;
			}

			pintaSegmento (a, b, color); 
		}
	}

	void DrawArea::pintaSolis () {
		CvPoint3D32f a, b, color;
		std::vector<Segment3D>::iterator it;

		for(it = this->view->controller->solisSegments1_3D.begin(); it != this->view->controller->solisSegments1_3D.end(); it++) {
			a.x = (*it).start.X;
			a.y = (*it).start.Y;
			a.z = (*it).start.Z;

			b.x = (*it).end.X;
			b.y = (*it).end.Y;
			b.z = (*it).end.Z;

			color.x = 0;
			color.y = 128;
			color.z = 128;

			pintaSegmento (a, b, color); 
		}

		if (this->view->controller->getNumCamerasUsed () == 2) {
			for(it = this->view->controller->solisSegments2_3D.begin(); it != this->view->controller->solisSegments2_3D.end(); it++) {
				a.x = (*it).start.X;
				a.y = (*it).start.Y;
				a.z = (*it).start.Z;

				b.x = (*it).end.X;
				b.y = (*it).end.Y;
				b.z = (*it).end.Z;

				color.x = 64;
				color.y = 64;
				color.z = 64;

				pintaSegmento (a, b, color); 
			}
		}

	}

	void DrawArea::pintaPredicted () {
		CvPoint3D32f a, b, color;
		std::vector<Segment3D>::iterator it;
		std::vector<Parallelogram3D>::iterator it2;

		for(it = this->view->controller->predictedSegments1_3D.begin(); it != this->view->controller->predictedSegments1_3D.end(); it++) {
			a.x = (*it).start.X;
			a.y = (*it).start.Y;
			a.z = (*it).start.Z;

			b.x = (*it).end.X;
			b.y = (*it).end.Y;
			b.z = (*it).end.Z;

			color.x = 0;
			color.y = 0;
			color.z = 255;

			pintaSegmento (a, b, color); 
		}

		if (this->view->controller->getNumCamerasUsed () == 2) {
			for(it = this->view->controller->predictedSegments2_3D.begin(); it != this->view->controller->predictedSegments2_3D.end(); it++) {
				a.x = (*it).start.X;
				a.y = (*it).start.Y;
				a.z = (*it).start.Z;

				b.x = (*it).end.X;
				b.y = (*it).end.Y;
				b.z = (*it).end.Z;

				color.x = 0;
				color.y = 0;
				color.z = 255;

				pintaSegmento (a, b, color); 
			}
		}

		glShadeModel(GL_FLAT);
		glColor3f(0.0f, 0.0f, 1.0f);

		for(it2 = this->view->controller->predictedParallelograms1_3D.begin(); it2 != this->view->controller->predictedParallelograms1_3D.end(); it2++) {
			glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
				glVertex3f ((*it2).p2.X/SCALE, (*it2).p2.Y/SCALE, (*it2).p2.Z/SCALE);
				glVertex3f ((*it2).p1.X/SCALE, (*it2).p1.Y/SCALE, (*it2).p1.Z/SCALE);
				glVertex3f ((*it2).p3.X/SCALE, (*it2).p3.Y/SCALE, (*it2).p3.Z/SCALE);
				glVertex3f ((*it2).p4.X/SCALE, (*it2).p4.Y/SCALE, (*it2).p4.Z/SCALE);
			glEnd ();
		}
	}

	void DrawArea::pintaNew () {
		CvPoint3D32f a, b, color;
		std::vector<Segment3D>::iterator it;
		std::vector<Parallelogram3D>::iterator it2;

		for(it = this->view->controller->newSegments1_3D.begin(); it != this->view->controller->newSegments1_3D.end(); it++) {
			a.x = (*it).start.X;
			a.y = (*it).start.Y;
			a.z = (*it).start.Z;

			b.x = (*it).end.X;
			b.y = (*it).end.Y;
			b.z = (*it).end.Z;

			color.x = 255;
			color.y = 255;
			color.z = 0;

			pintaSegmento (a, b, color); 
		}

		if (this->view->controller->getNumCamerasUsed () == 2) {
			for(it = this->view->controller->newSegments2_3D.begin(); it != this->view->controller->newSegments2_3D.end(); it++) {
				a.x = (*it).start.X;
				a.y = (*it).start.Y;
				a.z = (*it).start.Z;

				b.x = (*it).end.X;
				b.y = (*it).end.Y;
				b.z = (*it).end.Z;

				color.x = 255;
				color.y = 255;
				color.z = 0;

				pintaSegmento (a, b, color); 
			}
		}

		glShadeModel(GL_FLAT);
		glColor3f(1.0f, 1.0f, 0.0f);

		for(it2 = this->view->controller->newParallelograms1_3D.begin(); it2 != this->view->controller->newParallelograms1_3D.end(); it2++) {
			glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
				glVertex3f ((*it2).p2.X/SCALE, (*it2).p2.Y/SCALE, (*it2).p2.Z/SCALE);
				glVertex3f ((*it2).p1.X/SCALE, (*it2).p1.Y/SCALE, (*it2).p1.Z/SCALE);
				glVertex3f ((*it2).p3.X/SCALE, (*it2).p3.Y/SCALE, (*it2).p3.Z/SCALE);
				glVertex3f ((*it2).p4.X/SCALE, (*it2).p4.Y/SCALE, (*it2).p4.Z/SCALE);
			glEnd ();
		}
	}

	void DrawArea::pintaRefuted () {
		CvPoint3D32f a, b, color;
		std::vector<Segment3D>::iterator it;
		std::vector<Parallelogram3D>::iterator it2;

		for(it = this->view->controller->refutedSegments1_3D.begin(); it != this->view->controller->refutedSegments1_3D.end(); it++) {
			a.x = (*it).start.X;
			a.y = (*it).start.Y;
			a.z = (*it).start.Z;

			b.x = (*it).end.X;
			b.y = (*it).end.Y;
			b.z = (*it).end.Z;

			color.x = 255;
			color.y = 0;
			color.z = 0;

			pintaSegmento (a, b, color); 
		}

		if (this->view->controller->getNumCamerasUsed () == 2) {
			for(it = this->view->controller->refutedSegments2_3D.begin(); it != this->view->controller->refutedSegments2_3D.end(); it++) {
				a.x = (*it).start.X;
				a.y = (*it).start.Y;
				a.z = (*it).start.Z;

				b.x = (*it).end.X;
				b.y = (*it).end.Y;
				b.z = (*it).end.Z;

				color.x = 255;
				color.y = 0;
				color.z = 0;

				pintaSegmento (a, b, color); 
			}
		}

		glShadeModel(GL_FLAT);
		glColor3f(1.0f, 0.0f, 0.0f);

		for(it2 = this->view->controller->refutedParallelograms1_3D.begin(); it2 != this->view->controller->refutedParallelograms1_3D.end(); it2++) {
			glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
				glVertex3f ((*it2).p2.X/SCALE, (*it2).p2.Y/SCALE, (*it2).p2.Z/SCALE);
				glVertex3f ((*it2).p1.X/SCALE, (*it2).p1.Y/SCALE, (*it2).p1.Z/SCALE);
				glVertex3f ((*it2).p3.X/SCALE, (*it2).p3.Y/SCALE, (*it2).p3.Z/SCALE);
				glVertex3f ((*it2).p4.X/SCALE, (*it2).p4.Y/SCALE, (*it2).p4.Z/SCALE);
			glEnd ();
		}
	}

	void DrawArea::pintaFitted () {
		CvPoint3D32f a, b, color;
		std::vector<Segment3D>::iterator it;
		std::vector<Parallelogram3D>::iterator it2;

		for(it = this->view->controller->fittedSegments1_3D.begin(); it != this->view->controller->fittedSegments1_3D.end(); it++) {
			a.x = (*it).start.X;
			a.y = (*it).start.Y;
			a.z = (*it).start.Z;

			b.x = (*it).end.X;
			b.y = (*it).end.Y;
			b.z = (*it).end.Z;

			color.x = 0;
			color.y = 255;
			color.z = 0;

			pintaSegmento (a, b, color); 
		}

		if (this->view->controller->getNumCamerasUsed () == 2) {
			for(it = this->view->controller->fittedSegments2_3D.begin(); it != this->view->controller->fittedSegments2_3D.end(); it++) {
				a.x = (*it).start.X;
				a.y = (*it).start.Y;
				a.z = (*it).start.Z;

				b.x = (*it).end.X;
				b.y = (*it).end.Y;
				b.z = (*it).end.Z;

				color.x = 0;
				color.y = 255;
				color.z = 0;

				pintaSegmento (a, b, color); 
			}
		}

		glShadeModel(GL_FLAT);
		glColor3f(0.0f, 1.0f, 0.0f);

		for(it2 = this->view->controller->fittedParallelograms1_3D.begin(); it2 != this->view->controller->fittedParallelograms1_3D.end(); it2++) {
			glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
				glVertex3f ((*it2).p2.X/SCALE, (*it2).p2.Y/SCALE, (*it2).p2.Z/SCALE);
				glVertex3f ((*it2).p1.X/SCALE, (*it2).p1.Y/SCALE, (*it2).p1.Z/SCALE);
				glVertex3f ((*it2).p3.X/SCALE, (*it2).p3.Y/SCALE, (*it2).p3.Z/SCALE);
				glVertex3f ((*it2).p4.X/SCALE, (*it2).p4.Y/SCALE, (*it2).p4.Z/SCALE);
			glEnd ();
		}
	}

	void DrawArea::pintaExtremos () {
		CvPoint3D32f a, b, c, d, e, f, color;

		color.x = 128;
		color.y = 128;
		color.z = 255;

		a.x = this->view->controller->puntoExtremo1.X;
		a.y = this->view->controller->puntoExtremo1.Y;
		a.z = this->view->controller->puntoExtremo1.Z;

		b.x = this->view->controller->puntoExtremo2.X;
		b.y = this->view->controller->puntoExtremo2.Y;
		b.z = this->view->controller->puntoExtremo2.Z;

		c.x = this->view->controller->puntoExtremo3.X;
		c.y = this->view->controller->puntoExtremo3.Y;
		c.z = this->view->controller->puntoExtremo3.Z;

		d.x = this->view->controller->puntoExtremo4.X;
		d.y = this->view->controller->puntoExtremo4.Y;
		d.z = this->view->controller->puntoExtremo4.Z;

		pintaSegmento (a, b, color);
		pintaSegmento (b, c, color);
		pintaSegmento (c, d, color);
		pintaSegmento (d, a, color);

		if (this->view->controller->getNumCamerasUsed () == 2) {
			a.x = this->view->controller->puntoExtremo5.X;
			a.y = this->view->controller->puntoExtremo5.Y;
			a.z = this->view->controller->puntoExtremo5.Z;

			b.x = this->view->controller->puntoExtremo6.X;
			b.y = this->view->controller->puntoExtremo6.Y;
			b.z = this->view->controller->puntoExtremo6.Z;

			c.x = this->view->controller->puntoExtremo7.X;
			c.y = this->view->controller->puntoExtremo7.Y;
			c.z = this->view->controller->puntoExtremo7.Z;

			d.x = this->view->controller->puntoExtremo8.X;
			d.y = this->view->controller->puntoExtremo8.Y;
			d.z = this->view->controller->puntoExtremo8.Z;

			//printf ("[%f, %f, %f], [%f, %f, %f], [%f, %f, %f], [%f, %f, %f]\n", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z);

			pintaSegmento (a, b, color);
			pintaSegmento (b, c, color);
			pintaSegmento (c, d, color);
			pintaSegmento (d, a, color);
		}

	}

	void DrawArea::pintaCruz () {
		CvPoint3D32f a, b, c, d, color;

		color.x = 0;
		color.y = 0;
		color.z = 128;

		a.x = this->view->controller->puntoCruz1.X;
		a.y = this->view->controller->puntoCruz1.Y;
		a.z = this->view->controller->puntoCruz1.Z;

		b.x = this->view->controller->puntoCruz2.X;
		b.y = this->view->controller->puntoCruz2.Y;
		b.z = this->view->controller->puntoCruz2.Z;

		c.x = this->view->controller->puntoCruz3.X;
		c.y = this->view->controller->puntoCruz3.Y;
		c.z = this->view->controller->puntoCruz3.Z;

		d.x = this->view->controller->puntoCruz4.X;
		d.y = this->view->controller->puntoCruz4.Y;
		d.z = this->view->controller->puntoCruz4.Z;

		//printf ("[%f, %f, %f], [%f, %f, %f], [%f, %f, %f], [%f, %f, %f]\n", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z);

		pintaSegmento (a, b, color); 
		pintaSegmento (c, d, color); 
	}

	void DrawArea::pintaVergencyPoint () {
		CvPoint3D32f a, b, c, color;

		color.x = 0;
		color.y = 0;
		color.z = 255;

		a.x = this->view->controller->vergency3Dpoint.X;
		a.y = this->view->controller->vergency3Dpoint.Y;
		a.z = this->view->controller->vergency3Dpoint.Z;

		b.x = this->view->controller->myCamA.position.X;
		b.y = this->view->controller->myCamA.position.Y;
		b.z = this->view->controller->myCamA.position.Z;

		c.x = this->view->controller->myCamB.position.X;
		c.y = this->view->controller->myCamB.position.Y;
		c.z = this->view->controller->myCamB.position.Z;

		pintaSegmento (a, b, color);
		pintaSegmento (a, c, color);
	}

	void DrawArea::pintaClicados () {
		CvPoint3D32f a, b, c, d, color;

		color.x = 0;
		color.y = 255;
		color.z = 0;

		a.x = this->view->controller->clickedPixelA_3D.X;
		a.y = this->view->controller->clickedPixelA_3D.Y;
		a.z = this->view->controller->clickedPixelA_3D.Z;

		b.x = this->view->controller->clickedPixelB_3D.X;
		b.y = this->view->controller->clickedPixelB_3D.Y;
		b.z = this->view->controller->clickedPixelB_3D.Z;

		c.x = this->view->controller->myCamA.position.X;
		c.y = this->view->controller->myCamA.position.Y;
		c.z = this->view->controller->myCamA.position.Z;

		d.x = this->view->controller->myCamB.position.X;
		d.y = this->view->controller->myCamB.position.Y;
		d.z = this->view->controller->myCamB.position.Z;

		//printf ("[%f, %f, %f], [%f, %f, %f], [%f, %f, %f], [%f, %f, %f]\n", a.x, a.y, a.z, b.x, b.y, b.z, c.x, c.y, c.z, d.x, d.y, d.z);

		pintaSegmento (a, c, color); 
		pintaSegmento (b, d, color); 
	}

	void DrawArea::pintaParalelogramos () {
		glShadeModel(GL_FLAT);
		glColor3f(1.0f, 1.0f, 0.0f);

		std::vector<Parallelogram3D>::iterator it;

		for(it = this->view->controller->parallelograms.begin(); it != this->view->controller->parallelograms.end(); it++) {
//			printf ("%f, %f, %f, %f, %f, %f, %f, %f\n", (*it).p1.X, (*it).p2.X, (*it).p3.X, (*it).p4.X, (*it).p1.Y, (*it).p2.Y, (*it).p3.Y, (*it).p4.Y);
			glBegin (GL_QUADS); // Remember: Counter Clockwise Winding in order to draw OpenGL Quads :)
				glVertex3f ((*it).p2.X/SCALE, (*it).p2.Y/SCALE, (*it).p2.Z/SCALE);
				glVertex3f ((*it).p1.X/SCALE, (*it).p1.Y/SCALE, (*it).p1.Z/SCALE);
				glVertex3f ((*it).p3.X/SCALE, (*it).p3.Y/SCALE, (*it).p3.Z/SCALE);
				glVertex3f ((*it).p4.X/SCALE, (*it).p4.Y/SCALE, (*it).p4.Z/SCALE);
			glEnd ();
		}
	}

	void DrawArea::setToCamera1 () {
		this->glcam_pos.X=0.;
		this->glcam_pos.Y=(int)MAXWORLD*10/1.2;
		this->glcam_pos.Z=150.;

		this->glcam_foa.X=0.;
		this->glcam_foa.Y=0.;
		this->glcam_foa.Z=0.;
	}

	void DrawArea::setToCamera2 () {
		this->glcam_pos.X=-(int)MAXWORLD*10/1.2;
		this->glcam_pos.Y=0.;
		this->glcam_pos.Z=150.;

		this->glcam_foa.X=0.;
		this->glcam_foa.Y=0.;
		this->glcam_foa.Z=0.;
	}

	void DrawArea::setToCamera3 () {
		this->glcam_pos.X=0.;
		this->glcam_pos.Y=-(int)MAXWORLD*10/1.2;
		this->glcam_pos.Z=150.;

		this->glcam_foa.X=0.;
		this->glcam_foa.Y=0.;
		this->glcam_foa.Z=0.;
	}

	void DrawArea::setToCamera4 () {
		this->glcam_pos.X=(int)MAXWORLD*10/1.2;
		this->glcam_pos.Y=0.;
		this->glcam_pos.Z=150.;

		this->glcam_foa.X=0.;
		this->glcam_foa.Y=0.;
		this->glcam_foa.Z=0.;
	}

	void DrawArea::setToPioneerCamera () {
		this->posCamRel2Robot();
	}

	void DrawArea::InitOGL (int w, int h) {
		GLfloat ambient[] = {1.0, 1.0, 1.0, 1.0};
		GLfloat diffuse[] = {1.0, 1.0, 1.0, 1.0};
		GLfloat position[] = {0.0, 3.0, 3.0, 0.0};
		GLfloat lmodel_ambient[] = {0.2, 0.2, 0.2, 1.0};
		GLfloat local_view[] = {0.0};

		glViewport(0,0,(GLint)w,(GLint)h);  
		glDrawBuffer(GL_BACK);
		glClearColor(0.6f, 0.8f, 1.0f, 0.0f);
		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		/* With this, the pioneer appears correctly, but the cubes don't */
		glLightfv (GL_LIGHT0, GL_AMBIENT, ambient);
		glLightfv (GL_LIGHT0, GL_DIFFUSE, diffuse);
		glLightfv (GL_LIGHT0, GL_POSITION, position);
		glLightModelfv (GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);
		glLightModelfv (GL_LIGHT_MODEL_LOCAL_VIEWER, local_view);
		glEnable (GL_LIGHT0);
		// glEnable (GL_LIGHTING);
    glEnable(GL_POINT_SMOOTH);

		glEnable(GL_TEXTURE_2D);     // Enable Texture Mapping
		glEnable (GL_AUTO_NORMAL);
		glEnable (GL_NORMALIZE);  
		glEnable(GL_DEPTH_TEST);     // Enables Depth Testing
		glDepthFunc(GL_LESS);  
		glShadeModel(GL_SMOOTH);     // Enables Smooth Color Shading
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	bool DrawArea::on_expose_event(GdkEventExpose* event) {
		Gtk::Allocation allocation = get_allocation();
		GLfloat width, height;

		Glib::RefPtr<Gdk::GL::Window> glwindow = get_gl_window();
		glwindow->gl_begin(get_gl_context());

		glDrawBuffer(GL_BACK);
		glClearColor(0.6f, 0.8f, 1.0f, 0.0f);

		glClearDepth(1.0);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);
		
		width = allocation.get_width();
		height = allocation.get_height();

		this->InitOGL (width, height);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();

		/*Angulo	ratio		znear, zfar*/
		gluPerspective(50.0, 	width/height, 	1.0, 50000.0);	

		glMatrixMode (GL_MODELVIEW);
		glLoadIdentity();

		/*pos cam		pto central	vector up*/
		gluLookAt(this->glcam_pos.X, this->glcam_pos.Y, this->glcam_pos.Z,
							this->glcam_foa.X, this->glcam_foa.Y, this->glcam_foa.Z,
							0., 0., 1.);

		/* DRAW WORLD */
		this->drawScene();

		/*Swap buffers*/
		if (glwindow->is_double_buffered())
			glwindow->swap_buffers();
		else
			glFlush();

		glwindow->gl_end();

		return true;
	}

	bool DrawArea::on_timeout() {
		/*Force our program to redraw*/
		Glib::RefPtr<Gdk::Window> win = get_window();
		if (win) {
			Gdk::Rectangle r(0, 0, get_allocation().get_width(), get_allocation().get_height());
			win->invalidate_rect(r, false);
		}
		return true;
	}

	void DrawArea::drawFrustrum(double depth) {
		double left   = (depth/277.)*160.;
		double right  = (depth/277.)*(160.-IMAGE_WIDTH);
		double top    = (depth/277.)*120.;
		double bottom = (depth/277.)*(120.-IMAGE_HEIGHT);

		glBegin(GL_LINE_STRIP);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(depth, left, top);
		glVertex3f(depth, left, bottom);
		glEnd();	

		glBegin(GL_LINE_STRIP);
		glVertex3f(0.0f, 0.0f, 0.0f);		
		glVertex3f(depth, left, bottom);
		glVertex3f(depth, right, bottom);
		glEnd();	

		glBegin(GL_LINE_STRIP);
		glVertex3f(0.0f, 0.0f, 0.0f);		
		glVertex3f(depth, right, bottom);
		glVertex3f(depth, right, top);
		glEnd();

		glBegin(GL_LINE_STRIP);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(depth, right, top);
		glVertex3f(depth, left, top);
		glEnd();
	}

	void DrawArea::drawCameras() {
		float x,y,z;
		float offset=0.05;
		float left,right,top,bottom,frontpoint;
		CvPoint3D32f myPoint;
		this->view->controller->getPosition (&myPoint);

		HPoint3D pos_camA=this->view->controller->myCamA.position;
		HPoint3D foa_camA=this->view->controller->myCamA.foa;

		double angle = myPoint.z;
		double pan_angleA = this->view->controller->getPan1();
		double tilt_angleA = -this->view->controller->getTilt1();

		// Draw camera A
		glPushMatrix();
		glLineWidth(2.0f);
		glColor3f( 0.0, 0.0, 0.0 );
		glTranslatef(pos_camA.X/SCALE, pos_camA.Y/SCALE, pos_camA.Z/SCALE);
		glRotatef (angle, 0., 0.,1.);
		glRotatef (pan_angleA, 0., 0.,1.);
		glRotatef (tilt_angleA, 0., 1.,0.);
		glutWireCube(0.1);
		drawFrustrum(1.5);
		glPopMatrix();

		if (this->view->controller->getNumCamerasUsed () == 2) {
			HPoint3D pos_camB=this->view->controller->myCamB.position;
			HPoint3D foa_camB=this->view->controller->myCamB.foa;
			double pan_angleB= this->view->controller->getPan2();
			double tilt_angleB = -this->view->controller->getTilt2();

			// Draw camera B
			glPushMatrix();
			glLineWidth(2.0f);
			glColor3f( 0.0, 0.0, 0.0 );
			glTranslatef(pos_camB.X/SCALE, pos_camB.Y/SCALE, pos_camB.Z/SCALE);
			glRotatef (angle, 0., 0.,1.);
			glRotatef (pan_angleB, 0., 0.,1.);
			glRotatef (tilt_angleB, 0., 1.,0.);
			glutWireCube(0.1);
			drawFrustrum(1.5);
			glPopMatrix();
		}
	}

	void DrawArea::drawScene() {
		int i,c,row,j,k;
		Tvoxel start,end;
		float r,lati,longi,dx,dy,dz;
		float matColors[4];
		float  Xp_sensor, Yp_sensor;
		float dpan=0.5,dtilt=0.5;
		CvPoint3D32f robotPos;

		// Absolute Frame of Reference
		// floor
		glColor3f( 0.6, 0.6, 0.6 );
		glLineWidth(2.0f);
		glBegin(GL_LINES);
		for(i=0;i<((int)MAXWORLD+1);i++) {
			v3f(-(int)MAXWORLD*10/2.+(float)i*10,-(int)MAXWORLD*10/2.,0.);
			v3f(-(int)MAXWORLD*10/2.+(float)i*10,(int)MAXWORLD*10/2.,0.);
			v3f(-(int)MAXWORLD*10/2.,-(int)MAXWORLD*10/2.+(float)i*10,0.);
			v3f((int)MAXWORLD*10/2.,-(int)MAXWORLD*10/2.+(float)i*10,0.);
		}
		glEnd();

		// absolute axis
		glLineWidth(3.0f);
		glColor3f( 0.7, 0., 0. );
		glBegin(GL_LINES);
		v3f( 0.0, 0.0, 0.0 );   
		v3f( 10.0, 0.0, 0.0 );
		glEnd();
		glColor3f( 0.,0.7,0. );
		glBegin(GL_LINES);
		v3f( 0.0, 0.0, 0.0 );   
		v3f( 0.0, 10.0, 0.0 );
		glEnd();
		glColor3f( 0.,0.,0.7 );
		glBegin(GL_LINES);
		v3f( 0.0, 0.0, 0.0 );   
		v3f( 0.0, 0.0, 10.0 );
		glEnd();
		glLineWidth(1.0f);

		this->drawCameras ();
		//this->pintaExtremos ();
		//this->pintaCruz ();
		this->pintaVergencyPoint ();
		//this->pintaClicados ();

		this->pintaPuntos3D ();

		if ((this->view->controller->getMemoryCombo () == 1) || (this->view->controller->getMemoryCombo () == 2)) { // show segments memory
			this->pintaMemoria ();
		}

		if ((this->view->controller->getMemoryCombo () == 1) || (this->view->controller->getMemoryCombo () == 3)) { // show parallelograms memory
			this->pintaParalelogramos ();
		}

		// botones de la ventana de OGL:
		if (this->view->controller->getSolis2 ())
			this->pintaSolis ();
		if (this->view->controller->getPredictions2 ())
			this->pintaPredicted ();
		if (this->view->controller->getNews2 ())
			this->pintaNew ();
		if (this->view->controller->getRefuted2 ())
			this->pintaRefuted ();
		if (this->view->controller->getFitted2 ())
			this->pintaFitted ();		

		// Robot Frame of Reference
		mypioneer.posx=robotPos.x/SCALE;
		mypioneer.posy=robotPos.y/SCALE;
		mypioneer.posz=0.;
		mypioneer.foax=robotPos.x/SCALE;
		mypioneer.foay=robotPos.y/SCALE;
		mypioneer.foaz=10.;
		mypioneer.roll=robotPos.z;

		glTranslatef(mypioneer.posx,mypioneer.posy,mypioneer.posz);
		dx=(mypioneer.foax-mypioneer.posx);
		dy=(mypioneer.foay-mypioneer.posy);
		dz=(mypioneer.foaz-mypioneer.posz);
		longi=(float)atan2(dy,dx)*360./(2.*PI);
		glRotatef(longi,0.,0.,1.);
		r=sqrt(dx*dx+dy*dy+dz*dz);
		if (r<0.00001) lati=0.;
		else lati=acos(dz/r)*360./(2.*PI);
		glRotatef(lati,0.,1.,0.);
		glRotatef(mypioneer.roll,0.,0.,1.);

		// X axis
		glColor3f( 1., 0., 0. );
		glBegin(GL_LINES);
		v3f( 0.0, 0.0, 0.0 );   
		v3f( 5.0, 0.0, 0.0 );
		glEnd();

		// Y axis
		glColor3f( 0., 1., 0. );  
		glBegin(GL_LINES);
		v3f( 0.0, 0.0, 0.0 );   
		v3f( 0.0, 5.0, 0.0 );
		glEnd();

		// Z axis
		glColor3f( 0., 0., 1.);
		glBegin(GL_LINES);
		v3f( 0.0, 0.0, 0.0 );   
		v3f( 0.0, 0.0, 5.0 );
		glEnd();

/*
		// robot body
		glEnable (GL_LIGHTING);
		glPushMatrix();
		glTranslatef(1.,0.,0.);

		// the body it is not centered. With this translation we center it
		loadModel(); // CARGAMOS EL MODELO DEL PIONEER
		glPopMatrix();
		glDisable (GL_LIGHTING);

		// lasers
		glLineWidth(1.0f);
		glEnable (GL_LIGHTING); // luces... entramos en parte de dibujado del pioneer con texturas
		matColors[0] = 1.0;
		matColors[1] = 0.0;
		matColors[2] = 0.0;
		matColors[3] = 0.5;
		glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,matColors);

		start.x=laser_coord[0]*10.;
		start.y=laser_coord[1];
		for(k=0;k<this->numLasers;k++) {
			Xp_sensor = this->distanceData[k]*cos(((float)k-90.)*DEGTORAD);
			Yp_sensor = this->distanceData[k]*sin(((float)k-90.)*DEGTORAD);

			// Coordenadas del punto detectado por el US con respecto al sistema del sensor, eje x+ normal al sensor
			end.x = laser_coord[0]*10. + Xp_sensor*laser_coord[3] - Yp_sensor*laser_coord[4];
			end.y = laser_coord[1] + Yp_sensor*laser_coord[3] + Xp_sensor*laser_coord[4];

			glBegin(GL_POLYGON); 
				glVertex3f (laser_coord[0]*10./100., laser_coord[1]/100., 3.2);
				glVertex3f (start.x/100., start.y/100., 3.2);
				glVertex3f (end.x/100., end.y/100., 3.2);
			glEnd();

			start.x=end.x;
			start.y=end.y;
		}
		glDisable (GL_LIGHTING);*/
	}

	/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
	void DrawArea::linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D *intersectionPoint) {
		HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...
		float t;

		v.X = (B.X - A.X);
		v.Y = (B.Y - A.Y);
		v.Z = (B.Z - A.Z);

		// We'll calculate the ground intersection (Z = 0) on our robot system. Parametric equations:
		// intersectionPoint->Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
		t = (-A.Z) / (v.Z);
		intersectionPoint->X = A.X + (t*v.X);
		intersectionPoint->Y = A.Y + (t*v.Y);
		intersectionPoint->Z = A.Z + (t*v.Z); 
	}

	bool DrawArea::on_button_press (GdkEventButton* event) {
		float x=event->x;
		float y=event->y;
		TPinHoleCamera myActualCamera;
		HPoint2D myActualPoint2D, boton;
		HPoint3D cameraPos3D, myActualPoint3D, intersectionPoint;

		if (event->button == 2) { // con el botón del centro vamos a coger el destino
			myActualCamera.position.X = this->glcam_pos.X;
			myActualCamera.position.Y = this->glcam_pos.Y;
			myActualCamera.position.Z = this->glcam_pos.Z;
			myActualCamera.foa.X = this->glcam_foa.X;
			myActualCamera.foa.Y = this->glcam_foa.Y;
			myActualCamera.foa.Z = this->glcam_foa.Z;
			myActualCamera.roll = 0.;
			myActualCamera.skew = 0.;
			myActualCamera.rows = 450;
			myActualCamera.columns = 994;
			myActualCamera.v0=497.; // 994/2
			myActualCamera.u0=225.; // 450/2
			myActualCamera.fdistx = 483.;
			myActualCamera.fdisty = 483.;
			update_camera_matrix(&myActualCamera);

			// Modificamos la asignación de valores del pixel para el backproject,
			// sistema de referencia óptico
			myActualPoint2D.x=450.-1.-event->y; 
			myActualPoint2D.y=event->x;
			myActualPoint2D.h = 1.;

			// Proyectamos tal punto en 3D sobre el Plano Imagen de nuestra cámara virtual
			backproject(&myActualPoint3D, myActualPoint2D, myActualCamera);

			// Coordenadas en 3D de la posicion de la cámara
			cameraPos3D.X = myActualCamera.position.X;
			cameraPos3D.Y = myActualCamera.position.Y;
			cameraPos3D.Z = myActualCamera.position.Z;
			cameraPos3D.H = 1;

			linePlaneIntersection (myActualPoint3D, cameraPos3D, &intersectionPoint);
			this->view->controller->robotGoal.X = intersectionPoint.Y;
			this->view->controller->robotGoal.Y = intersectionPoint.Y;
			this->view->controller->robotGoal.Z = intersectionPoint.Z;
		}
	}

	bool DrawArea::on_motion_notify(GdkEventMotion* event) {
		float desp = 0.01;
		float x=event->x;
		float y=event->y;

		/* if left mouse button is toggled */
		if (event->state & GDK_BUTTON1_MASK) { // aquí además comprobamos punto de pulsación
			if ((x - old_x) > 0.0) longi -= desp;
			else if ((x - old_x) < 0.0) longi += desp;

			if ((y - old_y) > 0.0) lati += desp;
			else if ((y - old_y) < 0.0) lati -= desp;

			this->glcam_pos.X=radius*cosf(lati)*cosf(longi) + this->glcam_foa.X;
			this->glcam_pos.Y=radius*cosf(lati)*sinf(longi) + this->glcam_foa.Y;
			this->glcam_pos.Z=radius*sinf(lati) + this->glcam_foa.Z;
		}

		/* if right mouse button is toggled */
		if (event->state & GDK_BUTTON3_MASK) {
			if ((x - old_x) > 0.0) longi -= desp;
			else if ((x - old_x) < 0.0) longi += desp;

			if ((y - old_y) > 0.0) lati += desp;
			else if ((y - old_y) < 0.0) lati -= desp;

			this->glcam_foa.X=-radius*cosf(lati)*cosf(longi) + this->glcam_pos.X;
			this->glcam_foa.Y=-radius*cosf(lati)*sinf(longi) + this->glcam_pos.Y;
			this->glcam_foa.Z=-radius*sinf(lati) + this->glcam_pos.Z;
		}

		old_x=x;
		old_y=y;
	}

	bool DrawArea::on_drawarea_scroll(GdkEventScroll * event) {
		float vx, vy, vz;

		vx = (this->glcam_foa.X - this->glcam_pos.X)/radius;
		vy = (this->glcam_foa.Y - this->glcam_pos.Y)/radius;
		vz = (this->glcam_foa.Z - this->glcam_pos.Z)/radius;

		if (event->direction == GDK_SCROLL_UP) {
			this->glcam_foa.X = this->glcam_foa.X + vx;
			this->glcam_foa.Y = this->glcam_foa.Y + vy;
			this->glcam_foa.Z = this->glcam_foa.Z + vz;

			this->glcam_pos.X = this->glcam_pos.X + vx;
			this->glcam_pos.Y = this->glcam_pos.Y + vy;
			this->glcam_pos.Z = this->glcam_pos.Z + vz;
		}

		if (event->direction == GDK_SCROLL_DOWN) {
			this->glcam_foa.X = this->glcam_foa.X - vx;
			this->glcam_foa.Y = this->glcam_foa.Y - vy;
			this->glcam_foa.Z = this->glcam_foa.Z - vz;

			this->glcam_pos.X = this->glcam_pos.X - vx;
			this->glcam_pos.Y = this->glcam_pos.Y - vy;
			this->glcam_pos.Z = this->glcam_pos.Z - vz;
		}
	}
} // namespace

