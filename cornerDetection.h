/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISUALMEMORY_CORNERDETECTION_H
#define VISUALMEMORY_CORNERDETECTION_H

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdio.h>
#include <colorspaces/colorspacesmm.h>
#include "geometry.h"
#include "cvfast.h"
#include "structs.h"
#include "image.h"
#include <sys/time.h>

#define PARALLEL_FACTOR 0.3
#define MAX_DISTANCE_LINES 10.0
#define PARALLEL_DISTANCE 10.0

using namespace visionLibrary;
using namespace std;

namespace visualMemory {
  class cornerDetection {
		public:
			static float extra_lines[200][9];
			static void cannyFilter (const colorspaces::Image &image, float cannyScale, float houghThres, float houghLong, float houghGap, bool houghActived, std::vector<float> *lines, int idCam);
			static void add_line(float x0,float y0, float z0, float x1, float y1, float z1,int color);
			static void fastCorners (const colorspaces::Image &image, std::vector<Segment2D> *segments, double fastThres, double sobelThres, bool showBlackBorder, bool showSobelBorders, bool showFastCorners, bool showFastBorders, bool showLaplaceBorders, bool showLaplaceContours);
			static void solisAlgorithm (const colorspaces::Image &image, std::vector<Segment2D> *segments, double solisSegmentsThres);
			static void solisAlgorithm2 (IplImage &image, std::vector<Segment2D> *segments, double solisSegmentsThres);
			static void solisAlgorithm3 (IplImage &image, std::vector<Segment2D> *segments);
			static int findClosestPoint (CvPoint *p1, CvPoint *p2, CvSeq *points, int maxDist);
			static int isVertical(CvPoint *p1, CvPoint *p2);
			static int isHorizontal(CvPoint *p1, CvPoint *p2);
			static int isRight(CvPoint *pt1, CvPoint *pt2);
			static int isLeft(CvPoint *pt1, CvPoint *pt2);
			static int isBelow(CvPoint *pt1, CvPoint *pt2);
			static int isAbove(CvPoint *pt1, CvPoint *pt2);
		private:
			static const int solis_cases[][11];
			static const int CASES_OFFSET;
	};
}

#endif
