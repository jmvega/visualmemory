/*
*  Copyright (C) 1997-2011 JDERobot Developers Team
 *
 *  This program is free software; you can redistribute it and/or modifdisty
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *   Authors : Eduardo Perdices <eperdices@gsyc.es>,
 *             Jose María Cañas Plaza <jmplaza@gsyc.es>
 *
 */

#include "ColorFilter.h"

const unsigned char ColorFilter::sliceOrange[3] 	=	{29., 198., 255.};
const unsigned char ColorFilter::sliceBlue[3]		= 	{255., 205., 73.};
const unsigned char ColorFilter::sliceYellow[3] 	= 	{94., 218., 212.};
const unsigned char ColorFilter::sliceGreen[3]  	= 	{0., 110., 0.};
const unsigned char ColorFilter::sliceWhite[3]  	= 	{255, 255, 255};

/*RoboCup colors*/
/*const float ColorFilter::hsvOrange[6] 	=	{0.3, 0.6, 0.85, 1.0, 180.0, 255.0};
const float ColorFilter::hsvBlue[6] 	=	{3.2, 4.0, 0.5, 0.95, 127.0, 255.0};
const float ColorFilter::hsvYellow[6] 	=	{0.5, 1.3, 0.45, 0.85, 160.0, 255.0};
const float ColorFilter::hsvGreen[6] 	=	{1.24, 2.08, 0.3, 0.9, 0.0, 160.0};
const float ColorFilter::hsvWhite[6] 	=	{0.0, 6.28, 0.0, 0.25, 120.0, 255.0};*/

/*Building colors*/
const float ColorFilter::hsvOrange[6] 	=	{5.8, 0.35, 0.3, 1.0, 0.0, 190.0};
const float ColorFilter::hsvBlue[6] 	=	{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
const float ColorFilter::hsvYellow[6] 	=	{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
const float ColorFilter::hsvGreen[6] 	=	{0.0, 0.0, 0.0, 0.0, 0.0, 0.0};
const float ColorFilter::hsvWhite[6] 	=	{0.35, 5.8, 0.0, 0.5, 140.0, 255.0};

/**
 * Class constructor that initializes the specific color filter with a set of HSV thresholds and
 * with the parameters of the images to filter.
 *
 * @param hmin Sets the lower threshold of the H filter's component.
 * @param hmax Sets the upper threshold of the H filter's component.
 * @param smin Sets the lower threshold of the S filter's component.
 * @param smax Sets the upper threshold of the S filter's component.
 * @param vmin Sets the lower threshold of the V filter's component.
 * @param vmax Sets the upper threshold of the V filter's component.
 * @param width Sets the width of the image to filter.
 * @param height Sets the height of the image to filter.
 * @param channels Sets the number of the channels of the image to filter.
 */
ColorFilter::ColorFilter() {
}


/**
 * Class destructor.
 **/
ColorFilter::~ColorFilter() {
}

/**
 * Obtain the color of the pixel selected
 * @param image Source image to filter.
 **/
int
ColorFilter::getColor(const int cam, char *image, int col, int row)
{
	int pixel = row * ImageInput::IMG_WIDTH + col;
	unsigned char r,g,b;
	float h, s, v;

	/*Get HSV values*/
	r = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 0];
	g = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 1];
	b = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 2];
	this->rgb2hsv(r, g, b, h, s, v);

	/*Check filters*/
	if(this->hsvfilter(h, s, v, ColorFilter::hsvGreen))
		return ImageInput::CGREEN;

	if(this->hsvfilter(h, s, v, ColorFilter::hsvOrange))
		return ImageInput::CORANGE;

	if(this->hsvfilter(h, s, v, ColorFilter::hsvBlue))
		return ImageInput::CBLUE;

	if(this->hsvfilter(h, s, v, ColorFilter::hsvYellow))
		return ImageInput::CYELLOW;

	if(this->hsvfilter(h, s, v, ColorFilter::hsvWhite))
		return ImageInput::CWHITE;

	return ImageInput::CUNKNOWN;
}

/**
 * Obtain the color in RGB of the pixel selected
 * @param image Source.
 **/
void
ColorFilter::getColorRGB(const int cam, char *image, int col, int row, char &r, char &g, char &b, bool filter, int color)
{
	unsigned char rt=0, gt=0, bt=0, intensity;	
	int pixcolor;
	int pixel;

	if(!filter) {
		pixel = row * ImageInput::IMG_WIDTH + col;

		/*Get real pixel color*/
		rt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 0];
		gt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 1];
		bt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 2];
	} else {
		/*Filter pixel*/
		pixcolor = this->getColor(cam, image, col, row);
		switch(pixcolor) {
		case ImageInput::CGREEN:
			rt = sliceGreen[2];
			gt = sliceGreen[1];
			bt = sliceGreen[0];
			break;
		case ImageInput::CORANGE:
			rt = sliceOrange[2];
			gt = sliceOrange[1];
			bt = sliceOrange[0];
			break;
		case ImageInput::CBLUE:
			rt = sliceBlue[2];
			gt = sliceBlue[1];
			bt = sliceBlue[0];
			break;
		case ImageInput::CYELLOW:
			rt = sliceYellow[2];
			gt = sliceYellow[1];
			bt = sliceYellow[0];
			break;
		case ImageInput::CWHITE:
			rt = sliceWhite[2];
			gt = sliceWhite[1];
			bt = sliceWhite[0];
			break;
		default:

			pixel = row * ImageInput::IMG_WIDTH + col;

			/*Get pixel in gray scale*/
			rt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 0];
			gt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 1];
			bt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 2];

			intensity = 0.1 * bt + 0.1 * gt + 0.1 * rt;
			rt = intensity;
			gt = intensity;
			bt = intensity;
			break;			
		} 

		/*If a color is selected, we only filter that color*/
		if(color > 0) {
			if(color != pixcolor && pixcolor != ImageInput::CUNKNOWN) {
				pixel = row * ImageInput::IMG_WIDTH + col;

				/*Get pixel in gray scale*/
				rt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 0];
				gt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 1];
				bt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 2];

				intensity = 0.1 * bt + 0.1 * gt + 0.1 * rt;
				rt = intensity;
				gt = intensity;
				bt = intensity;
			}
		}
	}

	r = (char) rt;
	g = (char) gt;
	b = (char) bt;
}

/**
 * Obtain the color in RGB of the pixel selected
 * @param image Source.
 **/
void
ColorFilter::getColorHSV(const int cam, char *image, int col, int row, char &h, char &s, char &v)
{
	unsigned char rt=0, gt=0, bt=0;	
	float ht=0, st=0, vt=0;	
	int pixel, posSrc;

	pixel = row * ImageInput::IMG_WIDTH + col;
	posSrc = pixel * 2;

	/*Get real pixel color*/
	rt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 0];
	gt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 1];
	bt = (unsigned char) image[pixel*ImageInput::IMG_CHANNELS + 2];

	/*Convert to HSV*/
	this->rgb2hsv(rt, gt, bt, ht, st, vt);

	ht = 255.0 * ht / 360.0;
	st = st * 100.0;
	vt = vt;

	h = (char) (unsigned char) ht;
	s = (char) (unsigned char) st;
	v = (char) (unsigned char) vt;
}

void
ColorFilter::rgb2hsv(unsigned char r, unsigned char g, unsigned char b, float& H, float& S, float& V)
{
	double min, max;

	// Calculamos el minimo
	if ((r <= g) && (r <= b))
		min = r;
	else if ((g <= r) && (g <= b))
		min = g;
	else
		min = b;

	// Calculamos el máximo
	if ((r >= g) && (r >= b))
		max = r;
	else if ((g >= r) && (g >= b))
		max = g;
	else
		max = b;

	//printf("min=%.1f - max=%.1f - r=%.1f - g=%.1f - b=%.1f\n",min,max,r,g,b);
	// Calculamos valor de H
	if (max==min) {
		H=.0; // En estos casos, H no tiene sentido
	} else if (max==r && g>=b) {
		H=60*((g-b)/(max-min));
	} else if (max==r && g<b) {
		H=60*((g-b)/(max-min))+360;
	} else if (max==g) {
		H=60*((b-r)/(max-min))+120;
	} else if (max==b) {
		H=60*((r-g)/(max-min))+240;
	}

	// Calculamos el valor de S
	if (max==0)
		S=0.0;
	else
		S= 1-(min/max);

	// Calculamos el valor si V
	V=max;
}

bool
ColorFilter::hsvfilter(float H, float S, float V, const float * filter)
{
	float hmax, hmin, smax, smin, vmax, vmin;

	hmin = filter[0];
	hmax = filter[1];
	smin = filter[2];
	smax = filter[3];
	vmin = filter[4];
	vmax = filter[5];

	/*Convert parameters*/
	H = H*PI/180.0;

	if(!((S <= smax) && (S >= smin) && (V <= vmax) && (V >= vmin)))
		return false;

	if(hmin < hmax) {
		if((H <= hmax) && (H >= hmin))
			return true;
	} else {
		if(((H >= 0.0) && (H <= hmax)) || ((H <= 2*PI) && (H >= hmin))) 
			return true;
	}

	return false;
}
