#include "structs.h"
#include <stdio.h>
#include <sys/time.h>
#include "controller.h"

#ifndef VISUALMEMORY_DEPURACION_H
#define VISUALMEMORY_DEPURACION_H

namespace visualMemory {
  class depuracion {
		public:
			static const int DEBUG;

		  depuracion();
		  virtual ~depuracion();

			static void printPoint (HPoint3D point);
			static void printParallelogram (Parallelogram3D par1);
			static void print_time (double initTime, double endTime, char* message);
	};
}

#endif

