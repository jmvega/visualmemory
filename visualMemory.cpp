/*
 *
 *  Copyright (C) 2011 Julio Vega
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/. 
 *
 *  Author : Julio Vega <julio.vega@urjc.es>
 *
 */

// ICE includes
#include <Ice/Ice.h>
#include <IceUtil/IceUtil.h>
#include <jderobotice/component.h>
#include <jderobotice/application.h>

#include "memory3Dinterfaz.h"
#include "view.h"
#include "memory.h"
#include "attention.h"
#include "vergency.h"
#include "bufferMemory.h"
#include "SimulatorPioneer.h"
#include "NaoRobot.h"
#include "PioneerPlatform.h"

using namespace std;

namespace visualMemory {
	class Memory3DI: virtual public jderobot::Memory {
		private:
			visualMemory::bufferMemory *myBuffer;
			jderobot::MemoryDataPtr memoryData;
			std::vector <Segment3D> segmentMemory;
			std::string prefix;
			jderobotice::Context context;

		public:
			Memory3DI (std::string& propertyPrefix, const jderobotice::Context& context)
			: prefix(propertyPrefix), context(context), memoryData(new jderobot::MemoryData()) {
				Ice::PropertiesPtr prop = context.properties();
				myBuffer = visualMemory::bufferMemory::getInstance();
				segmentMemory.clear ();
				memoryData->segmentsData.resize(sizeof(float)*prop->getPropertyAsIntWithDefault(prefix+"NumberOfSegments",1000));
				memoryData->numSegments = 0;
			};

			virtual jderobot::MemoryDataPtr getMemory3D (const Ice::Current&) {
				std::vector<Segment3D>::iterator it;

				segmentMemory = myBuffer->getBuffer ();
				int i = 0, j = 0;

				for(it = this->segmentMemory.begin(); it != this->segmentMemory.end(); it++, j++, i=j*6) {
					memoryData->segmentsData[i].a.x = (*it).start.X;
					memoryData->segmentsData[i].a.y = (*it).start.Y;
					memoryData->segmentsData[i].a.z = (*it).start.Z;
					memoryData->segmentsData[i].a.color = (*it).colorStart;
					memoryData->segmentsData[i].b.x = (*it).end.X;
					memoryData->segmentsData[i].b.y = (*it).end.Y;
					memoryData->segmentsData[i].b.z = (*it).end.Z;
					memoryData->segmentsData[i].b.color = (*it).colorEnd;
				}
				memoryData->numSegments = j;
				return memoryData;
			};

			virtual ~Memory3DI() {};
	}; // end Memory3DI class

	// COMPONENT
	class Component: public jderobotice::Component {
		public:
			Component():jderobotice::Component("VisualMemory"), memory1(0) {}

			virtual void start() {
				// Memoria
				Ice::PropertiesPtr prop = context().properties();
				std::string objPrefix="VisualMemory.";
				std::string memoryName = prop->getProperty(objPrefix + "Memory");
				context().tracer().info("Creating " + objPrefix + memoryName);
				memory1 = new Memory3DI (objPrefix,context());
				context().createInterfaceWithString(memory1,memoryName);
				//usleep(1000); // para que pille los valores, que parece que le cuesta un tiempo...
			}

			virtual ~Component(){}

		private:
			Ice::ObjectPtr memory1;
	};

	void* mycallback2(void* obj);

  class ServerThread { 
		private:
			int argc;
			char** argv;
			visualMemory::Component *component;
			bool isRunning;
		
		public:
			int	main () {
				component = new visualMemory::Component ();
				jderobotice::Application app (*component);
				return app.jderobotMain (this->argc,this->argv);
			}

			void run (int argc, char** argv) {
				this->argc = argc;
				this->argv = argv;
				pthread_create(&thread, 0, &mycallback2, this);
			}

			void stop() {
				this->isRunning=false;
			}

			int	join() {
				return pthread_join(thread, ret);
			}

			pthread_t thread;
			void** ret;
	}; // end class

	void* mycallback2(void* obj) {
		static_cast<ServerThread*>(obj)->main();
		return(0);
	} // mycallback2

	void* mycallback(void* obj);

  class ControlThread { 
		private:
			visualMemory::Controller * controller;
			visualMemory::View * view;
			visualMemory::Memory *memory;
			visualMemory::Attention *attention;
			visualMemory::Vergency *vergency;
			visualMemory::bufferMemory *myBuffer;
			bool isRunning;
		
		public:
			int	main() {
				struct timeval a, b;
				int cycle = 33;
				long totalb,totala;
				long diff;

				this->isRunning=true;

				while (this->isRunning) {
					if (((this->controller->getIteration ()) && (this->controller->getPlusIteration())) || (!this->controller->getIteration ())) {
						gettimeofday(&a,NULL);
						totala=a.tv_sec*1000000+a.tv_usec;

						// Actualización de sensores/actuadores. Modelo vista controlador.
						this->controller->iteration ();

						// MEMORIA 3D: análisis 2D, paso a 3D (hip. suelo), mantenimiento (elim. duplicados)
						this->memory->iteration ();

						// SIST. ATENTIVO: dinámica saliencia/vida de los elementos a atender
						//this->attention->iteration ();

						this->vergency->iteration ();

						this->myBuffer->setBuffer (this->controller->segmentMemory);

						this->view->iteration (); // actualizamos las estructuras del visualizad

						this->controller->setPlusIteration (false);

						gettimeofday(&b,NULL);
						totalb=b.tv_sec*1000000+b.tv_usec;
						//std::cout << "visualMemory takes " << (totalb-totala)/1000 << " ms" << std::endl;

						diff = (totalb-totala)/1000;
						if(diff < 0 || diff > cycle)
							diff = cycle;
						else
							diff = cycle-diff;

						/*Sleep Algorithm*/
						if(diff < 10)
							diff = 10;

						usleep(diff*1000);
					}
				}

				return 0;
			}

			void run(visualMemory::Controller *controller, visualMemory::View *view, visualMemory::Memory *memory, visualMemory::Attention *attention, visualMemory::Vergency *vergency) {
				this->controller = controller;
				this->view = view;
				this->memory = memory;
				this->attention = attention;
				this->vergency = vergency;
				myBuffer = visualMemory::bufferMemory::getInstance();

				pthread_create(&thread, 0, &mycallback, this);
			}

			void stop() {
				this->isRunning=false;
			}

			int	join() {
				return pthread_join(thread, ret);
			}

			pthread_t thread;
			void** ret;
	}; // end class

	void* mycallback(void* obj) {
		static_cast<ControlThread*>(obj)->main();
		return(0);
	} // mycallback
} // end namespace

int main(int argc, char** argv){
  int status,i;
	visualMemory::Controller *controller;
	visualMemory::View *view; // vista de los datos vertidos por controller
	visualMemory::Memory *memory; // prepara la memoria visual alojada en controller
	visualMemory::Attention *attention; // maneja el sistema atentivo según la memoria
	visualMemory::Vergency *vergency;
	visualMemory::ControlThread controlThread;
	visualMemory::ServerThread serverThread;
	RoboCompJointMotor::JointMotorPrx jprx;

	jderobot::MotorsPrx mprx;
	jderobot::EncodersPrx eprx;

	jderobot::CameraPrx cprx1;
	jderobot::Pose3DMotorsPrx ptmprx1;
	jderobot::Pose3DEncodersPrx pteprx1;
	jderobot::CameraPrx cprx2;
	jderobot::Pose3DMotorsPrx ptmprx2;
	jderobot::Pose3DEncodersPrx pteprx2;

  Ice::CommunicatorPtr ic;

  try{ // Connect to my components as a CLIENT
		serverThread.run ((int)argc, (char**)argv); // launch THREAD AS A SERVER

    ic = Ice::initialize(argc,argv);

		Ice::PropertiesPtr prop = ic->getProperties ();

		int numCamerasUsed = prop->getPropertyAsIntWithDefault("VisualMemory.NumCameras",2);
		int justNeck = prop->getPropertyAsIntWithDefault("VisualMemory.JustNeck",0);
		std::string typeOfRobot = prop->getPropertyWithDefault("VisualMemory.TypeOfRobot","Pioneer");

		if (typeOfRobot == "Pioneer") {
			// Contact to GIRAFFE interface
			Ice::ObjectPrx giraffe = ic->propertyToProxy("VisualMemory.JointMotor.Proxy");
			if (0==giraffe)
				throw "Could not create proxy to giraffe server";

			// Cast to JointMotorPrx
			jprx = RoboCompJointMotor::JointMotorPrx::checkedCast(giraffe);
			if (0==jprx)
				throw "Invalid proxy";
		}

    if (((typeOfRobot == "Pioneer") && (justNeck == 0)) || (typeOfRobot=="Nao") || (typeOfRobot=="Simulator")) {
			// Contact to MOTORS interface
		  Ice::ObjectPrx baseMotors = ic->propertyToProxy("VisualMemory.Motors.Proxy");
		  if (0==baseMotors)
		    throw "Could not create proxy with Motors";
		  // Cast to motors
		  mprx = jderobot::MotorsPrx::checkedCast(baseMotors);
		  if (0==mprx)
		    throw "Invalid proxy VisualMemory.Motors.Proxy";

			// Contact to ENCODERS interface
		  Ice::ObjectPrx baseEncoders = ic->propertyToProxy("VisualMemory.Encoders.Proxy");
		  if (0==baseEncoders)
		    throw "Could not create proxy with Encoders";

		  // Cast to encoders
		  eprx = jderobot::EncodersPrx::checkedCast(baseEncoders);
		  if (0==eprx)
		    throw "Invalid proxy VisualMemory.Encoders.Proxy";

			// Contact to PTENCODERS interface
		  Ice::ObjectPrx ptencoders1 = ic->propertyToProxy("VisualMemory.Pose3DEncoders1.Proxy");
		  if (0==ptencoders1)
		    throw "Could not create proxy with Pose3DEncoders1";

		  // Cast to encoders
			pteprx1 = jderobot::Pose3DEncodersPrx::checkedCast(ptencoders1);
		  if (0==pteprx1)
		    throw "Invalid proxy VisualMemory.Pose3DEncoders1.Proxy";

			// Contact to PTMOTORS interface
		  Ice::ObjectPrx ptmotors1 = ic->propertyToProxy("VisualMemory.Pose3DMotors1.Proxy");
		  if (0==ptmotors1)
		    throw "Could not create proxy with Pose3DMotors1";

		  // Cast to ptmotors
		  ptmprx1 = jderobot::Pose3DMotorsPrx::checkedCast(ptmotors1);
		  if (0==ptmprx1)
		    throw "Invalid proxy VisualMemory.Pose3DMotors1.Proxy";

			if (numCamerasUsed == 2) {
				// Contact to PTMOTORS interface
				Ice::ObjectPrx ptmotors2 = ic->propertyToProxy("VisualMemory.Pose3DMotors2.Proxy");
				if (0==ptmotors2)
				  throw "Could not create proxy with Pose3DMotors2";

				// Cast to ptmotors
				ptmprx2 = jderobot::Pose3DMotorsPrx::checkedCast(ptmotors2);
				if (0==ptmprx2)
				  throw "Invalid proxy VisualMemory.Pose3DMotors2.Proxy";

				// Contact to PTENCODERS interface
				Ice::ObjectPrx ptencoders2 = ic->propertyToProxy("VisualMemory.Pose3DEncoders2.Proxy");
				if (0==ptencoders2)
				  throw "Could not create proxy with Pose3DEncoders2";

				// Cast to encoders
				pteprx2 = jderobot::Pose3DEncodersPrx::checkedCast(ptencoders2);
				if (0==pteprx2)
				  throw "Invalid proxy VisualMemory.Pose3DEncoders2.Proxy";
			}
		}

		// Get driver camera
		Ice::ObjectPrx camara1 = ic->propertyToProxy("VisualMemory.Camera1.Proxy");
		if (0==camara1)
			throw "Could not create proxy to Camera1 server";

		// cast to CameraPrx
		cprx1 = jderobot::CameraPrx::checkedCast(camara1);
		if (0==cprx1)
			throw "Invalid proxy";

		if (numCamerasUsed == 2) {
			// Get driver camera
			Ice::ObjectPrx camara2 = ic->propertyToProxy("VisualMemory.Camera2.Proxy");
			if (0==camara2)
				throw "Could not create proxy to Camera2 server";

			// cast to CameraPrx
			cprx2 = jderobot::CameraPrx::checkedCast(camara2);
			if (0==cprx2)
				throw "Invalid proxy";
		}


		/**************************************************************************/
		// Create Controller and View
		if (typeOfRobot == "Pioneer") {
			if (justNeck == 0) { // también cargamos la base motora
				if (numCamerasUsed == 1)
					controller = new visualMemory::Controller (mprx, eprx, jprx, cprx1);
				else if (numCamerasUsed == 2)
					controller = new visualMemory::Controller (mprx, eprx, jprx, cprx1, cprx2);
			} else if (justNeck == 1) { // no cargamos la base motora
				if (numCamerasUsed == 1)
					controller = new visualMemory::Controller (jprx, cprx1);
				else if (numCamerasUsed == 2)
					controller = new visualMemory::Controller (jprx, cprx1, cprx2);
			}
		} else if (numCamerasUsed == 1)
			controller = new visualMemory::Controller (mprx, eprx, cprx1, ptmprx1, pteprx1);
		else if (numCamerasUsed == 2)
			controller = new visualMemory::Controller (mprx, eprx, cprx1, ptmprx1, pteprx1, cprx2, ptmprx2, pteprx2);

		if (typeOfRobot == "Simulator")
			controller->robotPlatform = new visualMemory::SimulatorPioneer ();
		else if (typeOfRobot == "Pioneer")
			controller->robotPlatform = new visualMemory::PioneerPlatform ();
		else if (typeOfRobot == "Nao")
			controller->robotPlatform = new visualMemory::NaoRobot ();

		controller->putTypeOfRobot (typeOfRobot);

		view = new visualMemory::View (controller);
		memory = new visualMemory::Memory (controller);
		attention = new visualMemory::Attention (controller);
		vergency = new visualMemory::Vergency (controller);

		controlThread.run (controller, view, memory, attention, vergency);

		while(view->isVisible()){
			view->display(); // display image
			usleep(100*1000); // sleep GUI
		}
  } catch (const Ice::Exception& ex) {
    std::cerr << ex << std::endl;
    status = 1;
  } catch (const char* msg) {
    std::cerr << msg << std::endl;
    status = 1;
  }

	/*Stop algorithm controlThread*/
	controlThread.stop();
	controlThread.join();

  if (ic)
    ic->destroy();
  return status;
}

