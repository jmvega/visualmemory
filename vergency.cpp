/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "vergency.h"

namespace visualMemory {
	Vergency::Vergency (Controller* controller) {
		this->controller = controller;
		this->grayImage = cvCreateImage(cvSize(IMAGE_WIDTH, IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
		this->patch = cvCreateImage(cvSize(PATCH_SIZE, PATCH_SIZE), IPL_DEPTH_32S, 1);
		this->patch_tmp = cvCreateImage(cvSize(PATCH_SIZE, PATCH_SIZE), 8, 1);
		this->product = cvCreateImage(cvSize(PATCH_SIZE, PATCH_SIZE), IPL_DEPTH_32S, 1);
		this->patchedImgA = cvCreateImage(cvSize(PATCH_SIZE, PATCH_SIZE), IPL_DEPTH_32S, 1);
		this->patchedImgB = cvCreateImage(cvSize(PATCH_SIZE, PATCH_SIZE), IPL_DEPTH_32S, 1);
	}

	Vergency::~Vergency () {}

	HPoint2D Vergency::getMiddleOfSegment (Segment2D segment) {
		HPoint2D point;

		point.x = (segment.start.x + segment.end.x) / 2;
		point.y = (segment.start.y + segment.end.y) / 2;
		point.h = 1.;

		return point;
	}

	void Vergency::iteration () {
		double vergency_time1, vergency_time2, vergency_time3;

		std::vector<Segment2D>::iterator it;
		HPoint2D point2D_A;
		bool analizeNeighboring = false;
		bool manualVergency = false;
		CvPoint p1, p2;
		IplImage src;
		this->updateStructures ();

		for (it = this->controller->solisSegments1.begin(); (it != this->controller->solisSegments1.end()) && (!manualVergency); it++) {
			vergency_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;

			manualVergency=this->controller->getManualVergency();

			if (manualVergency)	point2D_A = this->controller->clickedPixelA;
			else point2D_A = this->getMiddleOfSegment (*it);

			this->calculateEpipolar (point2D_A, this->controller->myCamA, this->controller->myCamB); // rellena el vector epipolarPoints

			printf ("points %i\n", this->epipolarPoints.size());
			p1.x = this->epipolarPoints[1].x;
			p1.y = this->epipolarPoints[1].y;
			p2.x = this->epipolarPoints[this->epipolarPoints.size()-1].x;
			p2.y = this->epipolarPoints[this->epipolarPoints.size()-1].y;

			src = *this->image2;

			cvLine (&src, p1, p2, CV_RGB(0,255,0), 2, 8);

			this->calculatePatchDavison (*this->image1, point2D_A, this->patchedImgA, this->patch_avgA, this->patch_sdvA); // rellena la imagen-parche de A
			this->matchPoint (point2D_A, analizeNeighboring); // emparejamiento y triangulación

			vergency_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
			depuracion::print_time (vergency_time1, vergency_time2,"Vergency: One segment triangulation takes");
			printf ("______________________________________________________________________\n");
		}

		vergency_time3 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		depuracion::print_time (vergency_time1, vergency_time3,"Vergency: One full iteration takes");
	}

	void Vergency::updateStructures () {
		// Tratamiento de imágenes
		//pthread_mutex_lock(&this->controller->cameraMutex); // lock
    colorspaces::Image::FormatPtr fmt1 = colorspaces::Image::Format::searchFormat(this->controller->data1->description->format);
    if (!fmt1)
			throw "Format not supported";

    this->image1 = new colorspaces::Image (this->controller->data1->description->width,
		       this->controller->data1->description->height,
		       fmt1,
		       &(this->controller->data1->pixelData[0]));

	  colorspaces::Image::FormatPtr fmt2 = colorspaces::Image::Format::searchFormat(this->controller->data2->description->format);
	  if (!fmt2)
			throw "Format not supported";

	  this->image2 = new colorspaces::Image (this->controller->data2->description->width,
			     this->controller->data2->description->height,
			     fmt2,
			     &(this->controller->data2->pixelData[0]));

		//pthread_mutex_unlock(&this->controller->cameraMutex); // unlock
	}

	void Vergency::matchPoint (HPoint2D point2D_A, bool analizeNeighboring) {
		std::vector<HPoint2D>::iterator it1, it2;
		std::vector<Segment2D>::iterator it;
		double match_time1, match_time2, match_time3, match_time4;

		HPoint2D p2D, p2D_aux, p2D_max, point2D_B, p2D_match, intersection;
		HPoint3D p3d_fin;
		//double dataDistGraph[SIFNTSC_COLUMNS], dataDistGraphAux[SIFNTSC_COLUMNS];
		int i, j, k, l, isInside;
		double distPatch, distPatchMin = 100000000.0,  distPatchMax = 0.0, current_dist = 100000000.0, dist, minDist, minDistAux, distEpipolar;
		Segment2D epipolar;
		bool manualVergency = false, isIntersection = false;
		IplImage src1, src2;
		CvPoint p1, p2;

		src1 = *this->image1;
		src2 = *this->image2;

		p1.x = point2D_A.x;
		p1.y = point2D_A.y;
		cvCircle (&src1, p1, 2, CV_RGB(0,0,255), 2, 8);

		match_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		epipolar.start = epipolarPoints [0];
		epipolar.end = epipolarPoints [epipolarPoints.size()-1];

		// Searching the epipolar filter points
		for (i = 0, it = this->controller->solisSegments2.begin(); (it != this->controller->solisSegments2.end()) && (!manualVergency); it++, i++) { // sólo tendremos en cuenta los segmentos que intersecten con la epipolar
			//manualVergency = this->controller->getManualVergency();
			//if (manualVergency) p2D = this->controller->clickedPixelB;
			/*else { p2D = this->getMiddleOfSegment(*it); */ 

			isIntersection = geometry::LineIntersect ((*it).start.x, (*it).start.y, (*it).end.x, (*it).end.y, epipolar.start.x, epipolar.start.y, epipolar.end.x, epipolar.end.y, &intersection.x, &intersection.y);

			if (isIntersection) { // intersectan el segmento y la epipolar
				intersection.h = 1.;
				p2D = intersection;
				//epipolarCandidatePoints.clear ();
				//this->isCandidatePoint (p2D, *this->image2, true); // obtains the candidate points
				// calcule the patch for all candidates					
				//for (it2 = this->epipolarCandidatePoints.begin(); it2 != this->epipolarCandidatePoints.end(); it2++) {
				//	p2D_aux = (*it2);
				//	dist = 0.0;

				this->calculatePatchDavison (*this->image2, p2D, this->patchedImgB, this->patch_avgB, this->patch_sdvB);

				distPatch = this->comparePatchsDavison (this->patchedImgA, this->patchedImgB, this->patch_avgA, this->patch_sdvA, this->patch_avgB, this->patch_sdvB);

				printf ("%f, %f\n", distPatch, distPatchMin);

				if (distPatch < distPatchMin) {
						distPatchMin = distPatch;
						p2D_max = p2D;
				}
				//}
		
				//dataDistGraphAux[i] = current_dist; // hemos cogido la menor de las distancias (la que mejor se ajusta)
		
				//printf("dataDistGraphAux[i]=%.2f\n",dataDistGraphAux[i]);
		
				/*if (current_dist < distPatchMin) {
					distPatchMin = current_dist;
					p2D_match = p2D_max;
				}*/
			
				//if (current_dist > distPatchMax) distPatchMax = current_dist;		
			}
		}

		match_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		depuracion::print_time (match_time1, match_time2,"--MatchPoint searching on epipolar takes ");

		//printf ("MatchPoint: candidate points searching takes %d\n", match_time2-match_time1);
		//printf ("2\n");
		/*gettimeofday (&pica1_endTime,NULL);			
		print_timeMessage (pica1_initTime,pica1_endTime,"Calculo del punto homólogo:");*/

//		if (distPatchMax == distPatchMin) printf(" ***** WARNING \n"); // if denom nulo
/*
		for (i=0; i<(this->controller->solisSegments2.size()*2); i++) {					
			dataDistGraphAux[i] =  ((distPatchMax - dataDistGraphAux[i])/(distPatchMax-distPatchMin)) * 49.0 ;
			//printf ("(%.4f) %.4f \n ",distPatchMax,dataDistGraphAux[i]);
		}*/
		//printf ("3\n");

/*		struct timeval vecinos_initTime, vecinos_endTime;
		gettimeofday (&vecinos_initTime,NULL);*/
/*
		if (analizeNeighboring) { // Se analiza los vecinos para cada punto que está a menos de 3unidades de distancia del valor maximo
			minDist = 1000.0;
			
			for (k=0; k<this->epipolarPoints.size(); k++) {
				if (dataDistGraph[k] <= 2.0) {
					minDistAux = 0.0;
					for (l=0;l<20;l++) {
						minDistAux = minDistAux + fabs (dataDistGraph[k]-dataDistGraph[k-l]);
						minDistAux = minDistAux + fabs (dataDistGraph[k]-dataDistGraph[k+l]);
					}
					minDistAux += 50.0 - dataDistGraph[k];

					if (minDistAux < minDist) {
						minDist = minDistAux;
						p2D_match = this->epipolarPoints[k];
					}
					//printf ("dataDistGraph[%d]=%.2f  -- DistCurrent: %.2f -- Dist: %.2f\n",k,dataDistGraph[k],minDistAux,minDist);
				}
			}
			/*gettimeofday (&pica1_endTime,NULL);			
			print_timeMessage (pica1_initTime,pica1_endTime,"Calculo de los vecinos:");*/
		//} // end if analizeNeighboring

		match_time3 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		depuracion::print_time (match_time2, match_time3,"--MatchPoint analize neighboring takes ");
/*
		for (i = 0; i<(this->controller->solisSegments1.size()*2); i++) { // change values for draw graph
			if (dataDistGraphAux[i] > 49.0) dataDistGraphAux[i] = 49.0;

			dataDistGraph[i] = 49.0 - dataDistGraphAux[i]; // the drawing model is 50 => 0 to 49

			// Many candidate points are between 0 and 1. In graph,
			// 0.2 and 0.4 are in the same position 0.
			// This operation don't affect to decision, only affect in graph mode	
			if (dataDistGraph[i] < 2.0) dataDistGraph[i] = dataDistGraph[i]*10;

			//printf("Value[%d]=%.2f\n",i,dataDistGraph[i]);
		}*/

		point2D_B = p2D_max; // we obtaing the choosen point

		p2.x = point2D_B.x;
		p2.y = point2D_B.y;
		cvCircle (&src2, p2, 2, CV_RGB(0,0,255), 2, 8);	
	
		//distPatch = (int) distPatchMin;
		
		this->calculatePatchDavison (*this->image2, point2D_B, this->patchedImgB, this->patch_avgB, this-> patch_sdvB);				

		p3d_fin = this->triangular (point2D_A, point2D_B, this->controller->myCamA, this->controller->myCamB);

		printf ("Found final 3D point = (%2.2f,%2.2f,%2.2f) \n",p3d_fin.X, p3d_fin.Y, p3d_fin.Z);

		this->controller->pointMemory.push_back(p3d_fin);

		this->controller->vergency3Dpoint.X = p3d_fin.X;
		this->controller->vergency3Dpoint.Y = p3d_fin.Y;
		this->controller->vergency3Dpoint.Z = p3d_fin.Z;
	
		match_time4 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		depuracion::print_time (match_time1, match_time4,"MatchPoint finally takes");
	}

	/* 
	 * \brief Triangula la posicion en 3D de un punto indicado por un pixel en cada camara 
	 */
	HPoint3D Vergency::triangular (HPoint2D point2D_camA, HPoint2D point2D_camB, TPinHoleCamera camA, TPinHoleCamera camB) {
		HPoint3D a3D, b3D, vector, vector2, R, S, final;
		float A, B, C, D, E, sigma, theta;
		double triangular_time1, triangular_time2;

		triangular_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;

		this->pixel2optical(&camA, &point2D_camA);
		this->pixel2optical(&camB, &point2D_camB);
		backproject(&a3D, point2D_camA, camA);
		backproject(&b3D, point2D_camB, camB);

		/* Cálculo del Vector director de las rectas */
		vector.X = camA.position.X - a3D.X;
		vector.Y = camA.position.Y - a3D.Y;
		vector.Z = camA.position.Z - a3D.Z;
		vector2.X = camB.position.X - b3D.X;
		vector2.Y = camB.position.Y - b3D.Y;
		vector2.Z = camB.position.Z - b3D.Z;

		/* Calculos para hallar sigma y theta de las ecuaciones parametricas de las rectas */
		A = (vector.X * vector2.X) + (vector.Y * vector2.Y) + (vector.Z * vector2.Z);
		B = (vector.X * vector.X) + (vector.Y * vector.Y) + (vector.Z * vector.Z);
		C = (vector2.X * vector2.X) + (vector2.Y * vector2.Y) + (vector2.Z * vector2.Z);
		D = (vector.X * (b3D.X - a3D.X)) + (vector.Y * (b3D.Y - a3D.Y)) + (vector.Z * (b3D.Z - a3D.Z));
		E = (vector2.X * (b3D.X - a3D.X)) + (vector2.Y * (b3D.Y - a3D.Y)) + (vector2.Z * (b3D.Z - a3D.Z));

		theta = ((A * E) - (D * C)) / ((A * A) - (B * C));
		sigma = ((A * theta) - E) / C;

		/* Calculo de la interseccion de la perpendicular comun con nuestras dos rectas */
		R.X = a3D.X + (vector.X * theta);
		R.Y = a3D.Y + (vector.Y * theta);
		R.Z = a3D.Z + (vector.Z * theta);

		S.X = b3D.X + (vector2.X * sigma);
		S.Y = b3D.Y + (vector2.Y * sigma);
		S.Z = b3D.Z + (vector2.Z * sigma);

		final.X = (R.X + S.X) / 2; 
		final.Y = (R.Y + S.Y) / 2;
		final.Z = (R.Z + S.Z) / 2;
		final.H = 1.;

		triangular_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		//depuracion::print_time (triangular_time1, triangular_time2,"Triangular takes");

		return final;
	}

	/* 
	 * Patch size is 90x90. The size is zoom. Really the size patch is 15x15.
	 *//*
	float Vergency::comparePatchs (colorspaces::Image patchedImgA, colorspaces::Image patchedImgB) {
		float acc = 0.0;
		struct timeval compare_time1, compare_time2;
		gettimeofday (&compare_time1,NULL);

		int salto = PATCH_ROWS/PATCH_SIZE;
	
		for (int k=0; k<PATCH_ROWS; k=k+salto) {
			for (int j=0; j<PATCH_COLUMNS; j=j+salto) {			
				int i = k*PATCH_COLUMNS+j;
				
				acc += abs(int(patchedImgA.data[4*i]-patchedImgB.data[4*i])) + 
					     abs(int(patchedImgA.data[4*i+1]-patchedImgB.data[4*i+1])) + 
					     abs(int(patchedImgA.data[4*i+2]-patchedImgB.data[4*i+2])) ;
			}
		}

		gettimeofday (&compare_time2,NULL);
		//depuracion::print_time (compare_time1, compare_time2,"ComparePatchs takes");

		return acc;
	}
*/
	/*   
	 * \brief return the candidate points that has saliency
	 * \var oneCandidate: If is true, that a point is candidate is enought.
	 *                    If is false, all the points that has saliency, are taken into account
	 */
	int Vergency::isCandidatePoint (HPoint2D p2D, colorspaces::Image image, bool oneCandidate) {
		double candidate_time1, candidate_time2;
		candidate_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;

		const int MINI_PATCH_SIZE = 4;
	
		int nfor=(int)((MINI_PATCH_SIZE-1)/2);
	
		int pixel1 = (SIFNTSC_COLUMNS*(int)p2D.y+(int)p2D.x);
	
		for (int i=-1*nfor; i<=nfor; i++) 
			for (int j=-1*nfor; j<=nfor; j++)
				if (image.data[pixel1+(i*SIFNTSC_COLUMNS+j)]>0.0) {
					if (oneCandidate) {
						this->epipolarCandidatePoints.push_back(p2D);
						return 0;
					}
					HPoint2D myP2D = p2D;
					myP2D.x += j;
					myP2D.y += i;
					this->epipolarCandidatePoints.push_back (myP2D);
				}

		candidate_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		//depuracion::print_time (candidate_time1, candidate_time2,"isCandidatePoint takes");

		return 0;
	}

	void Vergency::calculateEpipolar (HPoint2D saliencyPoint2D_A, TPinHoleCamera camA, TPinHoleCamera camB) {
		double epipolar_time1, epipolar_time2;
		epipolar_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;

		HPoint3D point3D;
		HPoint2D pdraw2D_1, pdraw2D_2, auxLine, intPoint, auxPoint2D, epiLine, gooda, goodb;
		this->pixel2optical(&camA, &saliencyPoint2D_A);
		backproject(&point3D, saliencyPoint2D_A, camA);
	
		HPoint3D pdraw3D = this->getPointInLineNearOfFocus(camA,point3D,-0.5);
		//printf ("(1) 3D: (%2.2f,%2.2f,%2.2f) \n",pdraw3D.X, pdraw3D.Y, pdraw3D.Z);
		HPoint3D pdraw3D_2 = this->getPointInLineNearOfFocus(camA,point3D,-2.);
		//printf ("(2) 3D: (%2.2f,%2.2f,%2.2f) \n",pdraw3D_2.X, pdraw3D_2.Y, pdraw3D_2.Z);
	
		project(pdraw3D, &pdraw2D_1, camB);
		//printf("(1) 2D: (%2.2f,%2.2f,%2.2f) \n",pdraw2D_1.x, pdraw2D_1.y, pdraw2D_1.h);	
		pdraw2D_1.h=1.0;
	
		project(pdraw3D_2, &pdraw2D_2, camB);
		//printf("(2) 2D: (%2.2f,%2.2f,%2.2f) \n",pdraw2D_2.x, pdraw2D_2.y, pdraw2D_2.h);
		pdraw2D_2.h=1.0;

		displayline(pdraw2D_1,pdraw2D_2,&gooda,&goodb,camA);
			optical2pixel (&camA, &gooda);
			optical2pixel (&camA, &goodb);
	
			epiLine = this->getLine2D(gooda, goodb);
		
			this->epipolarPoints.clear (); // reseteamos el vector de puntos epipolar
	
			for (float x=0.0; x<320.0; x++) {
				auxLine.x = x;
				auxLine.y = 240.;
				auxLine.h = 1.0;
		
				intPoint = this->getLine2D (epiLine,auxLine);

				//printf ("intPoint: (%.2f,%.2f,%.2f)\n",intPoint.x,intPoint.y,intPoint.h);
		
				//if (intPoint.h != 0.0)
				//{
				//	pintapixel(imagen_bufB,4*(SIFNTSC_COLUMNS*(int)intPoint.y+(int)intPoint.x),255,255,255);
				//}

				float y = getCoordinateY (epiLine, x);
		
				if (y>0. && y<240.) {	
					auxPoint2D.x = x;
					auxPoint2D.y = y-2.;
					auxPoint2D.h = 1.0; 
					//printf("(epipolarPoints) auxPoint: (%2.2f,%2.2f,%2.2f) \n",auxPoint2D.x, auxPoint2D.y, auxPoint2D.h);	
					this->epipolarPoints.push_back (auxPoint2D);
				}
			}

		epipolar_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		//depuracion::print_time (epipolar_time1, epipolar_time2,"calculateEpipolar takes");
	}

	void Vergency::calculatePatchDavison (colorspaces::Image image, HPoint2D saliencyPoint2D, IplImage *patch_out, 		CvScalar &patch_avg, CvScalar &patch_sdv) {
		double patch_time1, patch_time2;

		patch_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		// 1º Transformamos imagen a escala de grises
		IplImage src = image;
		cvResetImageROI(this->grayImage);
		cvCvtColor (&src, this->grayImage, CV_BGR2GRAY);

		// 2º Extraemos las características del parche
		cvSetImageROI(this->grayImage, cvRect(saliencyPoint2D.x, saliencyPoint2D.y, PATCH_SIZE, PATCH_SIZE));
		cvResize(this->grayImage, this->patch_tmp, CV_INTER_NN);
		cvConvert(this->patch_tmp, this->patch);

		cvAvgSdv(this->patch, &patch_avg, &patch_sdv, NULL);
		cvCopy(this->patch, patch_out);
		patch_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		//depuracion::print_time (patch_time1, patch_time2,"CalculatePatch takes");
	}

	double Vergency::comparePatchsDavison (IplImage* patchA, IplImage* patchB, CvScalar avgA, CvScalar sdvA, CvScalar avgB, CvScalar sdvB) {
		double compare_time1, compare_time2;
		compare_time1 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		double covariance;
		CvScalar avg;

		cvMul(patchA, patchB, this->product);
		avg = cvAvg (this->product, NULL);
		covariance = - avgB.val[0] * avgA.val[0] + avg.val[0];

		/*Avoid division by zero*/
		if(sdvA.val[0] == 0.0) {
			if(sdvB.val[0] == 0.0)
				return 0.0;
			else
				return 1.0;
		} else if(sdvB.val[0] == 0.0)
			return 1.0;	

		compare_time2 = ((double) cvGetTickCount() / ((double)cvGetTickFrequency()))/1000;
		//depuracion::print_time (compare_time1, compare_time2,"ComparePatchs takes");

		return 2.0*(1.0-(1.0/(sdvB.val[0]*sdvA.val[0]))*(covariance));
	}
/*
	void Vergency::calculatePatchs (colorspaces::Image image, HPoint2D saliencyPoint2D, colorspaces::Image& patchedImg) {
		int i,j,k,l, u,v, tpixel, nfor;
		int myPixel = 4*(SIFNTSC_COLUMNS*(int)saliencyPoint2D.y+(int)saliencyPoint2D.x);
		nfor=(int)((PATCH_SIZE-1)/2);
		tpixel=(int)90/PATCH_SIZE;
		struct timeval patch_time1, patch_time2;

		gettimeofday (&patch_time1,NULL);

		int rowlimitLeft = (myPixel/4)%320;
		int rowlimitRight = 320 - rowlimitLeft;		

		//printf ("pixel=%d - limitIzq=%d - limitDecho = %d\n",myPixel,rowlimitLeft,rowlimitRight);
		//printf ("%f, %f, %f\n", saliencyPoint2D.x, saliencyPoint2D.y, saliencyPoint2D.h);

		for (i=-1*nfor; i<=nfor; i++) {
		  for (j=-1*nfor; j<=nfor; j++) {		    
		    k=i+nfor;
		    l=j+nfor;
//printf ("0\n");
		    for (u=0; u<tpixel; u++) {
		      for (v=0; v<tpixel; v++) {
		        double R,G,B;
//printf ("1\n");		        
		        // Limite superior e inferior
		        if ( myPixel+(4*(i*SIFNTSC_COLUMNS+j)) < 0 || myPixel+(4*(i*SIFNTSC_COLUMNS+j)) > SIFNTSC_COLUMNS*SIFNTSC_ROWS*4) {
		        	R = G = B = 0.0;
		        }
		        // limite por la izquierda
		        else if ( j<0 && rowlimitLeft<abs(j) && (myPixel+(4*(i*SIFNTSC_COLUMNS+j)) < ((myPixel-(rowlimitLeft*4))+(4*(i*SIFNTSC_COLUMNS))))) {
		        	R = G = B = 0.0;
		        	//printf ("myPixel=%d - rowlimitLeft=%d -myPixel%320=%d \n",myPixel,rowlimitLeft,(myPixel/4)%320);
		        	//printf ("dato1 = %d - data2 = %d\n",myPixel+(4*(i*SIFNTSC_COLUMNS+j)),(myPixel-(myPixel%320)+(4*(i*SIFNTSC_COLUMNS))));
		        }
		        // limite por la derecha
		        else if ( j>=0 && (myPixel+(4*(i*SIFNTSC_COLUMNS+j)) >= ((myPixel+(rowlimitRight*4))+(4*(i*SIFNTSC_COLUMNS))))) {
		        	R = G = B = 0.0;
		        }
						else {
			        B = image.data[myPixel+(4*(i*SIFNTSC_COLUMNS+j))];
			        G = image.data[myPixel+(4*(i*SIFNTSC_COLUMNS+j))+1];
			        R = image.data[myPixel+(4*(i*SIFNTSC_COLUMNS+j))+2];
							/*
			        if (hsiFilterPatch) {
			        	const HSV* myHSV = RGB2HSV_getHSV ((int)R,(int)G,(int)B);         
			        	hsv2rgb (myHSV->H,myHSV->S, 128.0, &R, &G, &B);
			        }       
			        else if (luminFilterPatch) {
			        	double aux = (R*0.30) + (G*0.59) + (B*0.11);
			        	R = G = B = aux;
			        }**
		        }
//printf ("%i\n", 4*(((tpixel*k+u)*90)+(tpixel*l+v)));
		        patchedImg.data[4*(((tpixel*k+u)*90)+(tpixel*l+v))]  = (unsigned char)B;
		        patchedImg.data[4*(((tpixel*k+u)*90)+(tpixel*l+v))+1]= (unsigned char)G;
		        patchedImg.data[4*(((tpixel*k+u)*90)+(tpixel*l+v))+2]= (unsigned char)R;
		        patchedImg.data[4*(((tpixel*k+u)*90)+(tpixel*l+v))+3]= 0; /* dummy byte **
//printf ("3\n");
		      }
				}
		  }
		}

		gettimeofday (&patch_time2,NULL);
		//depuracion::print_time (patch_time1, patch_time2,"CalculatePatch takes");
	}*/

	/* 
	 * This function calculates the parametric ecuations of the line that compose focus cam and pointLine.
	 * Then, it calculates a point in this line, with "reduccion" parameter.
	 */
	HPoint3D Vergency::getPointInLineNearOfFocus (TPinHoleCamera cam, HPoint3D pointLine, float reduccion) {
		HPoint3D e1,e2;
		/* recta proyeccion */
		e1 = cam.position;
		e2.X = pointLine.X - cam.position.X;
		e2.Y = pointLine.Y - cam.position.Y;
		e2.Z = pointLine.Z - cam.position.Z;
		e2.H = 1.0;
	
		HPoint3D p1_3D;
	
		p1_3D.X = e1.X + ((e2.X/reduccion));
		p1_3D.Y = e1.Y + ((e2.Y/reduccion));
		p1_3D.Z = e1.Z + ((e2.Z/reduccion));
		p1_3D.H = 1.0;
		
		return p1_3D;
	}

	/* In projective geometry the cross product of two points:
	 * A) Gets the line which contains them
	 * B) Gets the point where they intersect themselves */
	HPoint2D Vergency::getLine2D (HPoint2D point1, HPoint2D point2) {
		HPoint2D _line;

		_line.x = point1.y * point2.h - point1.h* point2.y;
		_line.y = -(point1.x * point2.h - point1.h * point2.x);
		_line.h = point1.x * point2.y - point1.y * point2.x;
	
		// normalizamos
		_line.x = _line.x / _line.h;
		_line.y = _line.y / _line.h;
		_line.h = 1.0;

		return _line;
	}

	float Vergency::getCoordinateY (HPoint2D line1, float coordinateX) {
		return (-(line1.x*coordinateX)-line1.h)/line1.y; 
	}

	void Vergency::pixel2optical (TPinHoleCamera * camera, HPoint2D * p)	{
		float aux;
		int height;

		height = camera->rows;

		aux = p->x;
		p->x = height-1.-(p->y);
		p->y = aux;
		p->h = 1.;
	}

	void Vergency::optical2pixel (TPinHoleCamera * camera, HPoint2D * p)	{
		float aux;
		int height;

		height = camera->rows;

		aux = p->y;
		p->y = height-1.-p->x;
		p->x = aux;	
		p->h = 1.;
	}

	const int Vergency::PATCH_SIZE = 11;
	const int Vergency::PATCH_ROWS = 90;
	const int Vergency::PATCH_COLUMNS = 90;
	const int Vergency::DIST_EPIPOLAR = 10;
}

