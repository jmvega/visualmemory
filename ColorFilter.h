/*
*  Copyright (C) 1997-2011 JDERobot Developers Team
 *
 *  This program is free software; you can redistribute it and/or modifdisty
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *   Authors : Eduardo Perdices <eperdices@gsyc.es>,
 *             Jose María Cañas Plaza <jmplaza@gsyc.es>
 *
 */

#ifndef HYBRIDLOC_COLORFILTER_H
#define HYBRIDLOC_COLORFILTER_H

#include <string>
#include <iostream>
#include "ImageInput.h"

#ifndef PI
#define PI 3.141592654
#endif

class ColorFilter : public Singleton<ColorFilter> {
public:
	ColorFilter();
	virtual ~ColorFilter();

	int getColor(const int cam, char *image, int col, int row);
	void getColorRGB(const int cam, char *image, int col, int row, char &r, char &g, char &b, bool filter, int color);
	void getColorHSV(const int cam, char *image, int col, int row, char &h, char &s, char &v);

private:

	void rgb2hsv(unsigned char r, unsigned char g, unsigned char b, float& H, float& S, float& V);
	bool hsvfilter(float H, float S, float V, const float * filter);

	static const unsigned char sliceOrange[3], sliceBlue[3], sliceYellow[3], sliceGreen[3],	sliceWhite[3];
	static const float hsvOrange[6], hsvBlue[6], hsvYellow[6], hsvGreen[6], hsvWhite[6];
};

#endif /*HYBRIDLOC_COLORFILTER_H*/
