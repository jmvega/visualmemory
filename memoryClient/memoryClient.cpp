/*
 *
 *  Copyright (C) 2011 Julio Vega
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see http://www.gnu.org/licenses/. 
 *
 *  Author : Julio Vega <julio.vega@urjc.es>
 *
 */

#include <iostream>
#include <Ice/Ice.h>
#include <IceUtil/IceUtil.h>
#include "memory3Dinterfaz.h"
#include "../structs.h"

int main(int argc, char** argv) {
  int status;
  Ice::CommunicatorPtr ic;

  try {
    ic = Ice::initialize(argc,argv);
    Ice::ObjectPrx base = ic->propertyToProxy("memoryClient.Memory.Proxy");
    if (0==base)
      throw "Could not create proxy";

    // cast to MemoryPrx
    jderobot::MemoryPrx memprx = jderobot::MemoryPrx::checkedCast(base);
    if (0 == memprx)
      throw "Invalid proxy";

		jderobot::MemoryDataPtr memoryData;
		std::vector<Segment3D> myMemory;
		Segment3D tempSegment;

		myMemory.clear ();

		memoryData = memprx->getMemory3D(); // cogemos informacion de la memoria

		for (int k = 0, j = 0; j < memoryData->numSegments; j++, k=j*6) {
			tempSegment.start.X = memoryData->segmentsData[k];
			tempSegment.start.Y = memoryData->segmentsData[k+1];
			tempSegment.start.Z = memoryData->segmentsData[k+2];
			tempSegment.end.X = memoryData->segmentsData[k+3];
			tempSegment.end.Y = memoryData->segmentsData[k+4];
			tempSegment.end.Z = memoryData->segmentsData[k+5];
			//printf ("[%f; %f; %f; %f; %f; %f]\n", tempSegment.start.X,tempSegment.start.Y,tempSegment.start.Z,tempSegment.end.X,tempSegment.end.Y,tempSegment.end.Z);
			myMemory.push_back (tempSegment);
		}

		printf ("My memory has %i segment/s:\n", memoryData->numSegments);
		printf ("==============================\n");
		std::vector<Segment3D>::iterator it;

		for(it = myMemory.begin(); it != myMemory.end(); it++) {
			printf ("[%f; %f; %f; %f; %f; %f]\n", (*it).start.X,(*it).start.Y,(*it).start.Z,(*it).end.X,(*it).end.Y,(*it).end.Z);
		}
  } catch (const Ice::Exception& ex) {
    std::cerr << ex << std::endl;
    status = 1;
  } catch (const char* msg) {
    std::cerr << msg << std::endl;
    status = 1;
  }

  if (ic)
    ic->destroy();
  return status;
}
