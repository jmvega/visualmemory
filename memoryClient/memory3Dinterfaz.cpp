// **********************************************************************
//
// Copyright (c) 2003-2009 ZeroC, Inc. All rights reserved.
//
// This copy of Ice is licensed to you under the terms described in the
// ICE_LICENSE file included in this distribution.
//
// **********************************************************************

// Ice version 3.3.1
// Generated from file `memory3Dinterfaz.ice'

#include "memory3Dinterfaz.h"
#include <Ice/LocalException.h>
#include <Ice/ObjectFactory.h>
#include <Ice/BasicStream.h>
#include <IceUtil/Iterator.h>
#include <IceUtil/ScopedArray.h>

#ifndef ICE_IGNORE_VERSION
#   if ICE_INT_VERSION / 100 != 303
#       error Ice version mismatch!
#   endif
#   if ICE_INT_VERSION % 100 > 50
#       error Beta header file detected
#   endif
#   if ICE_INT_VERSION % 100 < 1
#       error Ice patch level mismatch!
#   endif
#endif

static const ::std::string __jderobot__Memory__getMemory3D_name = "getMemory3D";

::Ice::Object* IceInternal::upCast(::jderobot::MemoryData* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::jderobot::MemoryData* p) { return p; }

::Ice::Object* IceInternal::upCast(::jderobot::Memory* p) { return p; }
::IceProxy::Ice::Object* IceInternal::upCast(::IceProxy::jderobot::Memory* p) { return p; }

void
jderobot::__read(::IceInternal::BasicStream* __is, ::jderobot::MemoryDataPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::jderobot::MemoryData;
        v->__copyFrom(proxy);
    }
}

void
jderobot::__read(::IceInternal::BasicStream* __is, ::jderobot::MemoryPrx& v)
{
    ::Ice::ObjectPrx proxy;
    __is->read(proxy);
    if(!proxy)
    {
        v = 0;
    }
    else
    {
        v = new ::IceProxy::jderobot::Memory;
        v->__copyFrom(proxy);
    }
}

const ::std::string&
IceProxy::jderobot::MemoryData::ice_staticId()
{
    return ::jderobot::MemoryData::ice_staticId();
}

::IceInternal::Handle< ::IceDelegateM::Ice::Object>
IceProxy::jderobot::MemoryData::__createDelegateM()
{
    return ::IceInternal::Handle< ::IceDelegateM::Ice::Object>(new ::IceDelegateM::jderobot::MemoryData);
}

::IceInternal::Handle< ::IceDelegateD::Ice::Object>
IceProxy::jderobot::MemoryData::__createDelegateD()
{
    return ::IceInternal::Handle< ::IceDelegateD::Ice::Object>(new ::IceDelegateD::jderobot::MemoryData);
}

::IceProxy::Ice::Object*
IceProxy::jderobot::MemoryData::__newInstance() const
{
    return new MemoryData;
}

::jderobot::MemoryDataPtr
IceProxy::jderobot::Memory::getMemory3D(const ::Ice::Context* __ctx)
{
    int __cnt = 0;
    while(true)
    {
        ::IceInternal::Handle< ::IceDelegate::Ice::Object> __delBase;
        try
        {
#if defined(__BCPLUSPLUS__) && (__BCPLUSPLUS__ >= 0x0600) // C++Builder 2009 compiler bug
            IceUtil::DummyBCC dummy;
#endif
            __checkTwowayOnly(__jderobot__Memory__getMemory3D_name);
            __delBase = __getDelegate(false);
            ::IceDelegate::jderobot::Memory* __del = dynamic_cast< ::IceDelegate::jderobot::Memory*>(__delBase.get());
            return __del->getMemory3D(__ctx);
        }
        catch(const ::IceInternal::LocalExceptionWrapper& __ex)
        {
            __handleExceptionWrapperRelaxed(__delBase, __ex, 0, __cnt);
        }
        catch(const ::Ice::LocalException& __ex)
        {
            __handleException(__delBase, __ex, 0, __cnt);
        }
    }
}

const ::std::string&
IceProxy::jderobot::Memory::ice_staticId()
{
    return ::jderobot::Memory::ice_staticId();
}

::IceInternal::Handle< ::IceDelegateM::Ice::Object>
IceProxy::jderobot::Memory::__createDelegateM()
{
    return ::IceInternal::Handle< ::IceDelegateM::Ice::Object>(new ::IceDelegateM::jderobot::Memory);
}

::IceInternal::Handle< ::IceDelegateD::Ice::Object>
IceProxy::jderobot::Memory::__createDelegateD()
{
    return ::IceInternal::Handle< ::IceDelegateD::Ice::Object>(new ::IceDelegateD::jderobot::Memory);
}

::IceProxy::Ice::Object*
IceProxy::jderobot::Memory::__newInstance() const
{
    return new Memory;
}

::jderobot::MemoryDataPtr
IceDelegateM::jderobot::Memory::getMemory3D(const ::Ice::Context* __context)
{
    ::IceInternal::Outgoing __og(__handler.get(), __jderobot__Memory__getMemory3D_name, ::Ice::Idempotent, __context);
    bool __ok = __og.invoke();
    ::jderobot::MemoryDataPtr __ret;
    try
    {
        if(!__ok)
        {
            try
            {
                __og.throwUserException();
            }
            catch(const ::Ice::UserException& __ex)
            {
                ::Ice::UnknownUserException __uue(__FILE__, __LINE__, __ex.ice_name());
                throw __uue;
            }
        }
        ::IceInternal::BasicStream* __is = __og.is();
        __is->startReadEncaps();
        __is->read(::jderobot::__patch__MemoryDataPtr, &__ret);
        __is->readPendingObjects();
        __is->endReadEncaps();
        return __ret;
    }
    catch(const ::Ice::LocalException& __ex)
    {
        throw ::IceInternal::LocalExceptionWrapper(__ex, false);
    }
}

::jderobot::MemoryDataPtr
IceDelegateD::jderobot::Memory::getMemory3D(const ::Ice::Context* __context)
{
    class _DirectI : public ::IceInternal::Direct
    {
    public:

        _DirectI(::jderobot::MemoryDataPtr& __result, const ::Ice::Current& __current) : 
            ::IceInternal::Direct(__current),
            _result(__result)
        {
        }
        
        virtual ::Ice::DispatchStatus
        run(::Ice::Object* object)
        {
            ::jderobot::Memory* servant = dynamic_cast< ::jderobot::Memory*>(object);
            if(!servant)
            {
                throw ::Ice::OperationNotExistException(__FILE__, __LINE__, _current.id, _current.facet, _current.operation);
            }
            _result = servant->getMemory3D(_current);
            return ::Ice::DispatchOK;
        }
        
    private:
        
        ::jderobot::MemoryDataPtr& _result;
    };
    
    ::Ice::Current __current;
    __initCurrent(__current, __jderobot__Memory__getMemory3D_name, ::Ice::Idempotent, __context);
    ::jderobot::MemoryDataPtr __result;
    try
    {
        _DirectI __direct(__result, __current);
        try
        {
            __direct.servant()->__collocDispatch(__direct);
        }
        catch(...)
        {
            __direct.destroy();
            throw;
        }
        __direct.destroy();
    }
    catch(const ::Ice::SystemException&)
    {
        throw;
    }
    catch(const ::IceInternal::LocalExceptionWrapper&)
    {
        throw;
    }
    catch(const ::std::exception& __ex)
    {
        ::IceInternal::LocalExceptionWrapper::throwWrapper(__ex);
    }
    catch(...)
    {
        throw ::IceInternal::LocalExceptionWrapper(::Ice::UnknownException(__FILE__, __LINE__, "unknown c++ exception"), false);
    }
    return __result;
}

jderobot::MemoryData::MemoryData(const ::jderobot::seqFloat& __ice_segmentsData, ::Ice::Int __ice_numSegments) :
    segmentsData(__ice_segmentsData),
    numSegments(__ice_numSegments)
{
}

::Ice::ObjectPtr
jderobot::MemoryData::ice_clone() const
{
    ::jderobot::MemoryDataPtr __p = new ::jderobot::MemoryData(*this);
    return __p;
}

static const ::std::string __jderobot__MemoryData_ids[2] =
{
    "::Ice::Object",
    "::jderobot::MemoryData"
};

bool
jderobot::MemoryData::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__jderobot__MemoryData_ids, __jderobot__MemoryData_ids + 2, _s);
}

::std::vector< ::std::string>
jderobot::MemoryData::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__jderobot__MemoryData_ids[0], &__jderobot__MemoryData_ids[2]);
}

const ::std::string&
jderobot::MemoryData::ice_id(const ::Ice::Current&) const
{
    return __jderobot__MemoryData_ids[1];
}

const ::std::string&
jderobot::MemoryData::ice_staticId()
{
    return __jderobot__MemoryData_ids[1];
}

void
jderobot::MemoryData::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    if(segmentsData.size() == 0)
    {
        __os->writeSize(0);
    }
    else
    {
        __os->write(&segmentsData[0], &segmentsData[0] + segmentsData.size());
    }
    __os->write(numSegments);
    __os->endWriteSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__write(__os);
#else
    ::Ice::Object::__write(__os);
#endif
}

void
jderobot::MemoryData::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->read(segmentsData);
    __is->read(numSegments);
    __is->endReadSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__read(__is, true);
#else
    ::Ice::Object::__read(__is, true);
#endif
}

void
jderobot::MemoryData::__write(const ::Ice::OutputStreamPtr&) const
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type jderobot::MemoryData was not generated with stream support";
    throw ex;
}

void
jderobot::MemoryData::__read(const ::Ice::InputStreamPtr&, bool)
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type jderobot::MemoryData was not generated with stream support";
    throw ex;
}

class __F__jderobot__MemoryData : public ::Ice::ObjectFactory
{
public:

    virtual ::Ice::ObjectPtr
    create(const ::std::string& type)
    {
        assert(type == ::jderobot::MemoryData::ice_staticId());
        return new ::jderobot::MemoryData;
    }

    virtual void
    destroy()
    {
    }
};

static ::Ice::ObjectFactoryPtr __F__jderobot__MemoryData_Ptr = new __F__jderobot__MemoryData;

const ::Ice::ObjectFactoryPtr&
jderobot::MemoryData::ice_factory()
{
    return __F__jderobot__MemoryData_Ptr;
}

class __F__jderobot__MemoryData__Init
{
public:

    __F__jderobot__MemoryData__Init()
    {
        ::IceInternal::factoryTable->addObjectFactory(::jderobot::MemoryData::ice_staticId(), ::jderobot::MemoryData::ice_factory());
    }

    ~__F__jderobot__MemoryData__Init()
    {
        ::IceInternal::factoryTable->removeObjectFactory(::jderobot::MemoryData::ice_staticId());
    }
};

static __F__jderobot__MemoryData__Init __F__jderobot__MemoryData__i;

#ifdef __APPLE__
extern "C" { void __F__jderobot__MemoryData__initializer() {} }
#endif

void 
jderobot::__patch__MemoryDataPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::jderobot::MemoryDataPtr* p = static_cast< ::jderobot::MemoryDataPtr*>(__addr);
    assert(p);
    *p = ::jderobot::MemoryDataPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::jderobot::MemoryData::ice_staticId(), v->ice_id());
    }
}

bool
jderobot::operator==(const ::jderobot::MemoryData& l, const ::jderobot::MemoryData& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
jderobot::operator<(const ::jderobot::MemoryData& l, const ::jderobot::MemoryData& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}

::Ice::ObjectPtr
jderobot::Memory::ice_clone() const
{
    throw ::Ice::CloneNotImplementedException(__FILE__, __LINE__);
    return 0; // to avoid a warning with some compilers
}

static const ::std::string __jderobot__Memory_ids[2] =
{
    "::Ice::Object",
    "::jderobot::Memory"
};

bool
jderobot::Memory::ice_isA(const ::std::string& _s, const ::Ice::Current&) const
{
    return ::std::binary_search(__jderobot__Memory_ids, __jderobot__Memory_ids + 2, _s);
}

::std::vector< ::std::string>
jderobot::Memory::ice_ids(const ::Ice::Current&) const
{
    return ::std::vector< ::std::string>(&__jderobot__Memory_ids[0], &__jderobot__Memory_ids[2]);
}

const ::std::string&
jderobot::Memory::ice_id(const ::Ice::Current&) const
{
    return __jderobot__Memory_ids[1];
}

const ::std::string&
jderobot::Memory::ice_staticId()
{
    return __jderobot__Memory_ids[1];
}

::Ice::DispatchStatus
jderobot::Memory::___getMemory3D(::IceInternal::Incoming& __inS, const ::Ice::Current& __current)
{
    __checkMode(::Ice::Idempotent, __current.mode);
    __inS.is()->skipEmptyEncaps();
    ::IceInternal::BasicStream* __os = __inS.os();
    ::jderobot::MemoryDataPtr __ret = getMemory3D(__current);
    __os->write(::Ice::ObjectPtr(::IceInternal::upCast(__ret.get())));
    __os->writePendingObjects();
    return ::Ice::DispatchOK;
}

static ::std::string __jderobot__Memory_all[] =
{
    "getMemory3D",
    "ice_id",
    "ice_ids",
    "ice_isA",
    "ice_ping"
};

::Ice::DispatchStatus
jderobot::Memory::__dispatch(::IceInternal::Incoming& in, const ::Ice::Current& current)
{
    ::std::pair< ::std::string*, ::std::string*> r = ::std::equal_range(__jderobot__Memory_all, __jderobot__Memory_all + 5, current.operation);
    if(r.first == r.second)
    {
        throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
    }

    switch(r.first - __jderobot__Memory_all)
    {
        case 0:
        {
            return ___getMemory3D(in, current);
        }
        case 1:
        {
            return ___ice_id(in, current);
        }
        case 2:
        {
            return ___ice_ids(in, current);
        }
        case 3:
        {
            return ___ice_isA(in, current);
        }
        case 4:
        {
            return ___ice_ping(in, current);
        }
    }

    assert(false);
    throw ::Ice::OperationNotExistException(__FILE__, __LINE__, current.id, current.facet, current.operation);
}

void
jderobot::Memory::__write(::IceInternal::BasicStream* __os) const
{
    __os->writeTypeId(ice_staticId());
    __os->startWriteSlice();
    __os->endWriteSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__write(__os);
#else
    ::Ice::Object::__write(__os);
#endif
}

void
jderobot::Memory::__read(::IceInternal::BasicStream* __is, bool __rid)
{
    if(__rid)
    {
        ::std::string myId;
        __is->readTypeId(myId);
    }
    __is->startReadSlice();
    __is->endReadSlice();
#if defined(_MSC_VER) && (_MSC_VER < 1300) // VC++ 6 compiler bug
    Object::__read(__is, true);
#else
    ::Ice::Object::__read(__is, true);
#endif
}

void
jderobot::Memory::__write(const ::Ice::OutputStreamPtr&) const
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type jderobot::Memory was not generated with stream support";
    throw ex;
}

void
jderobot::Memory::__read(const ::Ice::InputStreamPtr&, bool)
{
    Ice::MarshalException ex(__FILE__, __LINE__);
    ex.reason = "type jderobot::Memory was not generated with stream support";
    throw ex;
}

void 
jderobot::__patch__MemoryPtr(void* __addr, ::Ice::ObjectPtr& v)
{
    ::jderobot::MemoryPtr* p = static_cast< ::jderobot::MemoryPtr*>(__addr);
    assert(p);
    *p = ::jderobot::MemoryPtr::dynamicCast(v);
    if(v && !*p)
    {
        IceInternal::Ex::throwUOE(::jderobot::Memory::ice_staticId(), v->ice_id());
    }
}

bool
jderobot::operator==(const ::jderobot::Memory& l, const ::jderobot::Memory& r)
{
    return static_cast<const ::Ice::Object&>(l) == static_cast<const ::Ice::Object&>(r);
}

bool
jderobot::operator<(const ::jderobot::Memory& l, const ::jderobot::Memory& r)
{
    return static_cast<const ::Ice::Object&>(l) < static_cast<const ::Ice::Object&>(r);
}
