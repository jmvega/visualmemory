/*
 *  Copyright (C) 1997-2011 JDERobot Developers Team
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Authors : Julio Vega <julio.vega@urjc.es>,
 *
 */

#ifndef VISUALMEMORY_CONTROLLER_H
#define VISUALMEMORY_CONTROLLER_H

#include <string>
#include <iostream>
#include <colorspaces/colorspacesmm.h>
#include <jderobot/camera.h>
#include <jderobot/image.h>
#include <jderobot/motors.h>
#include <jderobot/ptmotors.h>
#include <jderobot/encoders.h>
#include <jderobot/ptencoders.h>
#include <jderobot/pose3dmotors.h>
#include <jderobot/pose3dencoders.h>
#include <jderobot/jointmotor.h>
#include <pthread.h>
#include "cameraConf.h"
#include "camera.h"
#include "pioneer.h"
#include "drawarea.h"
#include "structs.h"
#include "cornerDetection.h"
#include <gsl/gsl_blas.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_eigen.h>
#include "AbstractRobot.h"

namespace visualMemory {
  class Controller {
		public:
			Controller(RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1);
			Controller(RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2);
			Controller(jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1);
			Controller(jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2);
			Controller (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1);
			Controller (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1, jderobot::CameraPrx cprx2, jderobot::Pose3DMotorsPrx ptmprx2, jderobot::Pose3DEncodersPrx pteprx2);

		  virtual ~Controller();

			// ESTRUCTURAS: serán rellenadas por los distintos algoritmos y posiblemente dibujadas por el view
			std::vector<Segment3D> segmentMemory;
			std::vector<HPoint3D> pointMemory;
			std::vector<Parallelogram3D> parallelograms;

			// vectores de segmentos en 2D:
			std::vector<Segment2D> solisSegments1; // cam A
			std::vector<Segment2D> solisSegments2; // cam B
			std::vector<Segment2D> predictedSegments1; // cam A
			std::vector<Segment2D> predictedSegments2; // cam B
			std::vector<Segment2D> newSegments1; // cam A
			std::vector<Segment2D> newSegments2; // cam B
			std::vector<Segment2D> refutedSegments1; // cam A
			std::vector<Segment2D> refutedSegments2; // cam B
			std::vector<Segment2D> fittedSegments1; // cam A
			std::vector<Segment2D> fittedSegments2; // cam B

			// vectores de segmentos en 3D, para pintado en OpenGL:
			std::vector<Segment3D> solisSegments1_3D; // cam A
			std::vector<Segment3D> solisSegments2_3D; // cam B
			std::vector<Segment3D> predictedSegments1_3D; // cam A
			std::vector<Segment3D> predictedSegments2_3D; // cam B
			std::vector<Segment3D> newSegments1_3D; // cam A
			std::vector<Segment3D> newSegments2_3D; // cam B
			std::vector<Segment3D> refutedSegments1_3D; // cam A
			std::vector<Segment3D> refutedSegments2_3D; // cam B
			std::vector<Segment3D> fittedSegments1_3D; // cam A
			std::vector<Segment3D> fittedSegments2_3D; // cam B

			// para tratamiento de paralelogramos
			std::vector<Parallelogram2D> predictedParallelograms1;
			std::vector<Parallelogram2D> newParallelograms1;
			std::vector<Parallelogram2D> refutedParallelograms1;
			std::vector<Parallelogram2D> fittedParallelograms1;
			std::vector<Parallelogram3D> predictedParallelograms1_3D;
			std::vector<Parallelogram3D> newParallelograms1_3D;
			std::vector<Parallelogram3D> refutedParallelograms1_3D;
			std::vector<Parallelogram3D> fittedParallelograms1_3D;

			HPoint2D clickedPixelA, clickedPixelB;
			HPoint3D clickedPixelA_3D, clickedPixelB_3D;
			HPoint3D puntoExtremo1, puntoExtremo2, puntoExtremo3, puntoExtremo4, puntoExtremo5, puntoExtremo6, puntoExtremo7, puntoExtremo8;
			HPoint3D puntoCruz1, puntoCruz2, puntoCruz3, puntoCruz4;
			HPoint3D robotGoal;
			HPoint3D vergency3Dpoint;

			AbstractRobot *robotPlatform;

			colorspaces::Image *image1;
			colorspaces::Image *image2;

			int getNumCamerasUsed ();
			void putNumCamerasUsed (int numCameras);
			std::string getTypeOfRobot ();
			void putTypeOfRobot (std::string typeOfRobot);

			//Progeo cameras
			TPinHoleCamera myCamA, myCamB;

		  std::string getGladePath();
			void iteration ();
			void initMutex ();
			void initSensors (RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1);
			void initSensors (RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2);
			void initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1);
			void initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, RoboCompJointMotor::JointMotorPrx jprx, jderobot::CameraPrx cprx1, jderobot::CameraPrx cprx2);
			void initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1);
			void initSensors (jderobot::MotorsPrx mprx, jderobot::EncodersPrx eprx, jderobot::CameraPrx cprx1, jderobot::Pose3DMotorsPrx ptmprx1, jderobot::Pose3DEncodersPrx pteprx1, jderobot::CameraPrx cprx2, jderobot::Pose3DMotorsPrx ptmprx2, jderobot::Pose3DEncodersPrx pteprx2);

			void initialization ();
			void initVariables ();
			void initProgeoCameras();
			void updateSensors1 ();
			void updateSensors2 ();
			void updatePioneerSensors ();
			void updateCamerasPos ();
			void printCameraInformation (TPinHoleCamera* actualCamera);
			void printJointMotorParams ();

			// FUNCIONES 'GET'
			void getCameraData1(unsigned char **image1);
			void getCameraData2(unsigned char **image2);
			void getPosition (CvPoint3D32f* myPoint);
			void getPoseCam1 (CvPoint3D32f* myPoint);
			void getPoseCam2 (CvPoint3D32f* myPoint);
			void getPTEncoders1();
			void getPTEncoders2();

			// FUNCIONES 'SET'
			void stopMotors ();
			void goLeft ();
			void goRight ();
			void goUp ();
			void goDown ();
			int checkAndSetGiraffe (float pan, float tilt);
			void setPT1 (float latitude, float longitude);
			void setPT2 (float latitude, float longitude);
			void setGiraffePosition (RoboCompJointMotor::MotorGoalPositionList list);
			void setSolis (bool value);
			bool getSolis ();
			void setPredictions (bool value);
			bool getPredictions ();
			void setRefuted (bool value);
			bool getRefuted ();
			void setFitted (bool value);
			bool getFitted ();
			void setNews (bool value);
			bool getNews ();
			void setSolis2 (bool value);
			bool getSolis2 ();
			void setPredictions2 (bool value);
			bool getPredictions2 ();
			void setRefuted2 (bool value);
			bool getRefuted2 ();
			void setFitted2 (bool value);
			bool getFitted2 ();
			void setNews2 (bool value);
			bool getNews2 ();
			void setManualVergency (bool value);
			bool getManualVergency ();
			void setImagenCombo (int value);
			int getImagenCombo ();
			void setMemoryCombo (int value);
			int getMemoryCombo ();
			void setSaliency (bool value);
			bool getSaliency ();
			void setIteration (bool value);
			bool getIteration ();
			void setPlusIteration (bool value);
			bool getPlusIteration ();
			bool giraffeIsMoving ();

			/* v: velocidad lineal (mm./s.) a comandar al robot */
			void setV (float v);

			/* w: velocidad rotacional (deg./s.) a comandar al robot */
			void setW (float w);

			void setPan1 (float value);
			void setPan2 (float value);
			void setTilt1 (float value);
			void setTilt2 (float value);

			float getPan1 ();
			float getPan2 ();
			float getTilt1 ();
			float getTilt2 ();

			static const float V_MOTOR;
			static const float W_MOTOR;

			jderobot::EncodersDataPtr ed;
			jderobot::ImageDataPtr data1;
			jderobot::ImageDataPtr data2;
			jderobot::Pose3DEncodersDataPtr pted1;
			jderobot::Pose3DEncodersDataPtr pted2;

			pthread_mutex_t cameraMutex;
			pthread_mutex_t encodersMutex;
			pthread_mutex_t ptEncodersMutex;
			pthread_mutex_t solisSegmentsMutex1;
			pthread_mutex_t predictionsMutex1;
			pthread_mutex_t fittedMutex1;
			pthread_mutex_t refutedMutex1;
			pthread_mutex_t newsMutex1;

		private:
			std::string gladepath;
			jderobot::MotorsPrx mprx;
			jderobot::EncodersPrx eprx;
			jderobot::CameraPrx cprx1;
			jderobot::CameraPrx cprx2;
			jderobot::Pose3DMotorsPrx ptmprx1;
			jderobot::Pose3DEncodersPrx pteprx1;
			jderobot::Pose3DMotorsPrx ptmprx2;
			jderobot::Pose3DEncodersPrx pteprx2;
			RoboCompJointMotor::JointMotorPrx jprx;
			RoboCompJointMotor::MotorParamsList motorsparams;
			RoboCompJointMotor::MotorStateMap motorsstate;

			int numCamerasUsed;
			std::string typeOfRobot;
			int typeOfPioneerLoaded;

			float pan1;
			float tilt1;
			float pan2;
			float tilt2;

			bool showSolis;
			bool showPredictions;
			bool showRefuted;
			bool showFitted;
			bool showNews;
			bool showSolis2;
			bool showPredictions2;
			bool showRefuted2;
			bool showFitted2;
			bool showNews2;
			bool manualVergency;
			int imagenComboNumber;
			int memoryComboNumber;
			bool showInstant;
			bool showSaliency;
			bool showIteration;
			bool plusIteration;

			// FUNCIONES 'SET' REFRESCO DE ESTRUCTURAS INTERNAS
			void setEncoders();
			void setCameraData1();
			void setCameraData2();
			void setPTEncoders1();
			void setPTEncoders2();
  };
} // namespace

#endif /*VISUALMEMORY_CONTROLLER_H*/
