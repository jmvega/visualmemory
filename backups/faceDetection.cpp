/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "faceDetection.h"

namespace visualMemory {

	const float faceDetection::SCALE_FACTOR = 1.1;
	const int faceDetection::MIN_NEIGHBORS = 2;
	const int faceDetection::OPERATION_MODE = CV_HAAR_DO_CANNY_PRUNING;
	const int faceDetection::MIN_WINDOW_HEIGHT = 20;
	const int faceDetection::MIN_WINDOW_WIDTH = 20;

	faceDetection::faceDetection (char* cascade_name) {
		this->cascade = (CvHaarClassifierCascade*) cvLoad (cascade_name, 0, 0, 0);
		this->storage = cvCreateMemStorage(0);

		if (!cascade) {
			printf("ERROR: faceDetection: Could not load classifier cascade %s\n", cascade_name);
		}
	}

	int faceDetection::getFaces (char* mycolorA, int widthImage, int heightImage, CvSeq *faces) {
		IplImage *imgLocal = NULL;
		CvMat img;

		img = cvMat (heightImage, widthImage, CV_8UC3, mycolorA);
		imgLocal = cvCreateImage (cvSize(widthImage, heightImage), IPL_DEPTH_8U, 1);

		if (imgLocal != NULL) {
			cvCvtColor (&img, imgLocal, CV_BGR2GRAY);
			cvClearMemStorage (this->storage);
			if (this->cascade && imgLocal)
			  faces = cvHaarDetectObjects (imgLocal, this->cascade, this->storage, SCALE_FACTOR, MIN_NEIGHBORS, OPERATION_MODE, cvSize (MIN_WINDOW_WIDTH, MIN_WINDOW_HEIGHT));
			else
				return (-1);

			cvReleaseImage (&imgLocal);
			return (1);
		} else
			return (-1);
	}

	faceDetection::~faceDetection() {	}
}

