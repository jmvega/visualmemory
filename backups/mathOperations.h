/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#ifndef VISIONLIBRARY_MATHOPERATIONS_H
#define VISIONLIBRARY_MATHOPERATIONS_H

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "structs.h"
#include "depuracion.h"

namespace visionLibrary {
  class geometry {
		private:
			static const float MAX_PAR;
			static const float MAX_DIST;
			static const float MAX_PAR_2D;
			static const float MAX_DIST_2D;

		public:
			geometry ();
		  virtual ~geometry ();

			static double meanVector (double* v, int vSize);
			static void meanUnitVector (double* v, int vSize);
			static int multiplyFFT (const CvArr* srcAarr, const CvArr* srcBarr, CvArr* dstarr);
			static void linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D &intersectionPoint);
			static float distanceBetweenPoints (HPoint3D p1, HPoint3D p2);
			static void getMaximizedSegment (HPoint3D *seg1Start, HPoint3D *seg1End, HPoint3D *seg2Start, HPoint3D *seg2End, HPoint3D *startPoint, HPoint3D *endPoint);
			static int overlapping (Segment3D segment, HPoint3D proy1, HPoint3D proy2, int ubi1, int ubi2, float dist1, float dist2, bool isIn2D);
			static float segmentMagnitude (Segment3D segment);
			static int distancePointLine (HPoint3D Point, Segment3D segment, HPoint3D *Intersection, float *Distance);
			static int areTheSameSegment (Segment3D s1, Segment3D s2);
			static int mergeSegment (std::vector<Segment3D> segmentCache, Segment3D segment, bool* areTheSame);
			static int areTheSameParallelogram (Parallelogram3D par1, Parallelogram3D par2);
			static void getPolygonCentroid (Parallelogram3D* parallelogram);
	};
}

#endif
