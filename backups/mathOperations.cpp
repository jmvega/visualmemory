 /*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "geometry.h"

namespace visionLibrary {

	const float geometry::MAX_PAR = 25;
	const float geometry::MAX_DIST = 25;
	const float geometry::MAX_PAR_2D = 15;
	const float geometry::MAX_DIST_2D = 15;

	geometry::geometry () {}

	double geometry::meanVector (double* v, int vSize) {
		int i;   
		double mean=0.0;
		double* ptr = v;

		for (i=0;i<vSize;i++) {
			mean+= *ptr;
			ptr++;
		}
		mean /=(double)vSize;
		return mean;
	}

	void geometry::meanUnitVector (double* v, int vSize) {
		double sqsum = 0.0;
		double mean = meanVector(v,vSize);
		double* ptr = v;
		int i;

		for(i=0;i<vSize;i++) {
			(*ptr) -= mean; 
			sqsum += (*ptr)*(*ptr);
			ptr++;
		}

		double a = 1.0f/(double)(sqrt(sqsum));
		ptr = v;

		for(i=0;i<vSize;i++) {
			(*ptr) *= a;
			ptr++;
		}    
	}

	int geometry::multiplyFFT (const CvArr* srcAarr, const CvArr* srcBarr, CvArr* dstarr) {
		CvMat *srcA = (CvMat*)srcAarr;
		CvMat *srcB = (CvMat*)srcBarr;
		CvMat *dst = (CvMat*)dstarr;

		int i,j, rows, cols;
		rows = srcA->rows;
		cols = srcA->cols;
		double c_re,c_im;

		for( i=0; i<rows; i++ )	{
			for( j = 0; j < cols; j ++ ) {
				c_re = ((double*)(srcA->data.ptr + srcA->step*i))[j*2]*((double*)(srcB->data.ptr + srcB->step*i))[j*2] -
				((double*)(srcA->data.ptr + srcA->step*i))[j*2+1]*((double*)(srcB->data.ptr + srcB->step*i))[j*2+1];
				c_im = ((double*)(srcA->data.ptr + srcA->step*i))[j*2]*((double*)(srcB->data.ptr + srcB->step*i))[j*2+1] +
				((double*)(srcA->data.ptr + srcA->step*i))[j*2+1]*((double*)(srcB->data.ptr + srcB->step*i))[j*2];
				((double*)(dst->data.ptr + dst->step*i))[j*2]	= c_re;
				((double*)(dst->data.ptr + dst->step*i))[j*2+1]	= c_im;
			}
		}

		return 1;
	}

	/* We want to know the intersection between a line given by two points (A and B) on 3D, and a plan (the ground, for example). Then, we'll get another point; it'll be our solution */
	void geometry::linePlaneIntersection (HPoint3D A, HPoint3D B, HPoint3D &intersectionPoint) {
		HPoint3D v;	// Line director vector: it the same to take A or B as origin or destination extrem...
		float t;

		A.X = A.X;
		A.Y = A.Y;
		A.Z = A.Z;

		B.X = B.X;
		B.Y = B.Y;
		B.Z = B.Z;

		v.X = (B.X - A.X);
		v.Y = (B.Y - A.Y);
		v.Z = (B.Z - A.Z);

		// We'll calculate the groun intersection (Z = 0) on our robot system. Parametric equations:
		intersectionPoint.Z = 0.; // intersectionPoint.Z = A.Z + t*v.Z => t = (-A.Z / v.Z)
		t = (-A.Z) / (v.Z);

		intersectionPoint.X = A.X + (t*v.X);
		intersectionPoint.Y = A.Y + (t*v.Y);
		intersectionPoint.H = 1.;
	}

	float geometry::distanceBetweenPoints (HPoint3D p1, HPoint3D p2) {
		float dist;

		dist = abs(sqrt(pow(p2.X-p1.X,2) + pow(p2.Y-p1.Y,2) + pow(p2.Z-p1.Z,2)));
		return dist;
	}

	void geometry::getMaximizedSegment (HPoint3D *seg1Start, HPoint3D *seg1End, HPoint3D *seg2Start, HPoint3D *seg2End, HPoint3D *startPoint, HPoint3D *endPoint) {

		HPoint3D* myArray[4] = {seg1Start, seg1End, seg2Start, seg2End};
		float max = 0;
		int i, j;
		float value;

		for (i = 0; i < 4; i ++) {
			for (j = i+1; j < 4; j ++) {
				value = abs(distanceBetweenPoints (*myArray[i], *myArray[j]));
				if (value > max) {
					max = value;
					*startPoint = *myArray[i];
					*endPoint = *myArray[j];
				}
			}
		}
		// Finally, we'll get two points in order to maximize distance between them
	}

	int geometry::overlapping (Segment3D segment, HPoint3D proy1, HPoint3D proy2, int ubi1, int ubi2, float dist1, float dist2, int isIn2D) {
		int found = 1;
		float d1, d2;
		HPoint3D startPoint, endPoint;
		float maxPar, maxDist;

		if (isIn2D) { // puede que vengamos de una comparación de segmentos 2D, en cuyo caso los límites de paralelismo y distancia varían
			maxPar = MAX_PAR_2D;
			maxDist = MAX_DIST_2D;
		} else {
			maxPar = MAX_PAR;
			maxDist = MAX_DIST;
		}

		//printf ("overlapping: ubi1 = %i, ubi2 = %i, dist1 = %0.2f, dist2 = %0.2f\n", ubi1, ubi2, dist1, dist2);
		if ((abs(dist1)<maxPar) && (abs(dist2)<maxPar)) { // They're parallel segments. Now we check if they're in distance
			// Posible cases: 1-Two intermediate points; 2-One intermediate point; 3-Any intermediate point
			getMaximizedSegment (&segment.start, &segment.end, &proy1, &proy2, &startPoint, &endPoint);

			if ((ubi1==0)&&(ubi2==0)) { // 1-Two intermediate points. We don't do anything
				//printf ("found!\n");
				found = 1;
			} else { // 2-One intermediate point
				if ((ubi1==0)||(ubi2==0)) {
					if ((ubi1==0)&&(ubi2==1)) {
						segment.end = proy2;
					} else {
						if ((ubi1==0)&&(ubi2==-1)) {
							segment.start = proy2; 
						} else {
							if ((ubi1==1)&&(ubi2==0)) {
								segment.end = proy1; 
							} else {
								if ((ubi1==-1)&&(ubi2==0)) {
									segment.start = proy1; 
								}
							}
						}
					}
				} else { // 3-Any intermediate point
					if ((ubi1==-1)&&(ubi2==1)) { // One on the right side and the other one on the left side
						segment.start = proy1;
						segment.end = proy2;
					} else {
						if ((ubi1==1)&&(ubi2==-1)) {
							segment.start = proy2;
							segment.end = proy1;
						} else {
							if (ubi1==1) { // segment is on the right side
								d1 = distanceBetweenPoints(proy1, segment.end);
								d2 = distanceBetweenPoints(proy2, segment.end);

								if ((abs(d1)<maxDist)||(abs(d2)<maxDist)) {
									if (d1<d2) {
										segment.end = proy2; 
									} else {
										segment.end = proy1;
									}
								} else { // They don't fusion themselves
									found = 0;
								}
							} else {    
								d1 = distanceBetweenPoints(proy1, segment.start);
								d2 = distanceBetweenPoints(proy2, segment.start); 

								if ((abs(d1)<maxDist)||(abs(d2)<maxDist)) {
									if (d1<d2) {
										segment.start = proy2; 
									} else {
										segment.start = proy1;
									}
								} else { // They don't fusion themselves
									found = 0;
								}
							} 
						}
					} 
				}
			}
			segment.start = startPoint;
			segment.end = endPoint;
		} else { // They aren't parallel lines
			found = 0;
		}

		return found;
	}

	float geometry::segmentMagnitude (Segment3D segment) {
		HPoint3D vector;

		vector.X = segment.start.X - segment.end.X;
		vector.Y = segment.start.Y - segment.end.Y;
		vector.Z = segment.start.Z - segment.end.Z;
		vector.H = 1.;

		return (float) (sqrt(vector.X * vector.X + vector.Y * vector.Y + vector.Z * vector.Z));
	}

	int geometry::distancePointLine (HPoint3D Point, Segment3D segment, HPoint3D *Intersection, float *Distance) {
		float LineMag;
		float U;
		int res;

		LineMag = segmentMagnitude (segment);

		U = ( ( ( Point.X - segment.start.X ) * ( segment.end.X - segment.start.X ) ) +
				  ( ( Point.Y - segment.start.Y ) * ( segment.end.Y - segment.start.Y ) ) +
				  ( ( Point.Z - segment.start.Z ) * ( segment.end.Z - segment.start.Z ) ) ) /
					( LineMag * LineMag );

		Intersection->position.X = segment.start.X + U * ( segment.end.X - segment.start.X );
		Intersection->position.Y = segment.start.Y + U * ( segment.end.Y - segment.start.Y );
		Intersection->position.Z = segment.start.Z + U * ( segment.end.Z - segment.start.Z );

		if( U >= 0.0f || U <= 1.0f ) {
			res = 0;
		} else {
			if (U < 0.) { // Intersection will be after segment
				res = -1;
			} else { // Intersection will be before segment
				res = +1;
			}
		}

		*Distance = distanceBetweenPoints(Point, Intersection->position);
		return res;
	}

	int geometry::areTheSameSegment (Segment3D s1, Segment3D s2) {
		int areTheSame = 0;
		if ((((float)s1.start.X==(float)s2.start.X) && 
				 ((float)s1.start.Y==(float)s2.start.Y) && 
				 ((float)s1.start.Z==(float)s2.start.Z) && 
				 ((float)s1.end.X==(float)s2.end.X) &&
				 ((float)s1.end.Y==(float)s2.end.Y) && 
				 ((float)s1.end.Z==(float)s2.end.Z))	|| 
				(((float)s1.start.X==(float)s2.end.X) && 
				 ((float)s1.start.Y==(float)s2.end.Y) && 
				 ((float)s1.start.Z==(float)s2.end.Z) && 
				 ((float)s1.end.X==(float)s2.start.X) &&
				 ((float)s1.end.Y==(float)s2.start.Y) && 
				 ((float)s1.end.Z==(float)s2.start.Z))) { // they're the same segment
			areTheSame = 1;
		}

		return (areTheSame);
	}

	int geometry::mergeSegment (struct CacheList *cache, Segment3D segment, int* areTheSame) {
		int found=0;
		HPoint3D proy1, proy2;
		int ubi1, ubi2;
		float dist1, dist2;
		struct CacheList *p;

		p = cache;
		(*areTheSame) = 0; // suponemos que no es el mismo con el que se fusionará

		while ((p != NULL) && (!found))	{
			if (p->segment->isValid == 1) {
				if (areTheSameSegment (*(p->segment), segment)) {
					found = 1;
					(*areTheSame) = 1;
				} else {
					// Check every segment in memory, with segment. Calculate intersection point between segment i and segment.start perpendicular
					ubi1 = distancePointLine (segment.start, *(p->segment), &proy1, &dist1);
					ubi2 = distancePointLine (segment.end, *(p->segment), &proy2, &dist2);

					found = overlapping (*(p->segment), proy1, proy2, ubi1, ubi2, dist1, dist2, 0); // If they're parallel lines -> we finish checking

					//printf ("mergeSegment s1=[%0.2f, %0.2f, %0.2f]-[%0.2f, %0.2f, %0.2f], s2=[%0.2f, %0.2f, %0.2f]-[%0.2f, %0.2f, %0.2f], overlapped = %i\n", p->segment->start.X, p->segment->start.Y, p->segment->start.Z, p->segment->end.X, p->segment->end.Y, p->segment->end.Z, segment.start.X, segment.start.Y, segment.start.Z, segment.end.X, segment.end.Y, segment.end.Z, found);
				}
				if (found) { // si hemos logrado encontrarlo, le hacemos merge de modo que reseteamos el tiempo y travel
					p->segment->timestamp = segment.timestamp;
					p->segment->traveled = segment.traveled;
				}
			}
			p = p->next;
		}

		return found;
	}

	int geometry::areTheSameParallelogram (Parallelogram3D par1, Parallelogram3D par2) {
		HPoint3D* myArray1[4] = {&(par1.p1), &(par1.p2), &(par1.p3), &(par1.p4)};
		HPoint3D* myArray2[4] = {&(par2.p1), &(par2.p2), &(par2.p3), &(par2.p4)};
		int i, j, found;
		float value;
		int equalPointsCounter = 0;

		//printParallelogram (par1);
		//printParallelogram (par2);

		for (i = 0; i < 4; i ++) {
			j = 0;
			found = FALSE;
			while ((j < 4) && (!found)) {
				value = abs(distanceBetweenPoints (*myArray1[i], *myArray2[j]));
				if (value < 20.) {
					equalPointsCounter ++;
					found = TRUE;
				}
				j ++;
			}
		}

		return (equalPointsCounter == 4);
	}

	void geometry::getPolygonCentroid (Parallelogram3D* parallelogram) {
		HPoint3D vertexList[5]; // Remember: Counter Clockwise Winding in order to get the centroid
		int i, j;
		double sum = 0.;
		double area = 0.;
		HPoint3D centroid;

		vertexList[0] = parallelogram->p2;
		vertexList[1] = parallelogram->p1;
		vertexList[2] = parallelogram->p3;
		vertexList[3] = parallelogram->p4;
		vertexList[4] = vertexList[0];

		centroid.X = 0.;
		centroid.Y = 0.;
		centroid.Z = 0.;
		centroid.H = 1.;

		for (i = 0; i < 5; i++) {
			j = (i+1)%5;
		  area = vertexList[i].X * vertexList[j].Y - vertexList[i].Y * vertexList[j].X;
		  sum += area;
		  centroid.X += (vertexList[i].X + vertexList[j].X) * area;
		  centroid.Y += (vertexList[i].Y + vertexList[j].Y) * area;
		}
		sum *= 3.;
		centroid.X /= sum;
		centroid.Y /= sum;

		parallelogram->centroid = centroid;
		if (depuracion::DEBUG) printf ("Centroid = [%0.2f, %0.2f, %0.2f]\n", centroid.X, centroid.Y, centroid.Z);
	}

	geometry::~geometry () {}
}

