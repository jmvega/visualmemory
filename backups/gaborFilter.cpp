/*
 *  Copyright (C) 2010 Julio Vega Pérez
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Library General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * 	Author : Julio Vega Pérez (julio [dot] vega [at] urjc [dot] es)
 *
 *  This library was programed for RobotVision Project http://jde.gsyc.es/index.php/robotvision
 *
 */

#include "gaborFilter.h"

namespace visualMemory {

	const int gaborFilter::IMAGE_SIZE = 128;
	const int gaborFilter::DSCALE = 8;
	const int gaborFilter::NUM_SCALES = 5;
	const int gaborFilter::NUM_ORIENTATIONS = 8;
	const int gaborFilter::DEBUG_GABOR = 1;

	gaborFilter::gaborFilter () {}

	CvMat** gaborFilter::loadGaborFFT(char* folderName) {
		int i,j;
		char filename[512];
		CvMat** mGabor=(CvMat**)malloc(40*sizeof(CvMat));

		for(i=0;i<5;i++) {
			for(j=0;j<8;j++) {
				sprintf(filename,"%s/FFT%s_%d_%d.data",folderName,"gaborBank",i,j);
				FILE *ifp = fopen(filename, "rb");
				if (ifp == NULL) {
					printf ("Can't load file %s\n", filename);
					exit (-1);
				}
				mGabor[i*8+j]	= fileOperations::getMatrix(ifp,false);
				fclose(ifp);
			}
		}
		return(mGabor);
	}

	void gaborFilter::unloadGaborFFT(CvMat** mGabor) {				
		int i,j;

		for(i=0;i<5;i++)
			for(j=0;j<8;j++)	 
				cvReleaseMat(&mGabor[i*8+j]); 

		cvReleaseMat(mGabor);
	}

	int gaborFilter::extractGabor (IplImage* img,double* object,CvMat** mGabor) {
		int w,h;
		w=IMAGE_SIZE;h=IMAGE_SIZE;
		int dscale = DSCALE;
		int i,j,x,y,n;
		char* coutfilename=(char*)malloc(512*sizeof(char));
		CvMat* imdft=cvCreateMatHeader( w*2, h*2, CV_64FC2 );
		cvCreateData( imdft);
		cvZero(imdft);
		for(i=0;i<h;i++) { // rellenamos la matriz de nuestra imagen de entrada...
			for(j=0;j<w;j++) {
				((double*)(imdft->data.ptr + imdft->step*i))[j*2] = ((double*)(img->imageData + img->widthStep*i))[j];
				((double*)(imdft->data.ptr + imdft->step*i))[j*2+1] = 0.0;
			}
		}

		cvDFT(imdft,imdft,CV_DXT_FORWARD); // ...y obtenemos su DFT

		n=w*h/(dscale*dscale);
		for (i=0;i<5;i++) {
			for (j=0;j<8;j++) {
				CvMat* gout=cvCreateMatHeader( w*2, h*2, CV_64FC2 );
				cvCreateData( gout);

				mathOperations::multiplyFFT(imdft,mGabor[i*8+j],gout); // multiplicamos la matriz FFT de nuestra imagen con la actual FFT de Gabor...
				cvDFT(gout,gout,CV_DXT_INVERSE); // ... y obtenemos la FFT inversa de tal resultado

				/*downsample scale factor dscale*dscale*/
				for (x=dscale/2;x<w;x+=dscale) {
					for (y=dscale/2;y<h;y+=dscale) {
						double sum=((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2]*
						((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2]+
						((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2+1]*
						((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2+1];

						object[(i*dscale+j)*n+x/dscale*h/dscale+y/dscale]=sqrt(sum);
					}
				}

				if (DEBUG_GABOR) { // creamos los ficheros salida de la imagen de entrada
					CvMat* gimg=cvCreateMatHeader( w, h, CV_64FC1 );
					cvCreateData( gimg);
					for (x=0;x<w;x++) {
						for (y=0;y<h;y++) {
							((double*)(gimg->data.ptr + gimg->step*x))[y] =
							sqrt( ((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2]*
							((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2]+
							((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2+1]*
							((double*)(gout->data.ptr + gout->step*(x+h/2)))[(y+w/2)*2+1]);
						}
					}

					char coutfilename[512];
					sprintf(coutfilename,"GaborImageOutput_%d_%d.bmp",i,j);
					fileOperations::saveMatrixImage(gimg,coutfilename);
					cvReleaseMat(&gimg);
				}
				cvReleaseMat(&gout);		 	
				mathOperations::meanUnitVector(object+(i*dscale+j)*n,n);
			}
		}
		free(coutfilename);
		cvReleaseMat(&imdft);

		return(1);
	}

	double* gaborFilter::extractImageFeatures(const IplImage* img,int *nsize) {
		(*nsize) = 40*128*128/64;
		CvSize size = cvSize(128,128);	
		CvSize img_size = cvGetSize( img );
		IplImage*	ipl=	cvCreateImage(img_size,8,0);

		if(img->nChannels==3)	{
			cvCvtColor(img,ipl,CV_BGR2GRAY);
		} else {
			cvCopy(img,ipl,0);
		}

		if((size.width!=img_size.width)||(size.height!=img_size.height)) { // reescalado de imagen a 128x128!
			IplImage* tmpsize=cvCreateImage(size,8,0);
			cvResize(ipl,tmpsize,CV_INTER_LINEAR); // mediante interlineado sencillo
			cvReleaseImage( &ipl);
			ipl=cvCreateImage(size,8,0);
			cvCopy(tmpsize,ipl,0);
			cvReleaseImage( &tmpsize);
		}

		double* object=(double*)malloc((*nsize)*sizeof(double));
		IplImage* tmp=cvCreateImage(size,IPL_DEPTH_64F,0);	

		cvConvertScale(ipl,tmp,1.0,0); // convertimos la profundidad a 64F
		CvMat** mGabor= loadGaborFFT("gaborBank"); // Gabor wavelet: cargamos la matriz mGabor con todas las distintas FFT's que tenemos

		extractGabor(tmp,object,mGabor);

		mathOperations::meanUnitVector(object,*nsize);
		cvReleaseImage( &tmp);
		cvReleaseImage( &ipl);

		unloadGaborFFT(mGabor);

		return object;
	}

	int gaborFilter::gaborKernel (int scale,int orientation,int mask_size,double kmax,double sigma,char* filename) {
		FILE *cout, *sout;
		register int u,v,x,y;

		char coutfilename[512],soutfilename[512];

		double* gabor_cos = (double *)malloc (mask_size*mask_size*sizeof (double));
		double* gabor_sin = (double *)malloc (mask_size*mask_size*sizeof (double));
		int offset = mask_size / 2;

		double f=sqrt(0.65); // será el "grueso" de la onda, a menor valor, menor grueso
		double sig=sigma;//*sigma; // prolongación de la onda
		for (v=0;v<scale;v++) {
			for (u=0;u<orientation;u++) {
				double kv=kmax/pow(f,v);
				double phiu=u*(M_PI/8.0); // ángulo de inclinación
				double kv_mag=kv*kv;

				for (x = 0; x < mask_size; x++) {
					for (y = 0; y < mask_size; y++) {
						int i=x-offset; 
						int j=y-offset;
						double mag=(double)(i*i+j*j);
						gabor_cos[x*mask_size+y] = (kv_mag/sig)*exp(-0.5*kv_mag*mag/sig)*(cos(kv*(i*cos(phiu)+j*sin(phiu)))-exp(-1.0*sig/2.0)); // parte imaginaria
						gabor_sin[x*mask_size+y] = (kv_mag/sig)*exp(-0.5*kv_mag*mag/sig)*(sin(kv*(i*cos(phiu)+j*sin(phiu)))-exp(-1.0*sig/2.0)); // parte real
					}
				}

				/* Let's deal with the COSINE PART first */
				sprintf(coutfilename,"gaborBank//C%s_%d_%d.data",filename,v,u);
				sprintf(soutfilename,"gaborBank//S%s_%d_%d.data",filename,v,u);

				if ((cout = fopen(coutfilename, "wb")) == NULL) {
					printf("Cannot open file %s!\n",coutfilename);
					return(0);
				}
				CvMat cmat = cvMat (mask_size, mask_size,CV_64FC1,gabor_cos);
				fileOperations::writeMatrix(&cmat,cout,false); // pasamos la matriz de datos (cmat) al fichero actual (.data)
				fclose(cout);
				sprintf(coutfilename,"gaborBank//C%s_%d_%d.bmp",filename,v,u);
				fileOperations::saveMatrixImage(&cmat,coutfilename); // guardamos la matriz como imagen (.bmp) para visualizar los resultados

				/* Now let's deal with the SINE PART */
				if ((sout = fopen(soutfilename, "wb")) == NULL) {
					printf("Cannot open file %s!\n",soutfilename);
					return(0);
				}
				CvMat smat = cvMat( mask_size, mask_size,CV_64FC1,gabor_sin );
				fileOperations::writeMatrix(&smat,sout,false);
				sprintf(soutfilename,"gaborBank//S%s_%d_%d.bmp",filename,v,u);
				fileOperations::saveMatrixImage(&smat,soutfilename);
				fclose(sout);
			}
		}
		free(gabor_cos);
		free(gabor_sin);
		printf(" OK!\n");
		return 1;
	}

	int gaborFilter::gaborFunction (char* base_fld) {
		int img_size=128;
		int num_scales=5;
		int num_orientations=8;
		int i,j,x,y;
		char coutfilename[512];
		printf("Building Gabor filter bank data: ");
		gaborKernel(num_scales,num_orientations,img_size,M_PI/2.0,2*M_PI, "gaborBank");
		printf("Calculating Gabor filter bank FFT: ");

		int dft_M = cvGetOptimalDFTSize (img_size + img_size - 1);
		int dft_N = cvGetOptimalDFTSize (img_size + img_size - 1);

		for (i=0;i<num_scales;i++)	{
			for (j=0;j<num_orientations;j++) {
				sprintf(coutfilename,"%s//C%s_%d_%d.data",base_fld,"gaborBank",i,j);
				FILE *ifp = fopen(coutfilename, "rb");
				if (ifp == NULL) {
					printf ("Can't load file %s\n", coutfilename);
					exit (-1);
				}
				CvMat* mGaborReal	= fileOperations::getMatrix(ifp,false); // obtenemos la matriz actual (REAL) contenida en el fichero (.data)
				fclose(ifp);

				sprintf(coutfilename,"%s//S%s_%d_%d.data",base_fld,"gaborBank",i,j);
				FILE *ifps = fopen(coutfilename, "rb");
				if (ifps == NULL) {
					printf ("Can't load file %s\n", coutfilename);
					exit (-1);
				}
				CvMat* mGaborImg	= fileOperations::getMatrix(ifps,false); // obtenemos la matriz actual (IMAGINARIA) contenida en el fichero (.data)
				fclose(ifps);

				CvMat* mGabor=cvCreateMatHeader( dft_M, dft_N, CV_64FC1 );
				cvCreateData( mGabor);

				CvMat* mGaborFFT=cvCreateMatHeader( dft_M, dft_N, CV_64FC1);
				cvCreateData( mGaborFFT);

				cvZero(mGabor);
				for(x=0;x<img_size;x++) {
					for(y=0;y<img_size;y++)	{	
						((double*)(mGabor->data.ptr + mGabor->step*x))[y]=
						CV_MAT_ELEM(*mGaborReal,double,x,y); // de momento sólo trataremos el gabor real, podemos también utilizar la imaginaria
						//	((double*)(mGabor->data.ptr + mGabor->step*x))[y*2+1]=
						//CV_MAT_ELEM(*mGaborImg,double,x,y);
					}
				}
				cvDFT(mGabor,mGaborFFT,CV_DXT_FORWARD,img_size);

				sprintf(coutfilename,"%s//FFT%s_%d_%d.data",base_fld,"gaborBank",i,j);
				FILE *ofps = fopen(coutfilename, "wb");
				if (ofps == NULL) {
					printf ("Can't load file %s\n", coutfilename);
					exit (-1);
				}
				fileOperations::writeMatrix(mGaborFFT,ofps,false);
				fclose(ofps);

				cvReleaseMat(&mGaborFFT);

				cvReleaseMat(&mGabor);
				cvReleaseMat(&mGaborReal);
				cvReleaseMat(&mGaborImg);
			}
		}
		printf(" OK!\n");
		return 1;
	}

	void gaborFilter::extractFeatures (char *imagefile, char *imagedata) {
		IplImage* img= cvLoadImage(imagefile,-1);
		if(img==0) {
			printf ("Can't load image file %s!\n", imagefile);	
			exit (0);
		}

		int len;
		double* fea_vec = extractImageFeatures(img,&len);
		FILE* fp = fopen(imagedata,"wb");
		if(fp==0) {
			printf ("Can't open data file %s!\n", imagedata);	
			exit (-1);
		}
		fileOperations::writeData(fea_vec,len,fp);
		fclose(fp);
		free(fea_vec);
		printf("Gabor Wavelets features %s length: %d\n", imagedata,len);
	}

	gaborFilter::~gaborFilter () {}

}

